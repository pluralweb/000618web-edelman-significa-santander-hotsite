
/**
 * jquery.mask.js
 * @version: v1.14.0
 * @author: Igor Escobar
 *
 * Created by Igor Escobar on 2012-03-10. Please report any bug at http://blog.igorescobar.com
 *
 * Copyright (c) 2012 Igor Escobar http://blog.igorescobar.com
 *
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

/* jshint laxbreak: true */
/* global define */

"use strict";
! function(a, b, c) { "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a(require("jquery")) : a(b || c) }(function(a) {
    var b = function(b, c, d) {
        var e = {
            invalid: [],
            getCaret: function() {
                try {
                    var a, c = 0,
                        d = b.get(0),
                        f = document.selection,
                        g = d.selectionStart;
                    return f && -1 === navigator.appVersion.indexOf("MSIE 10") ? (a = f.createRange(), a.moveStart("character", -e.val().length), c = a.text.length) : (g || "0" === g) && (c = g), c
                } catch (h) {}
            },
            setCaret: function(a) {
                try {
                    if (b.is(":focus")) {
                        var c, d = b.get(0);
                        d.setSelectionRange ? (d.focus(), d.setSelectionRange(a, a)) : (c = d.createTextRange(), c.collapse(!0), c.moveEnd("character", a), c.moveStart("character", a), c.select())
                    }
                } catch (e) {}
            },
            events: function() { b.on("keydown.mask", function(a) { b.data("mask-keycode", a.keyCode || a.which) }).on(a.jMaskGlobals.useInput ? "input.mask" : "keyup.mask", e.behaviour).on("paste.mask drop.mask", function() { setTimeout(function() { b.keydown().keyup() }, 100) }).on("change.mask", function() { b.data("changed", !0) }).on("blur.mask", function() { h === e.val() || b.data("changed") || b.trigger("change"), b.data("changed", !1) }).on("blur.mask", function() { h = e.val() }).on("focus.mask", function(b) { d.selectOnFocus === !0 && a(b.target).select() }).on("focusout.mask", function() { d.clearIfNotMatch && !f.test(e.val()) && e.val("") }) },
            getRegexMask: function() {
                for (var a, b, d, e, f, h, i = [], j = 0; j < c.length; j++) a = g.translation[c.charAt(j)], a ? (b = a.pattern.toString().replace(/.{1}$|^.{1}/g, ""), d = a.optional, e = a.recursive, e ? (i.push(c.charAt(j)), f = { digit: c.charAt(j), pattern: b }) : i.push(d || e ? b + "?" : b)) : i.push(c.charAt(j).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&"));
                return h = i.join(""), f && (h = h.replace(new RegExp("(" + f.digit + "(.*" + f.digit + ")?)"), "($1)?").replace(new RegExp(f.digit, "g"), f.pattern)), new RegExp(h)
            },
            destroyEvents: function() { b.off(["input", "keydown", "keyup", "paste", "drop", "blur", "focusout", ""].join(".mask ")) },
            val: function(a) {
                var c, d = b.is("input"),
                    e = d ? "val" : "text";
                return arguments.length > 0 ? (b[e]() !== a && b[e](a), c = b) : c = b[e](), c
            },
            getMCharsBeforeCount: function(a, b) {
                for (var d = 0, e = 0, f = c.length; f > e && a > e; e++) g.translation[c.charAt(e)] || (a = b ? a + 1 : a, d++);
                return d
            },
            caretPos: function(a, b, d, f) {
                var h = g.translation[c.charAt(Math.min(a - 1, c.length - 1))];
                return h ? Math.min(a + d - b - f, d) : e.caretPos(a + 1, b, d, f)
            },
            behaviour: function(c) {
                c = c || window.event, e.invalid = [];
                var d = b.data("mask-keycode");
                if (-1 === a.inArray(d, g.byPassKeys)) {
                    var f = e.getCaret(),
                        h = e.val(),
                        i = h.length,
                        j = e.getMasked(),
                        k = j.length,
                        l = e.getMCharsBeforeCount(k - 1) - e.getMCharsBeforeCount(i - 1),
                        m = i > f;
                    return e.val(j), m && (8 !== d && 46 !== d && (f = e.caretPos(f, i, k, l)), e.setCaret(f)), e.callbacks(c)
                }
            },
            getMasked: function(a, b) {
                var f, h, i = [],
                    j = void 0 === b ? e.val() : b + "",
                    k = 0,
                    l = c.length,
                    m = 0,
                    n = j.length,
                    o = 1,
                    p = "push",
                    q = -1;
                for (d.reverse ? (p = "unshift", o = -1, f = 0, k = l - 1, m = n - 1, h = function() {
                        return k > -1 && m > -1
                    }) : (f = l - 1, h = function() {
                        return l > k && n > m
                    }); h();) {
                    var r = c.charAt(k),
                        s = j.charAt(m),
                        t = g.translation[r];
                    t ? (s.match(t.pattern) ? (i[p](s), t.recursive && (-1 === q ? q = k : k === f && (k = q - o), f === q && (k -= o)), k += o) : t.optional ? (k += o, m -= o) : t.fallback ? (i[p](t.fallback), k += o, m -= o) : e.invalid.push({ p: m, v: s, e: t.pattern }), m += o) : (a || i[p](r), s === r && (m += o), k += o)
                }
                var u = c.charAt(f);
                return l !== n + 1 || g.translation[u] || i.push(u), i.join("")
            },
            callbacks: function(a) {
                var f = e.val(),
                    g = f !== h,
                    i = [f, a, b, d],
                    j = function(a, b, c) { "function" == typeof d[a] && b && d[a].apply(this, c) };
                j("onChange", g === !0, i), j("onKeyPress", g === !0, i), j("onComplete", f.length === c.length, i), j("onInvalid", e.invalid.length > 0, [f, a, b, e.invalid, d])
            }
        };
        b = a(b);
        var f, g = this,
            h = e.val();
        c = "function" == typeof c ? c(e.val(), void 0, b, d) : c, g.mask = c, g.options = d, g.remove = function() {
            var a = e.getCaret();
            return e.destroyEvents(), e.val(g.getCleanVal()), e.setCaret(a - e.getMCharsBeforeCount(a)), b
        }, g.getCleanVal = function() {
            return e.getMasked(!0)
        }, g.getMaskedVal = function(a) {
            return e.getMasked(!1, a)
        }, g.init = function(c) {
            if (c = c || !1, d = d || {}, g.clearIfNotMatch = a.jMaskGlobals.clearIfNotMatch, g.byPassKeys = a.jMaskGlobals.byPassKeys, g.translation = a.extend({}, a.jMaskGlobals.translation, d.translation), g = a.extend(!0, {}, g, d), f = e.getRegexMask(), c === !1) {
                d.placeholder && b.attr("placeholder", d.placeholder), b.data("mask") && b.attr("autocomplete", "off"), e.destroyEvents(), e.events();
                var h = e.getCaret();
                e.val(e.getMasked()), e.setCaret(h + e.getMCharsBeforeCount(h, !0))
            } else e.events(), e.val(e.getMasked())
        }, g.init(!b.is("input"))
    };
    a.maskWatchers = {};
    var c = function() {
            var c = a(this),
                e = {},
                f = "data-mask-",
                g = c.attr("data-mask");
            return c.attr(f + "reverse") && (e.reverse = !0), c.attr(f + "clearifnotmatch") && (e.clearIfNotMatch = !0), "true" === c.attr(f + "selectonfocus") && (e.selectOnFocus = !0), d(c, g, e) ? c.data("mask", new b(this, g, e)) : void 0
        },
        d = function(b, c, d) {
            d = d || {};
            var e = a(b).data("mask"),
                f = JSON.stringify,
                g = a(b).val() || a(b).text();
            try {
                return "function" == typeof c && (c = c(g)), "object" != typeof e || f(e.options) !== f(d) || e.mask !== c
            } catch (h) {}
        },
        e = function(a) {
            var b, c = document.createElement("div");
            return a = "on" + a, b = a in c, b || (c.setAttribute(a, "return;"), b = "function" == typeof c[a]), c = null, b
        };
    a.fn.mask = function(c, e) {
        e = e || {};
        var f = this.selector,
            g = a.jMaskGlobals,
            h = g.watchInterval,
            i = e.watchInputs || g.watchInputs,
            j = function() {
                return d(this, c, e) ? a(this).data("mask", new b(this, c, e)) : void 0
            };
        return a(this).each(j), f && "" !== f && i && (clearInterval(a.maskWatchers[f]), a.maskWatchers[f] = setInterval(function() { a(document).find(f).each(j) }, h)), this
    }, a.fn.masked = function(a) {
        return this.data("mask").getMaskedVal(a)
    }, a.fn.unmask = function() {
        return clearInterval(a.maskWatchers[this.selector]), delete a.maskWatchers[this.selector], this.each(function() {
            var b = a(this).data("mask");
            b && b.remove().removeData("mask")
        })
    }, a.fn.cleanVal = function() {
        return this.data("mask").getCleanVal()
    }, a.applyDataMask = function(b) {
        b = b || a.jMaskGlobals.maskElements;
        var d = b instanceof a ? b : a(b);
        d.filter(a.jMaskGlobals.dataMaskAttr).each(c)
    };
    var f = { maskElements: "input,td,span,div", dataMaskAttr: "*[data-mask]", dataMask: !0, watchInterval: 300, watchInputs: !0, useInput: e("input"), watchDataMask: !1, byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91], translation: { 0: { pattern: /\d/ }, 9: { pattern: /\d/, optional: !0 }, "#": { pattern: /\d/, recursive: !0 }, A: { pattern: /[a-zA-Z0-9]/ }, S: { pattern: /[a-zA-Z]/ } } };
    a.jMaskGlobals = a.jMaskGlobals || {}, f = a.jMaskGlobals = a.extend(!0, {}, f, a.jMaskGlobals), f.dataMask && a.applyDataMask(), setInterval(function() { a.jMaskGlobals.watchDataMask && a.applyDataMask() }, f.watchInterval)
}, window.jQuery, window.Zepto);
