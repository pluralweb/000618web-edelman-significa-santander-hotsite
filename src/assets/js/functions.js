$(document).ready(function() {
$('#myModal').modal('show');
    //mask - cpf
    var cpf = $('.cpf');
    if (cpf.length) {
        cpf.mask('000.000.000-00', {
            reverse: true
        });
    }

    $('#form_avatar').change(function(){
         $('#form_avatar').submit();
    });
       

    // Owl carousel
    var owl = $(".owl-carousel");
    if (owl.find('.row').length>1) {
      owl.owlCarousel({
        loop:false,
        margin:2,
        nav:false,
        responsive:{
          0:{
            items:2
          },
          600:{
            items:4
          }
        }
      })
    }


    $(".next").on('click', function() {
        owl.trigger('next.owl.carousel');
    });

    $(".prev").on('click', function() {
        owl.trigger('prev.owl.carousel');
    });

    // end:Owl carousel

    // Login
    $('.login').on('submit', function(event) {
        event.preventDefault();
        var el = $(this);
        var matricula = el.find('input[name="matricula"]');
        var senha = el.find('input[name="senha"]');
        var erros = 0;

        if (matricula.val().trim() == '') {
            matricula.addClass('error');
            erros++;
        } else {
            matricula.removeClass('error');
        }

        if (senha.val().trim() == '') {
            senha.addClass('error');
            erros++;
        } else {
            senha.removeClass('error');
        }

        if (erros == 0) {
            $.post('home/loga', {
                matricula: matricula.val().trim(),
                senha: senha.val().trim()
            }, function(data) {
                if (data['return'] == 's') {
                    location.replace('home/');
                } else if(data['return'] == 'v') {
                  alert(data['msg']);
                  // location.replace('home/');
                } else {
                    alert('Dados inválidos.');
                }
            }, 'json');
        }

        return false;
    });
    //end: Login

    // FAQ
    $('h3.question').on('click', function() {
      $('.faq .box .answer').removeClass('active').slideUp(800);

      if ($(this).next().is(':visible')) {
        return false;
      }

      $(this).next().removeClass('active').slideDown();


      return false;
    });
    // end:FAQ

    // Cadastro
    $('.firstButton').on('click', function(event) {
        event.preventDefault();

        var el = $(this).closest('.primeiroAcesso');
        var matricula = el.find('input[name="matricula"]');
        var cpf = el.find('input[name="cpf"]');

        var one = el.find('.firstStep');
        var two = el.find('.secondStep');

        var erros = 0;

        if (matricula.val().trim() == '') {
            matricula.addClass('error');
            erros++;
        } else {
            matricula.removeClass('error');
        }

        if (cpf.val().trim() == '') {
            cpf.addClass('error');
            erros++;
        } else {
            cpf.removeClass('error');
        }

        if (erros == 0) {
          $.post('home/verificaMatricula', {
              matricula: matricula.val().trim(),
              cpf: cpf.val().trim()
          }, function(data) { 
              if (data['return'] == 's') {                
                  $('.primeiroAcesso').removeClass('active');
                  $('.hideDiv.no').removeClass('no');
                  one.removeClass('active');
                  two.addClass('active');
              } else if(data['return'] == 'r') {
                  cpf.addClass('error');
                  matricula.addClass('error');
                  alert(data['msg']);
              } else {
                  alert(data['msg']);
              }
          }, 'json');

        }

    })

      // Upload de avatar
    if ($('#zoneUpload').length > 0) {
        $('#zoneUpload').on('submit', function(e) {
            e.preventDefault();
            var el = $(this);

            el.find('button').prop('disabled', true);

            if ($('#iframeNew').length == 0) {
                $('body').append('<iframe name="iframeNew" id="iframeNew" style="width: 0; height: 0; visibility: hidden; opacity: 0;"></iframe>');
            }

            var iframe = $('#iframeNew');

            setTimeout(function() {
                el.attr('action', 'home/cadastraImg');
                el.attr('target', 'iframeNew');
                el[0].submit();

                setTimeout(function() {
                    var interval = setInterval(function(){
                      var i = iframe.contents().find('body').text();
                      if (i.length > 0) {
                        clearInterval(interval);
                        if (i != 404) {
                          $('.hideDiv').addClass('no');
                          $('.primeiroAcesso').addClass('active');
                          $('#server_response').val(i);
                          iframe.remove();
                        } else {
                          iframe.remove();
                          alert('Ocorreu um erro ao subir a sua imagem, tente novamente!');
                        }
                      }
                    }, 200)
                }, 100);
            }, 300);

            el.find('button').prop('disabled', true);
        });
    }

    $('.secondButton').on('click', function(event) {
        event.preventDefault();

        var el = $(this).closest('.primeiroAcesso');
        var senha = el.find('input[name="senha"]');
        var resenha = el.find('input[name="resenha"]');

        var erros = 0;

        if (senha.val().trim() == '' && resenha.val().trim() == '') {
            senha.addClass('error');
            resenha.addClass('error');
            erros++;
        } else {
            if (senha.val().trim() !== resenha.val().trim()) {
                senha.addClass('error');
                resenha.addClass('error');
                erros++;
            } else {
                senha.removeClass('error');
                resenha.removeClass('error');
            }
        }

        if (erros == 0) {
            $('.primeiroAcesso').submit();
        }

    })

    $('.primeiroAcesso').on('submit', function(event) {
        event.preventDefault();
        var el = $(this);
        var erros = 0;
        var senha = el.find('input[name="senha"]');
        var matricula = el.find('input[name="matricula"]');
        var cpf = el.find('input[name="cpf"]');
        var img = el.find('input[name="imgResponse"]');

        $.post('home/cadastraSenha', {
            matricula: matricula.val().trim(),
            cpf: cpf.val().trim(),
            senha: senha.val().trim(),
            img: img.val().trim(),
        }, function(data) {
            if (data == 's') {
                location.replace('home/');
            } else {
                alert('Erro ao cadastrar senha');
            }
        });
        return false;
    });

    // end:Cadastro

    // Esqueci minha senha
    $('.esqueci').on('submit', function(event) {
        event.preventDefault();
        var el = $(this);
        var matricula = el.find('input[name="matricula"]');
        var erros = 0;

        if (matricula.val().trim() == '') {
            matricula.addClass('error');
            erros++;
        } else {
            matricula.removeClass('error');
        }

        if (erros == 0) {
          $.post('home/esqueciSenha', {
                matricula: matricula.val().trim()
            }, function(data) {
                if (data == 's') {
                    alert('Nova senha foi enviada por e-mail.');
                    el.removeClass('showForm');
                    el.filter(function() {
                      return $(this).data('form') == 'login';
                    }).addClass('showForm');
                } else {
                    alert('Matricula não encontrada');
                }
            });
        }
        return false;
    });
    // end:Esqueci minha senha

    $('.reg-open').on('click', function() {
        $('.regulamento').fadeIn().css('display', 'block');
        $('.modal').fadeIn().css('display', 'table');
    });

    $('.img_new').on('click', function() {
        $('.img_new_targ').fadeIn().css('display', 'block');
        $('.modal').fadeIn().css('display', 'table');
    });

    $('.duv-open').on('click', function() {
        $('.faq').fadeIn().css('display', 'block');
        $('.modal').fadeIn().css('display', 'table');
    });

    $('.modal .close').on('click', function() {
      $('.modal, .bodyModal').fadeOut();
    });

    $('.align').on('click', function(e) {
      var target = $(e.target);
      if(target.is('.bodyModal') || target.closest('.bodyModal').length > 0){
        return false;
      }
      $('.modal, .bodyModal').fadeOut();
    });

    $('.modalButton').on('click', function() {
      var img = $(this).data('img');
        $('.modal-gallery ').find('img').attr('src', img);
        $('.modal, .modal-gallery').fadeIn().css('display', 'table');
    });

    $('*[data-toggle=lightboxVideo]').on('click', function() {
        var el = $(this);
        var target = el.data('target');

        $(target).addClass('active');
        $(target).children().addClass('active');

        $('.lightboxVideo').on('click', function() {
            $(this).children().removeClass('active');
            $(this).removeClass('active');

            $(this).find('iframe').attr('src', '');
        });

        // Video from YouTube
        if (el.data('youtube')) {
            var videoUrl = el.data('youtube');
            var id = videoUrl.split('watch?v=')[1]; // get the id so you can add to iframe

            $(target).find('iframe').attr('src', 'http://www.youtube.com/embed/' + id + '?autoplay=1');
        }
    });

    /**
     * Hide current form and
     * show forget login form
     */

    $('.forgetPass').on('click', function(e) {
        e.preventDefault();
        var form = $('.form');
        form.removeClass('showForm');
        form.filter(function() {
            return $(this).data('form') == 'forget';
        }).addClass('showForm')

        return false;
    });

    /**
     * Hide current form and
     * show first access form
     */
    $('.firstAccessLink').on('click', function(e) {
        e.preventDefault();
        var form = $('.form');
        form.removeClass('showForm');
        form.filter(function() {
            return $(this).data('form') == 'firstAccess';
        }).addClass('showForm')

        return false;
    });

    /**
     * Hide current form and
     * show login form
     */
    $('.closeForm').on('click', function(e) {
        e.preventDefault();
        var form = $('.form');
        form.removeClass('showForm');
        form.filter(function() {
            return $(this).data('form') == 'login';
        }).addClass('showForm')

        return false;
    });
});
