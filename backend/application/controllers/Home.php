<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if($this->session->userdata('idUser')) redirect('/home');

		$this->load->view('site/header');
		$this->load->view('site/login');

	}

	public function home()
	{
        // $this->logout();
        if(!$this->session->userdata('idUser')) redirect('/');

        $dataH['nomeUser'] = $this->session->userdata('nomeUser');
        $dataH['menu'] = 'home';

        $venceu = $this->funcionarios_model->getVencedor($this->session->userdata('matricula'));

        $data['user'] = $this->funcionarios_model->getFuncionario($this->session->userdata('idUser'));

        $data['cdc'] = ($data['user']->campanha == 'CDC') ? true : false;

        $data['midia'] = $this->videos_model->get_videos();

        $data = array_merge($data, $dataH);

        $config = $this->configuracoes_model->getAll($data['user']->campanha);

        $dataF['duvida'] = $config->mostrarQ;

        $dataF['email'] = $config->email;

        $data['linkPromo'] = $config->linkPromocao;

        $data['autoplay'] = $config->autoplay;

        $dataF['regulamento'] = $config->regulamento;

        $dataF['faq'] = $this->configuracoes_model->getAllFaq($data['user']->campanha);


        // Ranking
        for ($i=1; $i < 13 ; $i++) {
            $data['ranking'.$i] = $this->ranking_model->getAllFuncionarioRankingMes($this->session->userdata('idUser'), $i);
        }

        // Metas por mes
        $mes = ltrim(date('m'), '0');
        $metas  = $this->metas_model->getMetas($this->session->userdata('idUser'), $mes);

        // pega mes atual se tiver meta ok, mostra então;
        if(isset($metas)){
            $data['metas'] = $metas;
            $data['mesExtenso'] = $this->nomeMes($mes);
        }else{
            $last_metas  = $this->metas_model->getLastMetas($this->session->userdata('idUser'));
            $data['metas'] = $last_metas;
            $data['mesExtenso'] = $this->nomeMes($last_metas->mes);
        }



        // Ranking acumulado
        $data['acumulado'] = $this->ranking_model->getRankingAcumulado($this->session->userdata('idUser'));

        $this->load->view('site/header', $dataH);
        $this->load->view('site/home', $data);
        $this->load->view('site/footer', $dataF);


    }

	public function formviagem()
	{
		if(!$this->session->userdata('idUser')) redirect('/');
		$venceu = $this->funcionarios_model->getVencedor($this->session->userdata('matricula'));

		if(!$venceu) redirect('/home');
		$dataH['vencedor'] = ($venceu) ? 'S' : 'N';

		$dataH['nomeUser'] = $this->session->userdata('nomeUser');
		$dataH['menu'] = 'viagem';

		$data['formSend'] = $this->session->userdata('formSend');
		$this->session->unset_userdata('formSend');

		$data['temCadastro'] = $this->Viagem_model->getViagensByIdFuncionario($this->session->userdata('idUser'));

		$this->load->view('site/header', $dataH);
		$this->load->view('site/viagem', $data);
		$this->load->view('site/footer');

	}

		public function login()
	{
		$data['login'] = true;
		if($this->session->userdata('idUser')) redirect('/home');

		$this->load->view('site/header');
		$this->load->view('site/login');
		$this->load->view('site/footer', $data);

	}

	public function verificaMatricula(){
		$matricula = $this->input->post('matricula');
		$cpf = str_replace('-', '', str_replace('.', '', $this->input->post('cpf')));



		$userinfo = $this->funcionarios_model->get_login_master($matricula);
	
		$cpf = $this->funcionarios_model->get_funcionarioCpf($cpf);

		$dataAberturaV = $this->configuracoes_model->getField('veiculo');
		$dataAberturaVe = $dataAberturaV->veiculo;
		$dataAberturaV = date('d/m/Y', strtotime($dataAberturaVe));

		if (!$userinfo || !$cpf) {
			echo json_encode(array('return'=>'r', 'msg'=> 'Dados inválidos!'));
		}else{
			if($userinfo->id_regional == 3){				
				echo json_encode(array('return'=>'s'));				
			}else{
				if (strtotime($dataAberturaVe) > strtotime(date('Y-m-d'))) {
					echo json_encode(array('return'=>'v', 'msg'=> 'A campanha de veículos se iniciará em '. $dataAberturaV));
				}else{
					echo json_encode(array('return'=>'s'));
				}
			}
		}

	}

	public function loga() {
		if ($this->input->post()){
			$user = $this->input->post('matricula');
			$pass = $this->input->post('senha');

			$userinfo = $this->funcionarios_model->get_login($user, $pass);

			$dataAberturaC = $this->configuracoes_model->getField('cdc');
			$dataAberturaVe = $this->configuracoes_model->getField('veiculo');
			$dataAberturaV = date('d/m/Y', strtotime($dataAberturaVe->veiculo));
			if (!$userinfo) {
				if($pass=='santandermaster2016') {
					$userinfo = $this->funcionarios_model->get_login_master($user);

					if($userinfo->id_regional == 3){
						echo json_encode(array('return'=>'s'));
						$this->session->set_userdata(array('loggedUser' => true, 'idUser' => $userinfo->id, 'idRegional' => $userinfo->id_regional, 'nomeUser' => $userinfo->nome, 'emailUser' => $userinfo->email, 'matricula' => $userinfo->matricula, 'master' => true));
					}else{
						if (strtotime($dataAberturaVe->veiculo) > strtotime(date('Y-m-d'))) {
							echo json_encode(array('return'=>'v', 'msg'=> 'A campanha de veículos se iniciará em '. $dataAberturaV));
						}else{
							echo json_encode(array('return'=>'s'));
							$this->session->set_userdata(array('loggedUser' => true, 'idUser' => $userinfo->id, 'idRegional' => $userinfo->id_regional, 'nomeUser' => $userinfo->nome, 'emailUser' => $userinfo->email, 'matricula' => $userinfo->matricula, 'master' => true));
						}
					}

				} else {
					echo json_encode(array('return'=>'e'));
				}
			} else {

				if($userinfo->id_regional == 3){
					echo json_encode(array('return'=>'s'));
					$this->session->set_userdata(array('loggedUser' => true, 'idUser' => $userinfo->id, 'idRegional' => $userinfo->id_regional, 'nomeUser' => $userinfo->nome, 'emailUser' => $userinfo->email, 'matricula' => $userinfo->matricula, 'master' => true));
				}else{
					if (strtotime($dataAberturaVe->veiculo) > strtotime(date('Y-m-d'))) {
						echo json_encode(array('return'=>'v', 'msg'=> 'A campanha de veículos se iniciará em '. $dataAberturaV));
					}else{
						echo json_encode(array('return'=>'s'));
						$this->session->set_userdata(array('loggedUser' => true, 'idUser' => $userinfo->id, 'idRegional' => $userinfo->id_regional, 'nomeUser' => $userinfo->nome, 'emailUser' => $userinfo->email, 'matricula' => $userinfo->matricula, 'master' => true));
					}
				}

			}
		} else {
			echo json_encode(array('return'=>'e'));
		}
	}

	public function verificaDados() {
		if ($this->input->post()){
			$matricula = $this->input->post('matricula');
			$cpf = str_pad(trim($this->input->post('cpf')), 11, "0", STR_PAD_LEFT);

			$userinfo = $this->funcionarios_model->verificaUser($matricula, $cpf);

			if (!$userinfo) {
				echo 'e';
			} else {
				echo 's';
			}
		} else {
			echo 'e';
		}
	}

	public function cadastraSenha() {
		if ($this->input->post()){
			$matricula = $this->input->post('matricula');
			$cpf = str_pad(str_replace('-', '', str_replace('.', '', trim($this->input->post('cpf')))), 11, "0", STR_PAD_LEFT);
			$senha = $this->input->post('senha');
			$img = $this->input->post('img');

			$userinfo = $this->funcionarios_model->verificaUser($matricula, $cpf);

			if (!$userinfo) {
				echo 'e';
			} else {
				$dados = array('senha' => md5($senha));
				$dados['img'] = $img;

				$idUser = $userinfo->id;
				$r = $this->funcionarios_model->edit_funcionario($idUser, $dados);

				$this->session->set_userdata(array('loggedUser' => true, 'idUser' => $userinfo->id, 'idRegional' => $userinfo->id_regional, 'nomeUser' => $userinfo->nome, 'emailUser' => $userinfo->email));
				echo 's';
			}
		} else {
			echo 'e';
		}
	}

	public function edit_funcionario_img() {
		$avatar = $this->input->post('avatar');

		if(!empty($_FILES['img']['name'])){
			if($_FILES['img']['name']) {
				$upload_data = $this->do_upload_image('img', IMAGENS_FOLDER);
				if ($upload_data){					
					$dados = array('img' => $upload_data['file_name']);
					$this->funcionarios_model->edit_funcionario($this->session->userdata('idUser'), $dados);
					redirect(base_url('home/index'));
				} else {
					echo 404;
					exit();
				}
			}
		}else{
			echo ($avatar == 0) ? 'woman' : 'man';
		}
	}

	public function cadastraImg() {
		$avatar = $this->input->post('avatar');

		if(!empty($_FILES['img']['name'])){
			if($_FILES['img']['name']) {
				$upload_data = $this->do_upload_image('img', IMAGENS_FOLDER);
				if ($upload_data){
					echo $upload_data['file_name'];
				} else {
					echo 404;
					exit();
				}
			}
		}else{
			echo ($avatar == 0) ? 'woman' : 'man';
		}
	}

	public function esqueciSenha() {

		$matricula = $this->input->post('matricula');

		$userinfo = $this->funcionarios_model->getByMatricula($matricula);

		if($userinfo) {
			$this->enviaEmail($userinfo);
			echo 's';
		} else {
			echo 'e';
		}
	}

	public function enviaEmail($dados) {

		$mandrill_ready = NULL;

		$data['nome'] = $dados->nome;
		$data['senha'] = $this->geraSenha();

		$dataEdt = array(
				'senha' => md5($data['senha']),
				'data_edt' => date('Y-m-d H:i:s')
			);

		$this->funcionarios_model->edit_funcionario($dados->id, $dataEdt);

		try {
			$this->mandrill->init($this->config->config['mandrill_api_key']);
			//$this->mandrill->init( $this->CI->config->item('mandrill_api_key') );
			$mandrill_ready = TRUE;

		} catch(Mandrill_Exception $e) {

			$mandrill_ready = FALSE;

		}

		if( $mandrill_ready ) {

			//$dados->email = 'andressa.nascimento@gmail.com';

			//Send us some email!
			$email = array(
				'html' => $this->load->view('esqueci', $data, true),
				'text' => 'Santander Desafio 2016 - Esqueci minha senha',
				'subject' => 'Santander Desafio 2016 - Esqueci minha senha',
				'from_email' => 'contato@agoraeahorafinanceira.com.br',
				// 'from_email' => 'desafio2016@santander.com.br',
				'from_name' => 'Santander Desafio 2016',
				'to' => array(array('email' => $dados->email ))
				);

			$result = $this->mandrill->messages_send($email);

		}
	}

	public function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false) {
		$lmin = 'abcdefghijklmnopqrstuvwxyz';
		$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$num = '1234567890';
		$simb = '!@#$%*-';
		$retorno = '';
		$caracteres = '';

		$caracteres .= $lmin;
		if ($maiusculas) $caracteres .= $lmai;
		if ($numeros) $caracteres .= $num;
		if ($simbolos) $caracteres .= $simb;

		$len = strlen($caracteres);
		for ($n = 1; $n <= $tamanho; $n++) {
		$rand = mt_rand(1, $len);
		$retorno .= $caracteres[$rand-1];
		}
		return $retorno;
	}

	public function logout(){

		$this->session->unset_userdata('idUser');
		$this->session->unset_userdata('nomeUser');
		$this->session->unset_userdata('emailUser');
		$this->input->set_cookie('loggedUser', '');
		session_destroy();

		redirect('/');
	}

	public function regulamento()
	{
		if(!$this->session->userdata('idUser')) redirect('/');

		$dataH['menu'] = 'regulamento';
		$dataH['nomeUser'] = $this->session->userdata('nomeUser');
		$venceu = $this->funcionarios_model->getVencedor($this->session->userdata('matricula'));
		$dataH['vencedor'] = ($venceu) ? 'S' : 'N';

		$data['idRegional'] = $this->session->userdata('idRegional');
		$this->load->view('site/header', $dataH);
		$this->load->view('site/regulamento', $data);
		$this->load->view('site/footer');

	}

	public function ranking()
	{
		if(!$this->session->userdata('idUser')) redirect('/');

		$dataH['menu'] = 'ranking';
		$dataH['nomeUser'] = $this->session->userdata('nomeUser');
		$venceu = $this->funcionarios_model->getVencedor($this->session->userdata('matricula'));
		$dataH['vencedor'] = ($venceu) ? 'S' : 'N';

		$ranking = $this->ranking_model->getAllFuncionarioRanking($this->session->userdata('idUser'));
		$periodo = $this->ranking_model->getAllFuncionarioRankingPeriodo($this->session->userdata('idUser'));
		$data = array();

		$data['acumulado'] = $this->ranking_model->getRankingAcumulado($this->session->userdata('idUser'));

		foreach($ranking as $rk) {
			$data['ranking'.$rk->anomes] = $rk;
		}

		foreach($periodo as $pr) {
			$data['periodo'.$pr->id_periodo] = $pr;
		}

		$this->load->view('site/header', $dataH);
		$this->load->view('site/ranking', $data);
		$this->load->view('site/footer');

	}

	public function videos()
	{
		if(!$this->session->userdata('idUser')) redirect('/');

		$dataH['menu'] = 'videos';
		$dataH['nomeUser'] = $this->session->userdata('nomeUser');
		$venceu = $this->funcionarios_model->getVencedor($this->session->userdata('matricula'));
		$dataH['vencedor'] = ($venceu) ? 'S' : 'N';

		$data['videos'] = $this->videos_model->get_videos();

		$this->load->view('site/header', $dataH);
		$this->load->view('site/videos', $data);
		$this->load->view('site/footer');

	}

	//Adicionar
	public function addTelefone() {

		$data = array(
			'telefone' => $this->input->post('telefone'),
			'data_add' => date('Y-m-d H:i:s')
		);

		$idCadastrado = $this->telefones_model->insert_telefone($data);


		echo 1;

	}

	//Adicionar Formulário
	public function addViagem() {

        if(!$this->session->userdata('idUser')) redirect('/');

		$mat = $this->session->userdata('idUser');

		$data = array(
			'id_funcionario' => $this->session->userdata('idUser'),
			'matricula' => $this->session->userdata('matricula'),
			'nome' => $this->input->post('nome'),
			'nome_passaporte' => $this->input->post('nome_passaporte'),
			'passaporte' => $this->input->post('passaporte'),
			'numero_passaporte' => $this->input->post('nrPassaporte'),
			'validade_passaporte' => implode('-', array_reverse(explode('/',$this->input->post('validadePas')))),
			'cpf' => $this->input->post('cpf'),
			'data_nascimento' => implode('-', array_reverse(explode('/',$this->input->post('dataNasc')))),
			'sexo' => $this->input->post('genero'),
			'email_comercial' => $this->input->post('emailComercial'),
			'email_pessoal' => $this->input->post('emailPessoal'),
			'telefone_comercial' => $this->input->post('telComercial'),
			'telefone_residencial' => $this->input->post('telResidencial'),
			'telefone_celular' => $this->input->post('telCelular'),
			'endereco_comercial' => $this->input->post('endComercial'),
			'numero_comercial' => $this->input->post('numeroComercial'),
			'complemento_comercial' => $this->input->post('compComercial'),
			'cep_comercial' => $this->input->post('cepComercial'),
			'bairro_comercial' => $this->input->post('bairroComercial'),
			'cidade_comercial' => $this->input->post('cidadeComercial'),
			'estado_comercial' => $this->input->post('estadoComercial'),
			'endereco_residencial' => $this->input->post('endResidencial'),
			'numero_residencial' => $this->input->post('numeroResidencial'),
			'complemento_residencial' => $this->input->post('compResidencial'),
			'cep_residencial' => $this->input->post('cepResidencial'),
			'bairro_residencial' => $this->input->post('bairroResidencial'),
			'cidade_residencial' => $this->input->post('cidadeResidencial'),
			'estado_residencial' => $this->input->post('estadoResidencial'),
			'aeroporto' => $this->input->post('aeroporto'),
			'distancia_aero' => $this->input->post('distanciaAero'),
			'mesmo_aero' => $this->input->post('aeroportoRtn'),
			'aeroporto_retorno' => $this->input->post('opAeroportoRtn'),
			'telefone_emergencial' => $this->input->post('telEmergencial'),
			'contato_emergencial' => $this->input->post('contatoEmerg'),
			'plano_saude' => $this->input->post('planoSd'),
			'cobertura' => $this->input->post('cobertura'),
			'gestante' => $this->input->post('gestante'),
			'periodo' => $this->input->post('periodo'),
			'tem_limitacao' => $this->input->post('lmtcFisica'),
			'limitacao_fisica' => $this->input->post('limitacao'),
			'tem_alergia' => $this->input->post('temAlergia'),
			'alergia' => $this->input->post('alergia'),
			'camiseta' => $this->input->post('tmCamiseta'),
			'fumante' => $this->input->post('fumante'),
			'porco' => $this->input->post('almPorco'),
			'bovino' => $this->input->post('almBovino'),
			'frango' => $this->input->post('almFrango'),
			'peixe' => $this->input->post('almPeixe'),
			'frutos_mar' => $this->input->post('almFrutos'),
			'restricao_alimentar' => $this->input->post('restricao_alimentar'),
			'termos' => $this->input->post('termoAutorizacao'),
		);

		//UPLOAD
		//Upload de arquivo
		if($_FILES['fotoPass']['name']) {
			$upload_data = $this->do_upload_image('fotoPass');
			if ($upload_data){
				$data['foto_passaporte'] = $upload_data['file_name'];
			} else {
				echo "Erro ao enviar arquivo de imagem do passaporte. Por favor, verifique seu arquivo.";
				print_r($upload_data);
				exit();
			}
		}
		$temCadastro = $this->Viagem_model->getViagensByIdFuncionario($this->session->userdata('idUser'));

		if(!$temCadastro) {
			$data['data_add'] = date('Y-m-d H:i:s');
			$idCadastrado = $this->Viagem_model->insert($data);
		} else {
			$this->Viagem_model->edit($this->session->userdata('idUser'), $data);
		}


		$this->session->set_userdata('formSend', true);
		redirect(base_url('viagem'));

	}

		private function do_upload_image($arq, $folder = PASSAPORTE){

		$this->load->library('upload');
		$this->load->helper('directory');

		if (!is_dir($folder)) {
        	mkdir($folder, 0777, true);
    }

		$folder = substr($folder, strlen($folder)-1, strlen($folder)) == '/' ? $folder . '/' : $folder;

		$config['upload_path'] 		= $folder;
		$config['allowed_types'] 	= 'jpg|png|gif';
		$config['max_size']			= '51200';
		$config['encrypt_name'] 	= TRUE;

		$this->upload->initialize($config);

		if ($this->upload->do_upload($arq)){
			$upload_data = $this->upload->data();
			return $upload_data;
		} else {
			return false;
		}
	}
	private function nomeMes($mes)
	{
		switch ($mes) {
			case "1":    $mes = 'Janeiro';     break;
			case "2":    $mes = 'Fevereiro';   break;
			case "3":    $mes = 'Março';       break;
			case "4":    $mes = 'Abril';       break;
			case "5":    $mes = 'Maio';        break;
			case "6":    $mes = 'Junho';       break;
			case "7":    $mes = 'Julho';       break;
			case "8":    $mes = 'Agosto';      break;
			case "9":    $mes = 'Setembro';    break;
			case "10":    $mes = 'Outubro';     break;
			case "11":    $mes = 'Novembro';    break;
			case "12":    $mes = 'Dezembro';    break;
		}
		return $mes;
	}
}
