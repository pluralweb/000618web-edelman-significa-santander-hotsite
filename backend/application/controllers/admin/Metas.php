<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metas extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'metas';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');

		$data['metas'] = $this->metas_model->getAllMetas();
		
		$data['importMetas'] = $this->session->userdata('importMetas');
		$this->session->unset_userdata('importMetas');


		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/metas_listagem', $data);
	}
	public function importar() {

		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'metas';
		$dataH['subsessao'] = 'importar';
		$dataH['nome'] = $this->session->userdata('nome');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/metas_importar');
	}


	public function import_data(){

		$mes = $this->input->post('mes');

		$config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'xls|xlsx';

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('arquivo')){

	    //retrun errors
	    $error = array('error' => $this->upload->display_errors());

	   // $this->load->view('welcome_message',array('error' => $error));
	    print_r($error);

    }
    else{

    	$uploaded_data = $this->upload->data();
      $file_name = $uploaded_data['file_name'];

    	$data = $this->uploader($file_name);

      //delete uploaded file for the server
      $file_path= './uploads/'.$file_name;
      unlink($file_path);

      $this->db->truncate(METAS_TABLE); 

			if (true) {
				foreach ($data as $user_data) {

					if(isset($user_data['E'])) {  
						$matricula = ltrim($user_data['E'], '0');
						$elegivel = ($user_data['H'] == 'Ativo') ? 1 : 0;
						$meta_prod = $user_data['I'];
						$meta_tab = $user_data['J'];
						$meta_cob = $user_data['K'];

						$funcionario = $this->funcionarios_model->getByMatricula($matricula);

						if($funcionario) { 
							//Ranking 
							$dadosRanking = array(
								'id_funcionario' => $funcionario->id,
								'status' => $elegivel,
								'meta_prod' => $meta_prod,
								'meta_tab' => $meta_tab,
								'meta_cob' => $meta_cob,
								'data_edt' => date('Y-m-d H:i:s'),
								'mes' => $mes
							);
							$getRanking = $this->metas_model->getMetas($funcionario->id, $mes);
							if($getRanking) {
								//editar
								$getRanking = $this->metas_model->edit_metas($getRanking->id, $dadosRanking);
							}
							else {
								//inserir
								$getRanking = $this->metas_model->insert_metas($dadosRanking);
							}
						}
					}
				}
			}

			$this->session->set_userdata(array('importMetas' => true));

			redirect( 'admin/metas' );
	}
}


	public function uploader($file) {

		$this->load->library('Php_excel');

		$file = './uploads/'.$file;

		//read file from path
		$objPHPExcel = PHPExcel_IOFactory::load($file);

		$totalSheets = $objPHPExcel->getSheetCount();

		$retorno = array();

		//get only the Cell Collection
		$cell_collection = $objPHPExcel->getSheet(0)->getCellCollection();

		// extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
	    $column = $objPHPExcel->getSheet(0)->getCell($cell)->getColumn();
	    $row = $objPHPExcel->getSheet(0)->getCell($cell)->getRow();

			if(is_null($objPHPExcel->getSheet(0)->getCell($cell)->getOldCalculatedValue())){
				$data_value = $objPHPExcel->getSheet(0)->getCell($cell)->getValue();
			}else{
				$data_value = $objPHPExcel->getSheet(0)->getCell($cell)->getOldCalculatedValue();
			}

		  //header will/should be in row 1 only. of course this can be modified to suit your need.
		  if ($row > 1) {
		      $arr_data[$row][$column] = $data_value;
		  }
		}

		$retorno = $arr_data;

		$arr_data = array();

		return $retorno;
	}

}
