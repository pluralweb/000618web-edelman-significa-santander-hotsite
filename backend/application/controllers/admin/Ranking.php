<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ranking extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'ranking';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');

    $data['ranking'] = $this->ranking_model->getAllRanking();

		foreach ($data['ranking'] as $r) {
			$r->mes = $this->nomeMes($r->mes);
		}

		$data['importRanking'] = $this->session->userdata('importRanking');
		$this->session->unset_userdata('importRanking');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/ranking_listagem', $data);
	}

	public function importar() {

		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'ranking';
		$dataH['subsessao'] = 'importar';
		$dataH['nome'] = $this->session->userdata('nome');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/ranking_importar');
	}


	public function import_data(){

		$mes = $this->input->post('mes');

		$config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'xls|xlsx';

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('arquivo')){

    //retrun errors
    $error = array('error' => $this->upload->display_errors());

   // $this->load->view('welcome_message',array('error' => $error));
    print_r($error);

    }
    else{

    	$uploaded_data = $this->upload->data();
      $file_name = $uploaded_data['file_name'];

    	$data = $this->uploader($file_name);

      //delete uploaded file for the server
      $file_path= './uploads/'.$file_name;
      unlink($file_path);

			if (true) {
				foreach ($data['ranking'] as $user_data) {
					// echo "<pre>";print_r($user_data);die;
					if(isset($user_data[0])) {
						$anomes = $user_data[0];
						$matricula = ltrim($user_data[7], '0');
						$filial = $user_data[4];
						$rede = $user_data[2];
						$elegivel = $user_data[12] ;
						$pontuacao = $user_data[13];
						$colocacao = $user_data[14];
						$pontos_primeiro = $user_data[15];
						$pontos_anterior = $user_data[17];
						$pontos_depois = $user_data[19];

						$funcionario = $this->funcionarios_model->getByMatricula($matricula);

						//Cargo
						$getCargo = $this->funcionarios_model->get_cargoNome($user_data[9]);
						if(!$getCargo) {
							$dadosCargo = array('nome' => $user_data[9]);
							$idCargo = $this->funcionarios_model->insert_cargo($dadosCargo);
						}
						else {
							$idCargo = $getCargo->id;
						}

						//Filial
						$getFilial = $this->funcionarios_model->get_filialNome($user_data[9]);
						if(!$getFilial) {
							$dadosCargo = array('nome' => $filial);
							$idFilial = $this->funcionarios_model->insert_filial($dadosCargo);
						}
						else {
							$idFilial = $getFilial->id;
						}

						//Rede
						$getRede = $this->funcionarios_model->get_redeNome($rede);
						if(!$getFilial) {
							$dadosCargo = array('nome' => $rede);
							$idRede = $this->funcionarios_model->insert_rede($dadosCargo);
						}
						else {
							$idRede = $getRede->id;
						}

						// //Sub-Cargo
						// $getsubCargo = $this->funcionarios_model->get_subcargoNome($user_data[12]);
						// if(!$getsubCargo) {
						// 	$dadossubCargo = array('nome' => $user_data[12]);
						// 	$idsubCargo = $this->funcionarios_model->insert_subcargo($dadossubCargo);
						// } else {
						// 	$idsubCargo = $getsubCargo->id;
						// }

						// //Porte
						// $getPorte = $this->funcionarios_model->get_porteNome($user_data[14]);
						// if(!$getPorte) {
						// 	$dadosPorte = array('nome' => $user_data[14]);
						// 	$idPorte = $this->funcionarios_model->insert_porte($dadosPorte);
						// } else {
						// 	$idPorte = $getPorte->id;
						// }

						if($funcionario) {
							$this->funcionarios_model->edit_funcionario($funcionario->id, array('id_rede' => $idRede, 'id_filial'=> $idFilial));
							//Ranking
							$dadosRanking = array(
							'id_funcionario' => $funcionario->id,
							'anomes' => $anomes,
							'matricula' => $matricula,
							'id_cargo' => $idCargo,
							'elegivel' => $elegivel,
							'pontuacao' => $pontuacao,
							'colocacao' => $colocacao,
							'pontos_primeiro' => $pontos_primeiro,
							'pontos_anterior' => $pontos_anterior,
							'pontos_depois' => $pontos_depois,
							'data_edt' => date('Y-m-d H:i:s'),
							'mes' => $mes
							);

							$getRanking = $this->ranking_model->getRanking($funcionario->id, $anomes);
							if($getRanking) {
								//editar
								$getRanking = $this->ranking_model->edit_ranking($getRanking->id, $dadosRanking);
							}
							else {
								//inserir
								$getRanking = $this->ranking_model->insert_ranking($dadosRanking);
							}
						}
					}
				}
			}

    	/*foreach ($data['ranking'] as $user_data) {


						if(isset($user_data[9])) {
							//echo $user_data[9]."<br>";
							$periodo = trim($user_data[0]);
							$matricula = trim($user_data[9]);
							$pontuacao = $user_data[16];
							$colocacao = $user_data[17];
							$pontos_primeiro = $user_data[18];
							$funcionario = $this->funcionarios_model->getByMatricula(trim($user_data[9]));
							if($funcionario) {
							//print_r($funcionario);
							} else {
								echo $user_data[9]."<br>";
								//echo "Nao";
							}
							//echo "<br>";

							//Sub-Cargo
							$getsubCargo = $this->funcionarios_model->get_subcargoNome($user_data[12]);
							if(!$getsubCargo) {
								$dadossubCargo = array('nome' => $user_data[12]);
								$idsubCargo = $this->funcionarios_model->insert_subcargo($dadossubCargo);
							} else {
								$idsubCargo = $getsubCargo->id;
							}


							if($funcionario) {
								//Ranking
								$dadosRankingPeriodo = array('id_periodo' => $periodo,
														'id_funcionario' => $funcionario->id,
														'matricula' => $user_data[9],
														'id_subcargo' => $idsubCargo,
														'pontuacao' => $user_data[16],
														'colocacao' => $user_data[17],
														'pontos_primeiro' => $user_data[18],
														'data_add' => date('Y-m-d H:i:s'));


								$getRankingPeriodo = $this->ranking_model->getRankingPeriodo($funcionario->id,$periodo);
								if($getRankingPeriodo) {
									//Desativar
									//$dadosStatus = array('status' => 0);
									//$getRankingAcumulado2 = $this->ranking_model->edit_rankingAcumulado($getRankingAcumulado->id, $dadosStatus);

									//editar
									$getRankingPeriodo = $this->ranking_model->edit_rankingPeriodo($getRankingPeriodo->id, $dadosRankingPeriodo);
								} else {
									//inserir
									$getRankingPeriodo = $this->ranking_model->insert_rankingPeriodo($dadosRankingPeriodo);
								}
							}
						}
        	}*/

			if (true) {

					foreach ($data['acumulado'] as $user_data) {
						if(isset($user_data[7])) {
							$matricula = ltrim($user_data[7], '0');
							$pontuacao = $user_data[13];
							$colocacao = $user_data[14];
							$pontos_primeiro = $user_data[15];
							$pontos_anterior = $user_data[17];
							$pontos_depois = $user_data[19];

							$funcionario = $this->funcionarios_model->getByMatricula($matricula);

							if($funcionario) {
							//print_r($funcionario);
							} else {
								// echo $user_data[9]."<br>";
								//echo "Nao";
							}

							if($funcionario) {
								//Ranking
								$dadosRankingAcumulado = array('id_funcionario' => $funcionario->id,
														'matricula' => $matricula,
														'pontuacao' => $pontuacao,
														'colocacao' => $colocacao,
														'pontos_primeiro' => $pontos_primeiro,
														'pontos_anterior' => $pontos_anterior,
														'pontos_depois' => $pontos_depois,
														'data_add' => date('Y-m-d H:i:s'),
														'mes' => $mes
													);


								$getRankingAcumulado = $this->ranking_model->getRankingAcumulado($funcionario->id);
								if($getRankingAcumulado) {
									//Desativar
									//$dadosStatus = array('status' => 0);
									//$getRankingAcumulado2 = $this->ranking_model->edit_rankingAcumulado($getRankingAcumulado->id, $dadosStatus);

									//editar
									$getRankingAcumulado = $this->ranking_model->edit_rankingAcumulado($getRankingAcumulado->id, $dadosRankingAcumulado);
								} else {
									//inserir
									$getRankingAcumulado = $this->ranking_model->insert_rankingAcumulado($dadosRankingAcumulado);
								}
							}
						}
        	}
				}

      }

			$this->session->set_userdata(array('importRanking' => true));
			redirect( 'admin/ranking' );
	}


	public function uploader($file) {

		$this->load->library('Php_excel');

		$file = './uploads/'.$file;

		//read file from path
		$objPHPExcel = PHPExcel_IOFactory::load($file);

		$totalSheets = $objPHPExcel->getSheetCount();

		$retorno = array();
		foreach ($objPHPExcel->getAllSheets() as $sheet) {
					 $sheets[$sheet->getTitle()] = $sheet->toArray();
		}
		$retorno['ranking'] = $sheets["Mensal"];
		$retorno['acumulado'] = $sheets["Acumulado"];

		//echo "<pre>";print_r($retorno);die();
		return $retorno;
	}

private function nomeMes($mes)
{
	switch ($mes) {
		case 9:
			$mes = 'Setembro';
			break;
		case 10:
			$mes = 'Outubro';
			break;
		case 11:
			$mes = 'Novembro';
			break;
		case 12:
			$mes = 'Dezembro';
			break;
	}

	return $mes;
}


}
