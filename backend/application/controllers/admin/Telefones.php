<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Telefones extends CI_Controller {

	function __construct()
	{
		 parent::__construct(); 
		 
	}
	
	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'telefones';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');	
        
        $data['telefones'] = $this->telefones_model->get_telefones();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/telefones_listagem', $data);
	}
	
	public function exportar() {
        $data = date('d-m-Y');
        $this->load->library('Excel');
        $cadastros = $this->telefones_model->get_telefones();

        $this->excel->to_excel($cadastros, 'telefones-' . $data);
    }
	
}
