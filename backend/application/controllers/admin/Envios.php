<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Envios extends CI_Controller {

	function __construct()
	{
		 parent::__construct(); 
		 
	}
	
	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'envios';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');	
        
        $envios = $this->cadastros_model->get_envios();
		
		$i=0;
		$data['envios'] = array();
		
		foreach($envios as $envio) {
			$data['envios'][$i] = $envio;
			$data['envios'][$i]->cadastro = $this->cadastros_model->getCadastro($envio->id_cadastro);
			if(isset($envio->id_area1)) {
			$data['envios'][$i]->area1 = $this->areas_model->getArea($envio->id_area1);
			}
			if(isset($envio->id_area2)) {
			$data['envios'][$i]->area2 = $this->areas_model->getArea($envio->id_area2);
			}
			$i++;
		}

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/envio_listagem', $data);
	}
	
	public function filtro()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'envios';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');	
        
		$dados = array('idade1' => $this->input->get('idade1'),
						'idade2' => $this->input->get('idade2'),
						'genero' => $this->input->get('genero'),
						'deficiente' => $this->input->get('deficiente'),
						'escolaridade' => $this->input->get('escolaridade'),
						'mudanca' => $this->input->get('mudanca'),
						'viagens' => $this->input->get('viagens'),
						'cidade' => $this->input->get('cidade'),
						'estado' => $this->input->get('estado'),
						'area' => $this->input->get('area'),
						'nivel' => $this->input->get('nivel'),
						);
		
        $envios = $this->cadastros_model->buscaFiltrosEnvios($dados);
		
		$i=0;
		$data['envios'] = array();
		
		foreach($envios as $envio) {
			$data['envios'][$i] = $envio;
			$data['envios'][$i]->cadastro = $this->cadastros_model->getCadastro($envio->id_cadastro);
			if(isset($envio->id_area1)) {
			$data['envios'][$i]->area1 = $this->areas_model->getArea($envio->id_area1);
			}
			if(isset($envio->id_area2)) {
			$data['envios'][$i]->area2 = $this->areas_model->getArea($envio->id_area2);
			}
			$i++;
		}
        $data['estados'] = $this->cadastros_model->getEstados();
        $data['cidades'] = $this->cadastros_model->getTodasCidades();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/envio_listagem', $data);
	}
	

}
