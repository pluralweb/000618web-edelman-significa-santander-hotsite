<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		 parent::__construct(); 
		 
	}
	
	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'home';
		$dataH['subsessao'] = '';
		$dataH['nome'] = $this->session->userdata('nome');	
        
		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/home');
	}
	
	public function login()
	{
		if($this->session->userdata('id')) redirect('admin/home');
		$this->load->view('admin/login');
	}

	public function invalid(){

		$data['invalid'] = TRUE;

		$this->load->view( 'admin/login', $data );
	}
	
	public function dologin(){
		
		if ($this->input->post()){
			$user = $this->input->post('login');
			$pass = $this->input->post('senha');

			$userinfo = $this->usuarios_model->get_login($user, $pass);

			if (!$userinfo)
				redirect( 'admin/home/invalid' );


			$this->session->set_userdata(array('logged' => true, 'id' => $userinfo[0]->id, 'nome' => $userinfo[0]->nome));
			redirect( 'admin/home' );
		}
	}
	
	public function logout(){
		
		$this->session->unset_userdata('logged');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('nome');
		$this->input->set_cookie('logged', '');
		session_destroy();

		redirect('admin/home/login');
	}

	public function perfil()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'perfil';
		$dataH['subsessao'] = '';
		$dataH['nome'] = $this->session->userdata('nome');
		
		$data['usuario'] = $this->usuarios_model->getUsuario($this->session->userdata('id'));
		$data['sucesso'] = $this->input->get('editado');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/meu_perfil', $data);
	}
	
	public function edtUsuario() {
		
		$idUser = $this->session->userdata('id');
		
		$dataUsuario = array('nome' =>	$this->input->post('nome'),
							'usuario' => $this->input->post('usuario'),
							'data_edt' => date('Y-m-d H:i:s')
							);
		if($this->input->post('senha')!='') {
			$dataUsuario['senha'] = md5($this->input->post('senha'));
		}
		$this->usuarios_model->edit_user($idUser, $dataUsuario);

		redirect( 'admin/home/perfil/?editado=1' );

	}
	
	public function error404() {
		$this->load->view('404');
	}
	
}
