<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'usuarios';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');
 		$dataH['permissoes'] = $this->session->userdata('permissoes');

    $data['usuarios'] = $this->usuarios_model->get_usuarios();
		$i=0;

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/usuarios_listagem', $data);
	}

	public function inserir()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'usuarios';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/usuarios_inserir', $data);
	}

	public function addUsuario() {

		$data = array(
			'nome' => $this->input->post('nome'),
			'usuario' => $this->input->post('usuario'),
			'senha' => md5($this->input->post('senha')),
			'data_add' => date('Y-m-d H:i:s')
		);

		$idCadastrado = $this->usuarios_model->insert_usuario($data);

		redirect( 'admin/usuarios' );
	}


	public function editar()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$id = $this->uri->segment(4);

		$dataH['sessao'] = 'usuarios';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');

		$data['usuario'] = $this->usuarios_model->getUsuario($id);

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/usuarios_editar', $data);
	}

	public function edtUsuario() {

		$id = $this->input->post('id');
		$senha = $this->input->post('senha');
		$data = array(
			'nome' => $this->input->post('nome'),
			'usuario' => $this->input->post('usuario'),
			'data_edt' => date('Y-m-d H:i:s')
		);
		if(!empty($senha)){
			$data['senha'] = md5($this->input->post('senha'));
		}

		$idCadastrado = $this->usuarios_model->edit_user($id, $data);

		// $this->usuarios_model->delete_permissoes($id);

		redirect( 'admin/usuarios' );
	}

	public function excluir() {

		$id = $this->uri->segment(4);

		$data = array(
			'status' => 0
		);

		$idCadastrado = $this->usuarios_model->edit_user($id, $data);
		redirect( 'admin/usuarios' );
	}


}
