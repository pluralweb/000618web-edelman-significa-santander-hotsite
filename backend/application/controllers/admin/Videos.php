<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'videos';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');

        $data['videos'] = $this->videos_model->get_videos();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/videos_listagem', $data);
	}

	public function editar()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'videos';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');

		$id = $this->uri->segment(4);

        $data['video'] = $this->videos_model->getVideo($id);

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/videos_editar', $data);
	}

	public function inserir()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'videos';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/videos_inserir');
	}

	//Adicionar
	public function addVideo() {

		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$video = ($this->input->post('midia') == 1) ? 1 : 0;

		$data = array(
			'video' => $video
		);
		if ($video == 1) {
			$data['link'] = $this->input->post('link');
		}else{
			if(!empty($_FILES['linkImg']['name'])) {
				$upload_data = $this->do_upload('linkImg',IMAGENS_FOLDER);
				if ($upload_data){
					$data['link'] = $upload_data;
				} else {
					print_r($upload_data);
					exit();
				}
			}
		}

		$idCadastrado = $this->videos_model->insert_video($data);


		redirect( '/admin/videos/index/' );

	}

	//Editar
	public function edtVideo() {

		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$id = $this->input->post('id');

		$video = ($this->input->post('midia') == 1) ? 1 : 0;

		$data = array(
			'video' => $video
		);
		if ($video == 1) {
			$data['link'] = $this->input->post('link');
		}else{
			if(!empty($_FILES['linkImg']['name'])) {
				$upload_data = $this->do_upload('linkImg',IMAGENS_FOLDER);
				if ($upload_data){
					$data['link'] = $upload_data;
				} else {
					print_r($upload_data);
					exit();
				}
			}
		}
		$this->videos_model->edit_video($id, $data);

		redirect( '/admin/videos/index/' );

	}

	//Excluir
	public function remover() {

		$idVideo = $this->uri->segment(4);

		$data = array(
			'status' => 0
		);

		$this->videos_model->edit_video($idVideo, $data);

		redirect( '/admin/videos/index/' );
	}

	private function do_upload($imagem, $folder, $w=false, $h=false){
		$this->load->library('upload');

		$config['upload_path'] 		= './' . $folder;
		$config['allowed_types'] 	= 'jpg|png';
		$config['max_size']			= '5120';
		$config['encrypt_name'] 	= TRUE;

		$this->upload->initialize($config);


		if ($this->upload->do_upload($imagem)){
			$upload_data = $this->upload->data();

			if($w && $h) {
				$image_config["image_library"] = "gd2";
				$image_config["source_image"] = './'.$folder.'/'.$upload_data['file_name'];
				$image_config['create_thumb'] = FALSE;
				$image_config['maintain_ratio'] = TRUE;
				$image_config['new_image'] = './'.$folder.'/'.$upload_data['file_name'];
				$image_config['quality'] = "100%";
				$image_config['width'] = $w;
				$image_config['height'] = $h;
				$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
				$image_config['master_dim'] = ($dim > 0)? "height" : "width";

				$this->load->library('image_lib');
				$this->image_lib->initialize($image_config);

				if(!$this->image_lib->resize()){ //Resize image
					return false;
				}else{
					$image_config['image_library'] = 'gd2';
					$image_config['source_image'] = './'.$folder.'/'.$upload_data['file_name'];
					$image_config['new_image'] = './'.$folder.'/'.$upload_data['file_name'];
					$image_config['quality'] = "100%";
					$image_config['maintain_ratio'] = FALSE;
					$image_config['width'] = $w;
					$image_config['height'] = $h;
					$image_config['x_axis'] = '0';
					$image_config['y_axis'] = '0';

					$this->image_lib->clear();
					$this->image_lib->initialize($image_config);

					if (!$this->image_lib->crop()){
							return false;
					}else{
						return $upload_data['file_name'];
					}
				}
			} else {
				return $upload_data['file_name'];
			}
		} else {
			return false;
		}
	}

}
