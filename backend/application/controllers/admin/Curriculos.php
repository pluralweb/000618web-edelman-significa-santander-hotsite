<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curriculos extends CI_Controller {

	function __construct()
	{
		 parent::__construct(); 
		 
	}
	
	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'curriculos';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');	
        
        $data['curriculos'] = $this->cadastros_model->get_curriculos();
        $data['estados'] = $this->cadastros_model->getEstados();
        $data['cidades'] = $this->cadastros_model->getTodasCidades();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/curriculos_listagem', $data);
	}
	
	public function filtro()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'curriculos';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');	
        
		$dados = array('idade1' => $this->input->get('idade1'),
						'idade2' => $this->input->get('idade2'),
						'genero' => $this->input->get('genero'),
						'deficiente' => $this->input->get('deficiente'),
						'escolaridade' => $this->input->get('escolaridade'),
						'mudanca' => $this->input->get('mudanca'),
						'viagens' => $this->input->get('viagens'),
						'cidade' => $this->input->get('cidade'),
						'estado' => $this->input->get('estado'),
						'area' => $this->input->get('area'),
						'nivel' => $this->input->get('nivel'),
						);
		
        $data['curriculos'] = $this->cadastros_model->buscaFiltros($dados);
        $data['estados'] = $this->cadastros_model->getEstados();
        $data['cidades'] = $this->cadastros_model->getTodasCidades();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/curriculos_listagem', $data);
	}
	
	public function detalhes()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');
		
		$id = $this->uri->segment(4);

		$dataH['sessao'] = 'curriculos';
		$dataH['subsessao'] = 'detalhes';
		$dataH['nome'] = $this->session->userdata('nome');	
        
        $data['curriculo'] = $this->cadastros_model->getCadastro($id);
        $data['pessoais'] = $this->cadastros_model->getPessoais($id);
        $data['contato'] = $this->cadastros_model->getContato($id);
		if(isset($data['contato']->estado)) {
        $data['estado'] = $this->cadastros_model->getEstado($data['contato']->estado);
		}
		if(isset($data['contato']->cidade)) {
        $data['cidade'] = $this->cadastros_model->getCidade($data['contato']->cidade);
		}
        $data['anexo'] = $this->cadastros_model->getCurriculo($id);
        $data['objetivo'] = $this->cadastros_model->getProfissional($id);
		//regiao_interesse_raio==estado
		//regiao_interesse_ref==cidade
		if(isset($data['objetivo']->regiao_interesse_raio)) {
        $data['objestado'] = $this->cadastros_model->getEstado($data['objetivo']->regiao_interesse_raio);
		}
		if(isset($data['objetivo']->regiao_interesse_ref)) {
        $data['objcidade'] = $this->cadastros_model->getCidade($data['objetivo']->regiao_interesse_ref);
		}

        $areas = $this->cadastros_model->getConhecimento($id);
		$cont=0;
		foreach($areas as $ar) {
			$data['areas'][$cont] = $ar;
			if(isset($ar->area)) {
			$data['areas'][$cont]->nomeArea = $this->areas_model->getArea($ar->area);
			}
			$cont++;
		}
		
        $data['experiencia'] = $this->cadastros_model->getHistorico($id);
        $trajetoria = $this->cadastros_model->getTrajetoria($id);
		$i=0;
		$data['trajetoria'] = array();
		foreach($trajetoria as $traj) {
			$data['trajetoria'][$i] = $traj;
			$data['trajetoria'][$i]->cargos = $this->cadastros_model->getTrajetoriaOcupacoes($traj->id);
			$i++;
		}
        $data['escolaridade'] = $this->cadastros_model->getFormacao($id);
        $data['idiomas'] = $this->cadastros_model->getIdiomas($id);
        $data['formacao'] = $this->cadastros_model->getCursos($id);
		

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/curriculos_detalhes', $data);
	}
	
	
	
}
