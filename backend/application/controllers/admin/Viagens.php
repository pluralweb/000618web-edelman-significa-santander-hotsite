<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viagens extends CI_Controller {

	function __construct()
	{
		 parent::__construct(); 
		 
	}
	
	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'viagens';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');	
        
        $data['viagem'] = $this->Viagem_model->getViagens();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/viagens_listagem', $data);
	}
	
	public function detalhe() {
        if(!$this->session->userdata('id')) redirect('admin/home/login');
        
        $id = $this->uri->segment(4);

        $dataH['sessao'] = 'viagens';
		$dataH['subsessao'] = 'detalhe';
		$dataH['nome'] = $this->session->userdata('nome');
        
        $data['cadastros'] = $this->Viagem_model->getViagensById($id);
        
        $this->load->view('admin/header', $dataH);
        $this->load->view('admin/viagens_detalhes', $data);
    }
    
    public function exportar() {
        $data = date('d-m-Y');
        $this->load->library('Excel');
        $cadastros = $this->Viagem_model->getViagens();
        $this->excel->to_excel($cadastros, 'Viagens-' . $data);
    }
	
}
