<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionarios extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function index()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'funcionarios';
		$dataH['subsessao'] = 'listagem';
		$dataH['nome'] = $this->session->userdata('nome');

    $data['funcionarios'] = $this->funcionarios_model->get_funcionarios();
    // echo'<pre>';print_r($data['funcionarios']);die;
		$data['importFunc'] = $this->session->userdata('importFunc');
		$this->session->unset_userdata('importFunc');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/funcionarios_listagem', $data);
	}

	public function importar() {

		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'funcionarios';
		$dataH['subsessao'] = 'importar';
		$dataH['nome'] = $this->session->userdata('nome');

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/funcionarios_importar');
	}

	public function import_data(){

		$config['upload_path'] = './uploads/';
				$config['allowed_types'] = 'xls|xlsx';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('arquivo')){

				//retrun errors
				$error = array('error' => $this->upload->display_errors());

			 // $this->load->view('welcome_message',array('error' => $error));
				print_r($error);

				}
				else{

					$uploaded_data = $this->upload->data();
						$file_name = $uploaded_data['file_name'];

					$data = $this->uploader($file_name);

						//delete uploaded file for the server
						$file_path= './uploads/'.$file_name;
						unlink($file_path);

						// $this->funcionarios_model->desativa_funcionarios();
	
						unset($data[1]);

						$this->db->where('campanha', '');
						$this->db->delete(FUNCIONARIOS_TABLE);		

						$data[3]['I']; // colunha campanha, identificando o tipo de campanha.

						if (!empty($data[3]['I'])) {
							$this->db->where('campanha', $data[3]['I']);
	  					$this->db->delete(FUNCIONARIOS_TABLE);
						}	

					// die;

					foreach ($data as $user_data) {

						$matricula = $user_data['C'];
						$nome = $user_data['D'];
						$cpf = str_pad($user_data['H'], 11, "0", STR_PAD_LEFT);
						$email = $user_data['F'];
						$rede_comercial = $user_data['A'];
					// 	$rede = $user_data['H'];
						$regional = $user_data['B'];
					// $filial = $user_data['J'];
						$campanha = $user_data['I'];

						//RedeComercial
						$getRedeComercial = $this->funcionarios_model->get_redeComercialNome($rede_comercial);
						if(!$getRedeComercial) {
							$dadosRedeComercial = array('nome' => $rede_comercial);
							$idRedeComercial = 	$this->funcionarios_model->insert_redeComercial($dadosRedeComercial);
						} else {
							$idRedeComercial = $getRedeComercial->id;
						}

						// //Rede -> Não é necessário
						// $getRede = $this->funcionarios_model->get_redeNome($rede);
						// if(!$getRede) {
						// 	$dadosRede = array('nome' => $rede);
						// 	$idRede = $this->funcionarios_model->insert_rede($dadosRede);
						// } else {
						// 	$idRede = $getRede->id;
						// }

						//Regional
						$getRegional = $this->funcionarios_model->get_regionalNome($regional);
						if(!$getRegional) {
							$dadosRegional = array('nome' => $regional);
							$idRegional = $this->funcionarios_model->insert_regional($dadosRegional);
						} else {
							$idRegional = $getRegional->id;
						}

						// //Filial -> Não é necessário
						// $getFilial = $this->funcionarios_model->get_filialNome($filial);
						// if(!$getFilial) {
						// 	$dadosFilial = array('nome' => $filial);
						// 	$idFilial = $this->funcionarios_model->insert_filial($dadosFilial);
						// } else {
						// 	$idFilial = $getFilial->id;
						// }
						
						//Funcionário
						$dadosFuncionario = array('nome' => $nome,
													'matricula' => $matricula,
													'cpf' => $cpf,
													'email' => $email,
													'id_rede_comercial' => $idRedeComercial,
													'id_regional' => $idRegional,
													'campanha' => $campanha,		
													'status'	=> 1
												);


						$getFuncionario = $this->funcionarios_model->get_funcionarioCpf($cpf,$campanha);



						if(!$getFuncionario) {
							$dadosFuncionario['data_add'] = date('Y-m-d H:i:s');
							$idFuncionario = $this->funcionarios_model->insert_funcionario($dadosFuncionario);
						} else {
							$dadosFuncionario['data_edt'] = date('Y-m-d H:i:s');
							$dadosFuncionario['status'] = 1;							
							$this->funcionarios_model->edit_funcionario($getFuncionario->id, $dadosFuncionario);
						}
					}


					$this->session->set_userdata(array('importFunc' => true));

					redirect( 'admin/funcionarios' );


				}
	}

	public function inserir() {

		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'funcionarios';
		$dataH['subsessao'] = 'inserir';
		$dataH['nome'] = $this->session->userdata('nome');

		$data['regional'] = $this->funcionarios_model->get_regional_all();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/funcionarios_inserir',$data);
	}

	public function addfuncionario() {

		$data = array(
			'nome' => $this->input->post('nome'),
			'cpf' => $this->input->post('cpf'),
			'matricula' => $this->input->post('matricula'),
			'email' => $this->input->post('email'),
			'id_rede_comercial' => $this->input->post('id_rede_comercial'),
			'campanha' => $this->input->post('campanha'),			
		);

		$id = $this->funcionarios_model->insert_funcionario($data);

		redirect('admin/funcionarios/index/');
	}

	public function uploader($file){

		$this->load->library('Php_excel');

		$file = './uploads/'.$file;

		//read file from path
		$objPHPExcel = PHPExcel_IOFactory::load($file);

		//get only the Cell Collection
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
		    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
		    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
		    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();



		    //header will/should be in row 1 only. of course this can be modified to suit your need.
		    if ($row > 1) {
		        $arr_data[$row][$column] = $data_value;

		    }
		}



		return $arr_data;
	}



}
