<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends CI_Controller {

	function __construct()
	{
		 parent::__construct();

	}

	public function geral_cdc()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'configuracoes_cdc';
		$dataH['subsessao'] = 'geral_cdc';
		$dataH['nome'] = $this->session->userdata('nome');

		$info = $this->configuracoes_model->getAll('CDC');

		$info->veiculo = date('d/m/Y', strtotime($info->veiculo));
		$info->cdc = date('d/m/Y', strtotime($info->cdc));

		$data['update'] = (!is_null($info)) ? $info->id : null;

		$data['info'] = $info;

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/config_geral_cdc', $data);
	}


	public function faq_cdc()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'configuracoes_cdc';
		$dataH['subsessao'] = 'faq_cdc';
		$dataH['nome'] = $this->session->userdata('nome');

		$info = $this->configuracoes_model->getAllFaq('CDC');

		$data['faq'] = $info;
		$data['config'] = $this->configuracoes_model->getAll();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/config_faq_cdc', $data);
	}

	public function addFaq_cdc() {
		$update = $this->input->post('update');

		$dataF['mostrarQ'] = $this->input->post('mostrarQ');
		$dataF['email'] = $this->input->post('email');
		$dataF['campanha'] = 'CDC';

		$this->configuracoes_model->edit(1, $dataF);

		$id = $this->input->post('id');
		$pergunta = $this->input->post('pergunta');
		$resposta = $this->input->post('resposta');
		$img = $_FILES['imgfaq'];

		// $this->db->query('TRUNCATE TABLE '.FAQ_TABLE);

		foreach ($pergunta as $key => $value) {
			$idLoop = $id[$key];
			$data['pergunta'] = $pergunta[$key];
			$data['resposta'] = $resposta[$key];
			$data['campanha'] = 'CDC';

			if (empty($data['pergunta']) || empty($data['resposta'])) {
				continue;
			}

			$_FILES['imgfaq']['name'] = $img['name'][$key];
			$_FILES['imgfaq']['type'] = $img['type'][$key];
			$_FILES['imgfaq']['tmp_name'] = $img['tmp_name'][$key];
			$_FILES['imgfaq']['error'] = $img['error'][$key];
			$_FILES['imgfaq']['size'] = $img['size'][$key];

			if (!empty($img) && !empty($img['name'][$key])) {
				if($_FILES['imgfaq']['name']) {
					$upload_data = $this->do_upload('imgfaq',IMAGENS_FOLDER);
					if ($upload_data){
						$data['imgfaq'] = $upload_data;
					} else {
						print_r($upload_data);
						exit();
					}
				}
			}else{
				unset($data['imgfaq']);
			}


			if(!isset($idLoop)){
				$this->configuracoes_model->insert_faq($data);
			}else{
				if(strpos($idLoop, 'del') !== FALSE){
					$idDel = substr($idLoop, 3, strlen($idLoop));
					$this->configuracoes_model->deleteFaq($idDel);
				}else{
					$this->configuracoes_model->editFaq($idLoop, $data);
				}
			}
		}

		redirect('admin/configuracoes/faq_cdc');
	}
	public function addGerais_cdc() {
		$update = $this->input->post('update');

		$info = $this->configuracoes_model->getAll();

		$cdc = implode('-', array_reverse(explode('/', $this->input->post('cdc')))) . ' 00:00:00';

		$veiculo = implode('-', array_reverse(explode('/', $this->input->post('veiculo')))) . ' 00:00:00';

		$data = array(
			'titulo' => $this->input->post('titulo'),
			'descricao' => $this->input->post('descricao'),
			'regulamento' => $this->input->post('config'),
			'linkTwitter' => $this->input->post('linkTwitter'),
			'linkYoutube' => $this->input->post('linkYoutube'),
			'linkFacebook' => $this->input->post('linkFacebook'),
			'linkPromocao' => $this->input->post('linkPromocao'),
			'autoplay' => ($this->input->post('autoplay') == 'on') ? 1 : 0,
			'youtube' => ($this->input->post('youtube') == 'on') ? 1 : 0,
			'cdc' => $cdc,
			'veiculo' => $veiculo,
			'plus' => $this->input->post('plus')
		);

		if(!empty($_FILES['imgTwitter']['name'])) {
			$upload_data = $this->do_upload('imgTwitter',IMAGENS_FOLDER);
			if ($upload_data){
				unlink(IMAGENS_FOLDER.$info->imgTwitter);
				$data['imgTwitter'] = $upload_data;
			} else {
				print_r($upload_data);
				exit();
			}
		}

	if(!empty($_FILES['linkVideo']['name'])) {
			$upload_data = $this->do_upload_video('linkVideo',VIDEOS_FOLDER);
			if ($upload_data){
				unlink(VIDEOS_FOLDER.$info->linkVideo);
					$data['linkPromocao'] = $upload_data;
			} else {
				print_r($upload_data);
				exit();
			}
		}

		if(!empty($_FILES['imgFacebook']['name'])) {
			$upload_data = $this->do_upload('imgFacebook', IMAGENS_FOLDER);
			if ($upload_data){
				unlink(IMAGENS_FOLDER.$info->imgFacebook);
				$data['imgFacebook'] = $upload_data;
			} else {
				print_r($upload_data);
				exit();
			}
		}

		if(empty($update)){
			$this->configuracoes_model->insert($data);
		}else{
		  $this->configuracoes_model->edit($update, $data);
		}

		redirect('admin/configuracoes/geral_cdc');
	}

	private function do_upload($imagem, $folder, $w=false, $h=false){
		$this->load->library('upload');

		$config['upload_path'] 		= './' . $folder;
		$config['allowed_types'] 	= 'jpg|png';
		$config['max_size']			= '5120';
		$config['encrypt_name'] 	= TRUE;

		$this->upload->initialize($config);


		if ($this->upload->do_upload($imagem)){
			$upload_data = $this->upload->data();

			if($w && $h) {
				$image_config["image_library"] = "gd2";
				$image_config["source_image"] = './'.$folder.'/'.$upload_data['file_name'];
				$image_config['create_thumb'] = FALSE;
				$image_config['maintain_ratio'] = TRUE;
				$image_config['new_image'] = './'.$folder.'/'.$upload_data['file_name'];
				$image_config['quality'] = "100%";
				$image_config['width'] = $w;
				$image_config['height'] = $h;
				$dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
				$image_config['master_dim'] = ($dim > 0)? "height" : "width";

				$this->load->library('image_lib');
				$this->image_lib->initialize($image_config);

				if(!$this->image_lib->resize()){ //Resize image
					return false;
				}else{
					$image_config['image_library'] = 'gd2';
					$image_config['source_image'] = './'.$folder.'/'.$upload_data['file_name'];
					$image_config['new_image'] = './'.$folder.'/'.$upload_data['file_name'];
					$image_config['quality'] = "100%";
					$image_config['maintain_ratio'] = FALSE;
					$image_config['width'] = $w;
					$image_config['height'] = $h;
					$image_config['x_axis'] = '0';
					$image_config['y_axis'] = '0';

					$this->image_lib->clear();
					$this->image_lib->initialize($image_config);

					if (!$this->image_lib->crop()){
							return false;
					}else{
						return $upload_data['file_name'];
					}
				}
			} else {
				return $upload_data['file_name'];
			}
		} else {
			return false;
		}
	}
	private function do_upload_video($imagem, $folder){
		$upload_data = array();
		$this->load->library('upload');

		$config['upload_path'] 		= './' . $folder;
		$config['allowed_types'] = 'gif|jpg|png|mp4';
		$config['max_size']	= '1000000000000000'; // whatever you need
		$config['encrypt_name'] 	= TRUE;

		$this->upload->initialize($config);


		if ($this->upload->do_upload($imagem)){
				$upload_data = $this->upload->data();
				return $upload_data['file_name'];
		} else {
			return false;
		}
	}

	public function geral_veiculos()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'configuracoes_veiculos';
		$dataH['subsessao'] = 'geral_veiculos';
		$dataH['nome'] = $this->session->userdata('nome');

		$info = $this->configuracoes_model->getAll('VEICULOS');

		$info->veiculo = date('d/m/Y', strtotime($info->veiculo));
		$info->cdc = date('d/m/Y', strtotime($info->cdc));

		$data['update'] = (!is_null($info)) ? $info->id : null;

		$data['info'] = $info;

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/config_geral_veiculos', $data);
	}


	public function faq_veiculos()
	{
		if(!$this->session->userdata('id')) redirect('admin/home/login');

		$dataH['sessao'] = 'configuracoes_veiculos';
		$dataH['subsessao'] = 'faq_veiculos';
		$dataH['nome'] = $this->session->userdata('nome');

		$info = $this->configuracoes_model->getAllFaq('VEICULOS');

		$data['faq'] = $info;
		$data['config'] = $this->configuracoes_model->getAll();

		$this->load->view('admin/header', $dataH);
		$this->load->view('admin/config_faq_veiculos', $data);
	}

	public function addFaq_veiculos() {
		$update = $this->input->post('update');

		$dataF['mostrarQ'] = $this->input->post('mostrarQ');
		$dataF['email'] = $this->input->post('email');
		$dataF['campanha'] = 'VEICULOS';

		$this->configuracoes_model->edit(1, $dataF);

		$id = $this->input->post('id');
		$pergunta = $this->input->post('pergunta');
		$resposta = $this->input->post('resposta');
		$img = $_FILES['imgfaq'];

		// $this->db->query('TRUNCATE TABLE '.FAQ_TABLE);

		foreach ($pergunta as $key => $value) {
			$idLoop = $id[$key];
			$data['pergunta'] = $pergunta[$key];
			$data['resposta'] = $resposta[$key];
			$data['campanha'] = 'VEICULOS';

			if (empty($data['pergunta']) || empty($data['resposta'])) {
				continue;
			}

			$_FILES['imgfaq']['name'] = $img['name'][$key];
			$_FILES['imgfaq']['type'] = $img['type'][$key];
			$_FILES['imgfaq']['tmp_name'] = $img['tmp_name'][$key];
			$_FILES['imgfaq']['error'] = $img['error'][$key];
			$_FILES['imgfaq']['size'] = $img['size'][$key];

			if (!empty($img) && !empty($img['name'][$key])) {
				if($_FILES['imgfaq']['name']) {
					$upload_data = $this->do_upload('imgfaq',IMAGENS_FOLDER);
					if ($upload_data){
						$data['imgfaq'] = $upload_data;
					} else {
						print_r($upload_data);
						exit();
					}
				}
			}else{
				unset($data['imgfaq']);
			}


			if(!isset($idLoop)){
				$this->configuracoes_model->insert_faq($data);
			}else{
				if(strpos($idLoop, 'del') !== FALSE){
					$idDel = substr($idLoop, 3, strlen($idLoop));
					$this->configuracoes_model->deleteFaq($idDel);
				}else{
					$this->configuracoes_model->editFaq($idLoop, $data);
				}
			}
		}

		redirect('admin/configuracoes/faq_veiculos');
	}
	public function addGerais_veiculos() {
		$update = $this->input->post('update');

		$info = $this->configuracoes_model->getAll();

		$cdc = implode('-', array_reverse(explode('/', $this->input->post('cdc')))) . ' 00:00:00';

		$veiculo = implode('-', array_reverse(explode('/', $this->input->post('veiculo')))) . ' 00:00:00';

		$data = array(
			'titulo' => $this->input->post('titulo'),
			'descricao' => $this->input->post('descricao'),
			'regulamento' => $this->input->post('config'),
			'linkTwitter' => $this->input->post('linkTwitter'),
			'linkYoutube' => $this->input->post('linkYoutube'),
			'linkFacebook' => $this->input->post('linkFacebook'),
			'linkPromocao' => $this->input->post('linkPromocao'),
			'autoplay' => ($this->input->post('autoplay') == 'on') ? 1 : 0,
			'youtube' => ($this->input->post('youtube') == 'on') ? 1 : 0,
			'cdc' => $cdc,
			'veiculo' => $veiculo,
			'plus' => $this->input->post('plus')
		);

		if(!empty($_FILES['imgTwitter']['name'])) {
			$upload_data = $this->do_upload('imgTwitter',IMAGENS_FOLDER);
			if ($upload_data){
				unlink(IMAGENS_FOLDER.$info->imgTwitter);
				$data['imgTwitter'] = $upload_data;
			} else {
				print_r($upload_data);
				exit();
			}
		}

	if(!empty($_FILES['linkVideo']['name'])) {
			$upload_data = $this->do_upload_video('linkVideo',VIDEOS_FOLDER);
			if ($upload_data){
				unlink(VIDEOS_FOLDER.$info->linkVideo);
					$data['linkPromocao'] = $upload_data;
			} else {
				print_r($upload_data);
				exit();
			}
		}

		if(!empty($_FILES['imgFacebook']['name'])) {
			$upload_data = $this->do_upload('imgFacebook', IMAGENS_FOLDER);
			if ($upload_data){
				unlink(IMAGENS_FOLDER.$info->imgFacebook);
				$data['imgFacebook'] = $upload_data;
			} else {
				print_r($upload_data);
				exit();
			}
		}

		if(empty($update)){
			$this->configuracoes_model->insert($data);
		}else{
		  $this->configuracoes_model->edit($update, $data);
		}

		redirect('admin/configuracoes/geral_veiculos');
	}
}

