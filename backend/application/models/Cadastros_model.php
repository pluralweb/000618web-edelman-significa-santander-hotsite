<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros_model extends CI_Model {

	/*public function get_login($user, $password){
		return $this->db->select("id,nome,usuario")
						->where('usuario', $user)
						->where('senha',  md5($password))
						->get(CADASTROS_TABLE)
						->result();
	}*/
	
	//Filtro
	public function buscaFiltros($dados) {
		$sql = "SELECT c.*
				FROM ".CADASTROS_TABLE." AS c
				";
		$where = false;
		if($dados['genero'] || $dados['deficiente']) {
			$sql .= " INNER JOIN ".PESSOAIS_TABLE." AS p
					ON p.id_cadastro=c.id";
			
			if($dados['genero']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "p.sexo='".$dados['genero']."'";
			}
			
			if($dados['deficiente']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "p.deficiencia='".$dados['deficiente']."'";
			}
		}
		
		if($dados['escolaridade']) {
			$sql .= " INNER JOIN ".FORMACAO_TABLE." AS f
					ON f.id_cadastro=c.id";
			
			if(!$where) {
				$where .= ' WHERE ';
			} else {
				$where .= ' && ';
			}
			
			$where .= "f.nivel_escolaridade='".$dados['escolaridade']."'";
		}

		if($dados['mudanca'] || $dados['viagens']) {
			$sql .= " INNER JOIN ".PROFISSIONAIS_TABLE." AS prof
					ON prof.id_cadastro=c.id";
			
			if($dados['mudanca']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "prof.outra_cidade='".$dados['mudanca']."'";
			}
			
			if($dados['viagens']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "prof.viagem='".$dados['viagens']."'";
			}
		}

		if($dados['cidade'] || $dados['estado']) {
			$sql .= " INNER JOIN ".CONTATO_TABLE." AS cont
					ON cont.id_cadastro=c.id";
			
			if($dados['cidade']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "cont.cidade='".$dados['cidade']."'";
			}
			
			if($dados['estado']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "cont.estado='".$dados['estado']."'";
			}
		}

		if($dados['area'] || $dados['nivel']) {
			$sql .= " INNER JOIN ".AREAS_TABLE." AS ar
					ON ar.id_cadastro=c.id";
			
			if($dados['area']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "ar.area='".$dados['area']."'";
			}
			
			if($dados['nivel']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "ar.nivel='".$dados['nivel']."'";
			}
		}

		if($dados['idade1'] && $dados['idade2']) {
			
			if(!$where) {
				$where .= ' WHERE ';
			} else {
				$where .= ' && ';
			}
			$ano = date('Y')-$dados['idade1'];
			$ano2 = date('Y')-$dados['idade2'];
			$where .= "c.nascimento<='".$ano.'-'.date('m').'-'.date('d')."' && c.nascimento>='".$ano2.'-'.date('m').'-'.date('d')."'";
		}
		
		
		$sql = $sql.$where." GROUP BY c.id ORDER BY c.nome";
		
		return $this->db->query($sql)->result();
		
	}
	 
	//Cadastro
	public function count_total() {
		return $this->db->count_all(CADASTROS_TABLE);
	}
	
	public function getByCpf($cpf) {
	   return $this->db->query("SELECT * 
								FROM ".CADASTROS_TABLE."
								WHERE cpf='".$cpf."'")->row();
	}
	
	public function get_login($user, $pass) {
	   return $this->db->query("SELECT * 
								FROM ".CADASTROS_TABLE."
								WHERE cpf='".$user."' && senha=md5('".$pass."')")->result();
	}
	
   public function get_curriculos() {
	   return $this->db->query("SELECT * 
								FROM ".CADASTROS_TABLE."
								ORDER BY id DESC")->result();
   }

	public function insert_cadastro($data){
		
		$this->db->insert(CADASTROS_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_cadastro($idUser, $data){
		$this->db->where('id', $idUser);
		$this->db->update(CADASTROS_TABLE, $data);
		
		return true;
	}
    
   public function getCadastro($idCadastro) {
	   return $this->db->query("SELECT * 
								FROM ".CADASTROS_TABLE."
								WHERE id=".$idCadastro)->row();
   }
      
   public function buscaCPF($cpf) {
	   return $this->db->query("SELECT * 
								FROM ".CADASTROS_TABLE."
								WHERE cpf='".$cpf."'")->row();
   }
      
	//Dados Pessoais
	public function insert_pessoais($data){
		
		$this->db->insert(PESSOAIS_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_pessoais($id, $data){
		$this->db->where('id_cadastro', $id);
		$this->db->update(PESSOAIS_TABLE, $data);
		
		return true;
	}
    
   public function getPessoais($id) {
	   return $this->db->query("SELECT * 
								FROM ".PESSOAIS_TABLE."
								WHERE id_cadastro=".$id)->row();
   }
      
	//Dados de contato
	public function insert_contato($data){
		
		$this->db->insert(CONTATO_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_contato($id, $data){
		$this->db->where('id_cadastro', $id);
		$this->db->update(CONTATO_TABLE, $data);
		
		return true;
	}
    
   public function getContato($id) {
	   return $this->db->query("SELECT * 
								FROM ".CONTATO_TABLE."
								WHERE id_cadastro=".$id)->row();
   }
      
	//Objetivos profissionaios
	public function insert_profissional($data){
		
		$this->db->insert(PROFISSIONAIS_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_profissional($id, $data){
		$this->db->where('id_cadastro', $id);
		$this->db->update(PROFISSIONAIS_TABLE, $data);
		
		return true;
	}
    
   public function getProfissional($id) {
	   return $this->db->query("SELECT * 
								FROM ".PROFISSIONAIS_TABLE."
								WHERE id_cadastro=".$id)->row();
   }
      
	//Áreas de Conhecimento
	public function insert_conhecimento($data){
		
		$this->db->insert(AREAS_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_conhecimento($id, $data){
		$this->db->where('id', $id);
		$this->db->update(AREAS_TABLE, $data);
		
		return true;
	}
    
   public function getConhecimento($id) {
	   return $this->db->query("SELECT * 
								FROM ".AREAS_TABLE."
								WHERE id_cadastro=".$id."
								ORDER BY id ASC")->result();
   }
   
   public function removeConhecimento($id) {
	 
		$this->db->where('id_cadastro', $id);
		$this->db->delete(AREAS_TABLE);
		
		return true;
   }
       
	//Histórico profissional
	public function insert_historico($data){
		
		$this->db->insert(HISTORICO_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_historico($id, $data){
		$this->db->where('id_cadastro', $id);
		$this->db->update(HISTORICO_TABLE, $data);
		
		return true;
	}
    
   public function getHistorico($id) {
	   return $this->db->query("SELECT * 
								FROM ".HISTORICO_TABLE."
								WHERE id_cadastro=".$id)->row();
   }
      
	//Curriculo
	public function insert_curriculo($data){
		
		$this->db->insert(CURRICULO_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_curriculo($id, $data){
		$this->db->where('id_cadastro', $id);
		$this->db->update(CURRICULO_TABLE, $data);
		
		return true;
	}
    
   public function getCurriculo($id) {
	   return $this->db->query("SELECT * 
								FROM ".CURRICULO_TABLE."
								WHERE id_cadastro=".$id." ORDER BY id DESC LIMIT 1")->row();
   }
       
	//Histórico profissional
	public function insert_trajetoria($data){
		
		$this->db->insert(TRAJETORIA_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_trajetoria($id, $data){
		$this->db->where('id', $id);
		$this->db->update(TRAJETORIA_TABLE, $data);
		
		return true;
	}
    
   public function getTrajetoria($id) {
	   return $this->db->query("SELECT * 
								FROM ".TRAJETORIA_TABLE."
								WHERE id_cadastro=".$id."
								ORDER BY id ASC")->result();
   }
   
   public function removeTrajetoria($id) {
	 
		$this->db->where('id_cadastro', $id);
		$this->db->delete(TRAJETORIA_TABLE);
		
		return true;
   }
   
	//Histórico profissional -> Ocupações
	public function insert_trajetoria_ocupacao($data){
		
		$this->db->insert(OCUPACAO_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_trajetoria_ocupacao($id, $data){
		$this->db->where('id', $id);
		$this->db->update(OCUPACAO_TABLE, $data);
		
		return true;
	}
    
   public function getTrajetoriaOcupacoes($id) {
	   return $this->db->query("SELECT * 
								FROM ".OCUPACAO_TABLE."
								WHERE id_trajetoria=".$id."
								ORDER BY id ASC")->result();
   }
	//Formação
	public function insert_formacao($data){
		
		$this->db->insert(FORMACAO_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_formacao($idUser, $data){
		$this->db->where('id_cadastro', $idUser);
		$this->db->update(FORMACAO_TABLE, $data);
		
		return true;
	}
    
   public function getFormacao($id) {
	   return $this->db->query("SELECT * 
								FROM ".FORMACAO_TABLE."
								WHERE id_cadastro=".$id)->row();
   }
      
	//Idiomas
	public function insert_idioma($data){
		
		$this->db->insert(IDIOMAS_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_idioma($id, $data){
		$this->db->where('id', $id);
		$this->db->update(IDIOMAS_TABLE, $data);
		
		return true;
	}
    
   public function getIdiomas($id) {
	   return $this->db->query("SELECT * 
								FROM ".IDIOMAS_TABLE."
								WHERE id_cadastro=".$id."
								ORDER BY id ASC")->result();
   }
      
   public function removeIdiomas($id) {
	 
		$this->db->where('id_cadastro', $id);
		$this->db->delete(IDIOMAS_TABLE);
		
		return true;
   }

	//Cursos
	public function insert_curso($data){
		
		$this->db->insert(CURSOS_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit_curso($id, $data){
		$this->db->where('id', $id);
		$this->db->update(CURSOS_TABLE, $data);
		
		return true;
	}
    
   public function getCursos($id) {
	   return $this->db->query("SELECT * 
								FROM ".CURSOS_TABLE."
								WHERE id_cadastro=".$id."
								ORDER BY id ASC")->result();
   }
   
      
   public function removeCursos($id) {
	 
		$this->db->where('id_cadastro', $id);
		$this->db->delete(CURSOS_TABLE);
		
		return true;
   }

   	//Envios
	public function count_envio() {
		return $this->db->count_all(ENVIO_TABLE);
	}
	
   public function get_envios() {
	   return $this->db->query("SELECT * 
								FROM ".ENVIO_TABLE."
								ORDER BY id DESC")->result();
   }

	public function insert_envio($data){
		
		$this->db->insert(ENVIO_TABLE, $data);
		
		return $this->db->insert_id();
	}
	
		
	//Filtro
	public function buscaFiltrosEnvios($dados) {
		$sql = "SELECT e.*
				FROM ".ENVIO_TABLE." AS e
				";
		$where = false;
		if($dados['genero'] || $dados['deficiente']) {
			$sql .= " INNER JOIN ".PESSOAIS_TABLE." AS p
					ON p.id_cadastro=e.id_cadastro";
			
			if($dados['genero']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "p.sexo='".$dados['genero']."'";
			}
			
			if($dados['deficiente']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "p.deficiencia='".$dados['deficiente']."'";
			}
		}
		
		if($dados['escolaridade']) {
			$sql .= " INNER JOIN ".FORMACAO_TABLE." AS f
					ON f.id_cadastro=e.id_cadastro";
			
			if(!$where) {
				$where .= ' WHERE ';
			} else {
				$where .= ' && ';
			}
			
			$where .= "f.nivel_escolaridade='".$dados['escolaridade']."'";
		}

		if($dados['mudanca'] || $dados['viagens']) {
			$sql .= " INNER JOIN ".PROFISSIONAIS_TABLE." AS prof
					ON prof.id_cadastro=e.id_cadastro";
			
			if($dados['mudanca']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "prof.outra_cidade='".$dados['mudanca']."'";
			}
			
			if($dados['viagens']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "prof.viagem='".$dados['viagens']."'";
			}
		}

		if($dados['cidade'] || $dados['estado']) {
			$sql .= " INNER JOIN ".CONTATO_TABLE." AS cont
					ON cont.id_cadastro=e.id_cadastro";
			
			if($dados['cidade']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "cont.cidade='".$dados['cidade']."'";
			}
			
			if($dados['estado']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "cont.estado='".$dados['estado']."'";
			}
		}

		if($dados['area']) {
			
			if($dados['area']) {
				if(!$where) {
					$where .= ' WHERE ';
				} else {
					$where .= ' && ';
				}
				
				$where .= "(e.id_area1='".$dados['area']."' || e.id_area2='".$dados['area']."')";
			}
			
		}

		if($dados['idade1'] && $dados['idade2']) {
			$sql .= " INNER JOIN ".CADASTROS_TABLE." AS c
					ON c.id=e.id_cadastro";
			
			if(!$where) {
				$where .= ' WHERE ';
			} else {
				$where .= ' && ';
			}
			$ano = date('Y')-$dados['idade1'];
			$ano2 = date('Y')-$dados['idade2'];
			$where .= "c.nascimento<='".$ano.'-'.date('m').'-'.date('d')."' && c.nascimento>='".$ano2.'-'.date('m').'-'.date('d')."'";
		}
		
		
		$sql = $sql.$where." GROUP BY e.id ORDER BY e.id DESC";
		
		return $this->db->query($sql)->result();
		
	}


   
   //Estados e cidades
   
   public function getPaises() {
	   return $this->db->query("SELECT * 
								FROM ".PAISES_TABLE."
								ORDER BY nome ASC")->result();
   }
   
   public function getEstados() {
	   return $this->db->query("SELECT * 
								FROM ".ESTADOS_TABLE."
								ORDER BY nome ASC")->result();
   }
   
   public function getEstado($id) {
	   return $this->db->query("SELECT * 
								FROM ".ESTADOS_TABLE."
								WHERE id=".$id)->row();
   }
   
   public function getCidades($id) {
	   return $this->db->query("SELECT * 
								FROM ".CIDADES_TABLE."
								WHERE id_estado=".$id."
								ORDER BY nome ASC")->result();
   }
   public function getTodasCidades() {
	   return $this->db->query("SELECT * 
								FROM ".CIDADES_TABLE."
								ORDER BY nome ASC")->result();
   }
   
   public function getCidade($id) {
	   return $this->db->query("SELECT * 
								FROM ".CIDADES_TABLE."
								WHERE id=".$id."")->row();
   }
   
     
}

