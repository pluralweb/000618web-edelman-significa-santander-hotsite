<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Telefones_model extends CI_Model {

   public function get_telefones() {
	   return $this->db->query("SELECT * 
								FROM ".TELEFONES_TABLE."
								ORDER BY data_add DESC")->result();
   }

	public function insert_telefone($data){
		
		$this->db->insert(TELEFONES_TABLE, $data);
		
		return $this->db->insert_id();
	}

     
}

