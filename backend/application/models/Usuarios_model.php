<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	public function get_login($user, $password){
		return $this->db->select("id,nome,usuario")
						->where('usuario', $user)
						->where('senha',  md5($password))
						->get(USUARIOS_TABLE)
						->result();
	}

   public function get_usuarios() {
	   return $this->db->query("SELECT *
								FROM ".USUARIOS_TABLE."
								WHERE status=1
								ORDER BY data_add DESC")->result();
   }

   public function get_permissoes_user($id) {
	   return $this->db->query("SELECT p.*
								FROM ".PERMISSOES_USUARIOS_TABLE." AS per
								INNER JOIN ".PERMISSOES_TABLE." AS p
								ON p.id=per.id_permissao
								WHERE id_usuario=".$id."")->result();
   }


   public function get_permissoes() {
	   return $this->db->query("SELECT *
								FROM ".PERMISSOES_TABLE."
								ORDER BY nome ASC")->result();
   }

	public function insert_usuario($data){

		$this->db->insert(USUARIOS_TABLE, $data);

		return $this->db->insert_id();
	}

	public function insert_permissao($data){

		$this->db->insert(PERMISSOES_USUARIOS_TABLE, $data);

		return $this->db->insert_id();
	}

	public function edit_user($idUser, $data){
		$this->db->where('id', $idUser);
		$this->db->update(USUARIOS_TABLE, $data);

		return true;
	}

	public function delete_permissoes($idUsuario) {

		$this->db->delete(PERMISSOES_USUARIOS_TABLE, array('id_usuario', $idUsuario));

		return true;
	}

   public function getUsuario($idUsuario) {
	   return $this->db->query("SELECT *
								FROM ".USUARIOS_TABLE."
								WHERE id=".$idUsuario)->row();
   }

}
