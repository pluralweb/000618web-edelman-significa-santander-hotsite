<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos_model extends CI_Model {

   public function get_videos() {
	   return $this->db->query("SELECT *
								FROM ".VIDEOS_TABLE."
								WHERE status=1
								ORDER BY id DESC")->result();
   }

	public function insert_video($data){

		$this->db->insert(VIDEOS_TABLE, $data);

		return $this->db->insert_id();
	}

	public function edit_video($id, $data){
		$this->db->where('id', $id);
		$this->db->update(VIDEOS_TABLE, $data);

		return true;
	}

   public function getVideo($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".VIDEOS_TABLE."
								WHERE id=".$idCadastro)->row();
   }

}
