<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracoes_model extends CI_Model {

  /**
   * Método que retorn todas as
   * configurações presentes
   * na tabela configurações
   *
   * @return (array->obj) retorna os dados de todos os configurações no banco
   *
   */
  public function getAll($camp) {
    if(!empty($camp)){ 
      $this->db->where('campanha', $camp);
    }
    return $this->db
          ->get(CONFIGURACOES_TABLE)->row();
  }

  /**
   * Método que retorn todas as
   * configurações presentes
   * na tabela configurações
   *
   * @return (array->obj) retorna os dados de todos os configurações no banco
   *
   */
  public function getAllFaq($camp) {
    if(!empty($camp)){ 
      $this->db->where('campanha', $camp);
    }
    return $this->db
          ->get(FAQ_TABLE)->result();
  }


  /**
   * Método que retorn as
   * informações de um determinada
   * configuração na tabela configurações
   *
   * @param (string) $tipo tipo de configuação no banco
   * @return (array->obj) retorna os dados da config com o tipo = $tipo no banco
   *
   */

  public function getField($field) {
    return $this->db
                ->select($field)
                ->get(CONFIGURACOES_TABLE)
                ->row();
  }

  /**
   * Método que retorn as
   * se o codigo randomico existe
   *
   * @param (string) $tipo numero hexadecimal randomico
   * @return (obj) retorna os dados
   *
   */

  public function getRandom($code) {
    return $this->db
                ->select('id')
                ->where('random', $code)
                ->get(RESET_TABLE)
                ->row();
  }

  /**
   * Método que retorn as
   * se o usuario já pediu o reset hoje
   *
   * @param (string) $tipo numero hexadecimal randomico
   * @return (obj) retorna os dados
   *
   */

  public function getIdUserReset($id) {
    return $this->db
                ->select('id')
                ->where('id_user', $id)
                ->where("DATE(NOW()) BETWEEN DATE(data_add) AND DATE(data_cut)")
                ->get(RESET_TABLE)
                ->row();
  }

  /**
   * Método que insere informações
   * na tabela configurações
   *
   * @param (array) $data dados a serem inseridos
   * @return (int) retorna id que foi inserido no banco
   *
   */
  public function insert($data){
    $this->db->insert(CONFIGURACOES_TABLE, $data);
    return $this->db->insert_id();
  }

  /**
   * Método que insere informações
   * na tabela configurações
   *
   * @param (array) $data dados a serem inseridos
   * @return (int) retorna id que foi inserido no banco
   *
   */
  public function insert_faq($data){
    $this->db->insert(FAQ_TABLE, $data);
    return $this->db->insert_id();
  }

  /**
   * Método que insere informações
   * na tabela de reset
   *
   * @param (array) $data dados a serem inseridos
   * @return (int) retorna id que foi inserido no banco
   *
   */
  public function insertCode($data){
    $this->db->insert(RESET_TABLE, $data);
    return $this->db->insert_id();
  }

  /**
   * Método que insere informações
   * na tabela de reset
   *
   * @param (array) $data dados a serem inseridos
   * @return (int) retorna id que foi inserido no banco
   *
   */
  public function delRandom($code){
    $this->db->where('random', $code);
    $this->db->delete(RESET_TABLE);
    return $this->db->insert_id();
  }

  /**
   * Método que insere informações
   * na tabela de reset
   *
   * @param (array) $data dados a serem inseridos
   * @return (int) retorna id que foi inserido no banco
   *
   */
  public function deleteFaq($idFaq){
    $this->db->where('id', $idFaq);
    $this->db->delete(FAQ_TABLE);
    return $this->db->affected_rows();
  }

  /**
   * Método que edita informações
   * na tabela configurações
   *
   * @param (int) $id id da configuração no banco
   * @param (array) $data dados a serem editados
   * @return (int) retorna as linhas afetadas
   *
   */
  public function edit($id = 1, $data){
		$this->db->where('id', $id);
		$this->db->update(CONFIGURACOES_TABLE, $data);
	  return $this->db->affected_rows();
	}

  /**
   * Método que edita informações
   * na tabela configurações
   *
   * @param (int) $id id da configuração no banco
   * @param (array) $data dados a serem editados
   * @return (int) retorna as linhas afetadas
   *
   */
  public function editFaq($id, $data){
    $this->db->where('id', $id);
		$this->db->update(FAQ_TABLE, $data);
	  return $this->db->affected_rows();
	}

  /**
   * Método que edita informações
   * na tabela configurações
   *
   * @param (int) $id id da configuração no banco
   * @param (array) $data dados a serem editados
   * @return (int) retorna as linhas afetadas
   *
   */
  public function getCode($id = 1){
    $returnEmail = array();
    $code = null;
    $returnQuery = true;

    while(!is_null($returnQuery)){
      $code = bin2hex(openssl_random_pseudo_bytes(30));
      $returnQuery = $this->getRandom($code);
    };

    if(is_null($returnQuery) && is_null($this->getIdUserReset($id))){
      $now = new DateTime();
      $now = $now->format('Y-m-d H:i');

      $end = new DateTime($now);
      $end->add(new DateInterval('P1D')); // P1D means a period of 1 day
      $end = $end->format('Y-m-d H:i');

      $this->insertCode(array(
                              'random'=> $code,
                              'id_user'=>$id,
                              'data_add'=>$now,
                              'data_cut'=>$end
                            )
                        );

    	$returnEmail['msg'] = 200;
    	$returnEmail['code'] = $code;
    }else{
		$returnEmail['msg'] = 404;
    }
    return $returnEmail;

	}
}
