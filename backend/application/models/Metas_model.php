<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Metas_model extends CI_Model {

  public function getMetas($idFuncionario, $mes) {
    return $this->db->query("SELECT *
               FROM ".METAS_TABLE."
               WHERE id_funcionario=".$idFuncionario." && mes = ".$mes." ORDER BY id DESC LIMIT 1")->row();
  }

  public function getAllMetas() {
    return $this->db->query("SELECT f.nome, f.matricula, m.*, r.nome as nomeRede, fi.nome as nomeFilial
               FROM ".METAS_TABLE." as m
               LEFT JOIN ".FUNCIONARIOS_TABLE." as f
                ON m.id_funcionario = f.id
               LEFT JOIN ".REDE_TABLE." as r
                ON f.id_rede = r.id
               LEFT JOIN ".FILIAL_TABLE." as fi
                ON f.id_filial = fi.id"
               )->result();
  }

  public function edit_metas($id, $data){
    $this->db->where('id', $id);
    $this->db->update(METAS_TABLE, $data);

    return true;
  }
  public function insert_metas($data){

		$this->db->insert(METAS_TABLE, $data);

		return $this->db->insert_id();
  }

  public function getLastMetas($_id_funcionario){
      $this->db->where('id_funcionario', $_id_funcionario);
      $this->db->order_by('mes','desc');
      $result = $this->db->get(METAS_TABLE);
      return $result->result()[0];
  }

}
