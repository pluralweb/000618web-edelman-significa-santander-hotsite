<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ranking_model extends CI_Model {

   public function getRanking($idFuncionario, $anomes) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_TABLE."
								WHERE id_funcionario=".$idFuncionario." && anomes='".$anomes."'")->row();
   }

   public function getAllRanking() {
	   return $this->db->query("SELECT r.*, f.*
								FROM ".RANKING_TABLE." as r
                INNER JOIN ".FUNCIONARIOS_TABLE." as f
                ON r.id_funcionario = f.id
                ")->result();
   }

   public function getAllFuncionarioRanking($idFuncionario) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_TABLE."
								WHERE id_funcionario=".$idFuncionario."")->result();
   }

   public function getAllFuncionarioRankingMes($idFuncionario, $mes) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_TABLE."
								WHERE id_funcionario=".$idFuncionario." && mes = ".$mes)->result();
   }

   public function getFuncionarioRanking($colocacao, $periodo) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_TABLE."
								WHERE colocacao=".$colocacao." && anomes='".$periodo."'")->result();
   }

   public function getRankingPeriodo($idFuncionario, $periodo) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_PERIODO_TABLE."
								WHERE id_funcionario=".$idFuncionario." && id_periodo='".$periodo."'")->row();
   }

   public function getAllFuncionarioRankingPeriodo($idFuncionario) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_PERIODO_TABLE."
								WHERE id_funcionario=".$idFuncionario."")->result();
   }

	public function insert_ranking($data){

		$this->db->insert(RANKING_TABLE, $data);

		return $this->db->insert_id();
	}


	public function edit_ranking($id, $data){
		$this->db->where('id', $id);
		$this->db->update(RANKING_TABLE, $data);

		return true;
	}

   public function getRankingAcumuladoMes($idFuncionario, $mes) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_ACUMULADO_TABLE."
								WHERE id_funcionario=".$idFuncionario." && mes = ".$mes." ORDER BY id DESC LIMIT 1")->row();
   }

   public function getRankingAcumulado($idFuncionario) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_ACUMULADO_TABLE."
								WHERE id_funcionario=".$idFuncionario." ORDER BY id DESC LIMIT 1")->row();
   }

   public function getRankingAcumuladoCustom($posicao) {
	   return $this->db->query("SELECT *
								FROM ".RANKING_ACUMULADO_TABLE."
								WHERE colocacao=".$posicao." ORDER BY id DESC LIMIT 1")->row();
   }

	public function insert_rankingAcumulado($data){

		$this->db->insert(RANKING_ACUMULADO_TABLE, $data);

		return $this->db->insert_id();
	}


	public function edit_rankingAcumulado($id, $data){
		$this->db->where('id', $id);
		$this->db->update(RANKING_ACUMULADO_TABLE, $data);

		return true;
	}

	public function insert_rankingPeriodo($data){

		$this->db->insert(RANKING_PERIODO_TABLE, $data);

		return $this->db->insert_id();
	}


	public function edit_rankingPeriodo($id, $data){
		$this->db->where('id', $id);
		$this->db->update(RANKING_PERIODO_TABLE, $data);

		return true;
	}

}
