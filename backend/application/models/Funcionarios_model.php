<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Funcionarios_model extends CI_Model {

   public function get_funcionarios() {
	   return $this->db->query("SELECT f.*, r.nome AS rede, rg.nome AS regional
								FROM ".FUNCIONARIOS_TABLE." as f
								INNER JOIN ".REDE_COMERCIAL_TABLE." AS r
								ON r.id=f.id_rede_comercial
								INNER JOIN ".REGIONAL_TABLE." AS rg
								ON rg.id=f.id_regional
								WHERE f.status=1
								ORDER BY f.nome ASC")->result();
   }

   public function getVencedor($matricula) {
	   return $this->db->query("SELECT *
								FROM ".VENCEDORES_TABLE."
								WHERE matricula = '".$matricula."'")->result();
   }

	public function insert_funcionario($data){

		$this->db->insert(FUNCIONARIOS_TABLE, $data);

		return $this->db->insert_id();
	}

	public function edit_funcionario($id, $data){
		$this->db->where('id', $id);
		$this->db->update(FUNCIONARIOS_TABLE, $data);
    return $this->db->affected_rows();
		// return true;
	}

	public function desativa_funcionarios(){
		$data = array('status' => 0);
		$this->db->update(FUNCIONARIOS_TABLE, $data);

		return true;
	}

   public function getFuncionario($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".FUNCIONARIOS_TABLE."
								WHERE id=".$idCadastro)->row();
   }

   public function getFuncionarioNome($nome) {
	   return $this->db->query("SELECT *
								FROM ".FUNCIONARIOS_TABLE."
								WHERE nome='".$nome."'")->row();
   }

   public function getByMatricula($matricula) {
	   return $this->db->query("SELECT *
								FROM ".FUNCIONARIOS_TABLE."
								WHERE matricula='".$matricula."'")->row();
   }

   public function get_funcionarioCpf($cpf,$campanha = null) {    
	   return $this->db->query("SELECT *
								FROM ".FUNCIONARIOS_TABLE."
								WHERE cpf='".$cpf."'".(!empty($id_regional) ? "AND campanha='".$campanha."'" : ''))->row();
   }

   public function get_login($matricula, $senha) {
	   return $this->db->query("SELECT *
								FROM ".FUNCIONARIOS_TABLE."
								WHERE matricula='".$matricula."' && senha='".md5($senha)."' && status=1")->row();
   }

   public function get_login_master($matricula) {
	   return $this->db->query("SELECT *
								FROM ".FUNCIONARIOS_TABLE."
								WHERE matricula='".$matricula."' && status=1")->row();
   }

   public function verificaUser($matricula, $cpf) {
	   return $this->db->query("SELECT *
								FROM ".FUNCIONARIOS_TABLE."
								WHERE matricula='{$matricula}' && cpf='{$cpf}'")->row();
   }


   public function get_cargo($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".CARGOS_TABLE."
								WHERE id=".$idCadastro)->row();
   }

   public function get_cargoNome($nome) {
	   return $this->db->query("SELECT *
								FROM ".CARGOS_TABLE."
								WHERE nome='".$nome."'")->row();
   }

	public function insert_cargo($data){

		$this->db->insert(CARGOS_TABLE, $data);

		return $this->db->insert_id();
	}


   public function get_subcargo($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".SUBCARGOS_TABLE."
								WHERE id=".$idCadastro)->row();
   }

   public function get_subcargoNome($nome) {
	   return $this->db->query("SELECT *
								FROM ".SUBCARGOS_TABLE."
								WHERE nome='".$nome."'")->row();
   }

	public function insert_subcargo($data){

		$this->db->insert(SUBCARGOS_TABLE, $data);

		return $this->db->insert_id();
	}

   public function get_porte($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".PORTE_TABLE."
								WHERE id=".$idCadastro)->row();
   }

   public function get_porteNome($nome) {
	   return $this->db->query("SELECT *
								FROM ".PORTE_TABLE."
								WHERE nome='".$nome."'")->row();
   }

	public function insert_porte($data){

		$this->db->insert(PORTE_TABLE, $data);

		return $this->db->insert_id();
	}

   public function get_redeComercial($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".REDE_COMERCIAL_TABLE."
								WHERE id=".$idCadastro)->row();
   }

   public function get_redeComercialNome($nome) {
	   return $this->db->query("SELECT *
								FROM ".REDE_COMERCIAL_TABLE."
								WHERE nome='".$nome."'")->row();
   }

	public function insert_redeComercial($data){

		$this->db->insert(REDE_COMERCIAL_TABLE, $data);
		return $this->db->insert_id();
	}


   public function get_rede($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".REDE_TABLE."
								WHERE id=".$idCadastro)->row();
   }

   public function get_redeNome($nome) {
	   return $this->db->query("SELECT *
								FROM ".REDE_TABLE."
								WHERE nome='".$nome."'")->row();
   }

	public function insert_rede($data){

		$this->db->insert(REDE_TABLE, $data);

		return $this->db->insert_id();
	}

   public function get_regional($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".REGIONAL_TABLE."
								WHERE id=".$idCadastro)->row();
   }

    public function get_regional_all() {
     return $this->db->query("SELECT *
                FROM ".REGIONAL_TABLE)->result();
   }

   public function get_regionalNome($nome) {
	   return $this->db->query("SELECT *
								FROM ".REGIONAL_TABLE."
								WHERE nome='".$nome."'")->row();
   }

	public function insert_regional($data){

		$this->db->insert(REGIONAL_TABLE, $data);

		return $this->db->insert_id();
	}

   public function get_filial($idCadastro) {
	   return $this->db->query("SELECT *
								FROM ".FILIAL_TABLE."
								WHERE id=".$idCadastro)->row();
   }

	public function get_filialNome($nome) {
	   return $this->db->query("SELECT *
								FROM ".FILIAL_TABLE."
								WHERE nome='".$nome."'")->row();
   }

	public function insert_filial($data){

		$this->db->insert(FILIAL_TABLE, $data);

		return $this->db->insert_id();
	}

}
