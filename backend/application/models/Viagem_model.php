<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Viagem_model extends CI_Model {

   public function getViagens() {
	   return $this->db->query("SELECT * 
								FROM ".VIAGEM_TABLE."
								ORDER BY data_add DESC")->result();
   }
    
    public function getViagensById($id) {
	   return $this->db->query("SELECT * 
								FROM ".VIAGEM_TABLE."
								WHERE id = ".$id)->row();
   }
    
    public function getViagensByIdFuncionario($id) {
	   return $this->db->query("SELECT * 
								FROM ".VIAGEM_TABLE."
								WHERE id_funcionario = ".$id)->row();
   }

	public function insert($data){
		
		$this->db->insert(VIAGEM_TABLE, $data);
		
		return $this->db->insert_id();
	}
 	 
	public function edit($id, $data){
		$this->db->where('id_funcionario', $id);
		$this->db->update(VIAGEM_TABLE, $data);
		
		return true;
	}

     
}