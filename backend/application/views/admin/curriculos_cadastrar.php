    <div class="pageheader">
      <h2><i class="fa fa-file-text-o"></i> Cadastrar Autorização</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('/') ?>">AFT</a></li>
          <li>Autorizações</li>
          <li class="active">Nova Autorização</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Autorização</h3>
          <p>Aqui você cadastra uma autorização ao paciente previamente inserido no menu de cadastros.</p>
        </div>
        
        <form class="form-horizontal form-bordered" action="<?php echo base_url('autorizacoes/addAutorizacao') ?>" method="post">
            <div class="panel-body panel-body-nopadding">
          
            
            <div class="form-group">
              <div class="col-sm-3">
                <label class="control-label">Nº de Autorização</label>
                <input type="text" name="numero" id="numero" class="form-control" required />
              </div>
              <div class="col-sm-3">
                <label class="control-label">Nome do paciente</label>
                <select id="paciente" name="paciente" data-placeholder="Selecione" title="Selecione o Paciente" class="select2" required>
                      <option value=""></option>
                      <?php foreach($pacientes as $pct) {?>
                      <option value="<?php echo $pct->id; ?>"><?php echo $pct->nome; ?></option>
                      <?php } ?>
                    </select>
                    <label class="error" for="paciente"></label>
              </div>
              <div class="col-sm-3">
                <label class="control-label">Data de Início</label>
                 <div class="input-group">
                <input type="text" name="inicio" id="inicio" class="form-control" required>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
              </div>
              <div class="col-sm-3">
                <label class="control-label">Data de Final</label>
                 <div class="input-group">
                <input type="text" name="fim" id="fim" class="form-control" required>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-2">
                <label class="control-label">Periodicidade</label>
                 <select id="periodicidade" name="periodicidade" data-placeholder="Selecione" title="Selecione a periodicidade" class="select2" required>
                      <option value=""></option>
                      <?php for($i=1;$i<=7;$i++) {?>
                      <option value="<?php echo $i;?>"><?php echo $i;?>x por semana</option>
                      <?php } ?>
                      <option value="14">14x por semana</option>
                    </select>
              </div>
              <div class="col-sm-1">
                <label class="control-label">Sessões</label>
                <input type="text" name="sessoes" id="sessoes" class="form-control" readonly="readonly">
              </div>
              <div class="col-sm-9">
                <label class="control-label">Código</label>
                <textarea name="codigo" id="codigo" class="form-control" required></textarea>
              </div>
              
            </div>
            
            <div class="form-group">
              <div class="col-sm-6">
                  <label class="control-label">Nome do Fisioterapeuta</label>
                    <select id="fisioterapeuta" name="fisioterapeuta" data-placeholder="Selecione" title="Selecione o Fisioterapeuta" class="select2" required>
                      <option value=""></option>
                      <?php foreach($fisioterapeutas as $fisio) {?>
                      <option value="<?php echo $fisio->id; ?>"><?php echo $fisio->nome; ?></option>
                      <?php } ?>
                    </select>
                    <label class="error" for="fisioterapeuta"></label>
              </div>
              <div class="col-sm-3">
                <label class="control-label">Valor da Sessão</label>
                <input type="text" name="vl_sessao" id="vl_sessao" class="form-control" required />
              </div>
              <div class="col-sm-3">
                <label class="control-label">Valor do Plantão</label>
                <input type="text" name="vl_plantao" id="vl_plantao" class="form-control" />
              </div>
            </div>  
            <div style="display:none;" id="fisioplantao">
                <div class="form-group">
                <div class="col-sm-6">
                      <label class="control-label">Nome do Fisioterapeuta de Plantão</label>
                        <select name="fisio_plantao[]" data-placeholder="Selecione" title="Selecione o Fisioterapeuta" class="select2">
                          <option value=""></option>
                          <?php foreach($fisioterapeutas as $fisio) {?>
                          <option value="<?php echo $fisio->id; ?>"><?php echo $fisio->nome; ?></option>
                          <?php } ?>
                        </select>
                        <label class="error" for="fisio_plantao"></label>
                  </div>
                  <div class="col-sm-3">
                    <label class="control-label">Data do Plantão</label>
                     <div class="input-group">
                    <input type="text" name="data_plantao[]" class="form-control dateplantao">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                  </div>
                  <div class="col-sm-3">
                  	<label class="control-label">&nbsp;</label>
                    <div class="input-group">
                    <a class="btn btn-default btn removeFisioterapeuta"><i class="glyphicon glyphicon-remove"></i></a>
                    <a class="btn btn-default btn AddFisioterapeuta"><i class="glyphicon glyphicon-plus"></i></a>
                    </div>
                  </div>
                </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label">Observações</label>
                <textarea name="obs" id="obs" class="form-control"></textarea>
              </div>
            </div>
            
          </div><!-- panel-body -->
        
          <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>&nbsp;
                    <button type="reset" class="btn btn-default">Cancelar</button>
                  </div>
               </div>
            </div>
          
        </form>
      </div>
      
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {
    
    "use strict";
    
 	 jQuery('#inicio').datepicker({"dateFormat" : 'dd/mm/yy'});
 	 jQuery('#fim').datepicker({"dateFormat" : 'dd/mm/yy'});
	 
	 
 	  jQuery("#inicio").mask("99/99/9999");
	  jQuery("#fim").mask("99/99/9999");
	  
	  jQuery("#vl_sessao").maskMoney({thousands:'.', decimal:',', affixesStay: true});
	  jQuery("#vl_plantao").maskMoney({thousands:'.', decimal:',', affixesStay: true});
	  
	 jQuery("select.select2").select2({
	  width: '100%',
	});
	
	jQuery("#periodicidade").change(function(e) {
		e.preventDefault();
		
		if($(this).val()>=7) {
			$("#fisioplantao").fadeIn();
		} else {
			$("#fisioplantao").fadeOut();
		}
	});
	
	$("#inicio,#fim,#periodicidade").change(function(e) {
		e.preventDefault();
		var inicio = $("#inicio").val(),
			fim = $("#fim").val(),
			periodicidade = $("#periodicidade").val();
		if(inicio && fim && periodicidade) {
			var DAY = 1000 * 60 * 60  * 24;

			var nova1 = inicio.split('/');
			var Nova1 = nova1[1]+"/"+nova1[0]+"/"+nova1[2];
			var nova2 = fim.toString().split('/');
			var Nova2 = nova2[1]+"/"+nova2[0]+"/"+nova2[2];
			
			var d1 = new Date(Nova1)
			var d2 = new Date(Nova2)
			
			var days_passed = Math.round((d2.getTime() - d1.getTime()) / DAY) 

			var semanas = Math.floor(days_passed/7);
			
			var sessoes = semanas * periodicidade;
			
			$("#sessoes").val(sessoes);
		}
	});
	
  	init();
	
  });
	
 	function init() {
		
		jQuery('.dateplantao').datepicker({"dateFormat" : 'dd/mm/yy'});
	 	jQuery(".dateplantao").mask("99/99/9999");
	  
		jQuery("select.select2").select2({
		  width: '100%',
		});
	  
	  jQuery(".AddFisioterapeuta").unbind();
	  jQuery(".removeFisioterapeuta").unbind();
		
	  jQuery(".AddFisioterapeuta").click(function(e) {
		  e.preventDefault();
		  var html = '<div class="form-group">'+
				  '<div class="col-sm-6">'+
						'<label class="control-label">Nome do Fisioterapeuta de Plantão</label>'+
						  '<select name="fisio_plantao[]" data-placeholder="Selecione" title="Selecione o Fisioterapeuta" class="select2">'+
							'<option value=""></option>'+
							<?php foreach($fisioterapeutas as $fisio) {?>
							'<option value="<?php echo $fisio->id; ?>"><?php echo $fisio->nome; ?></option>'+
							<?php } ?>
						  '</select>'+
						  '<label class="error" for="fisio_plantao"></label>'+
					'</div>'+
					'<div class="col-sm-3">'+
					  '<label class="control-label">Data do Plantão</label>'+
					   '<div class="input-group">'+
					  '<input type="text" name="data_plantao[]" class="form-control dateplantao">'+
					  '<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>'+
					  '</div>'+
					'</div>'+
					'<div class="col-sm-3">'+
					  '<label class="control-label">&nbsp;</label>'+
					  '<div class="input-group">'+
					  '<a class="btn btn-default btn removeFisioterapeuta"><i class="glyphicon glyphicon-remove"></i></a>'+
					  '<a class="btn btn-default btn AddFisioterapeuta"><i class="glyphicon glyphicon-plus"></i></a>'+
					  '</div>'+
					'</div>'+
				  '</div>';
		  
		 
		  jQuery("#fisioplantao").append(html);
		  init();
		  
	  });
	  
	  
	  jQuery(".removeFisioterapeuta").click(function(e) {
		e.preventDefault();
		
		jQuery(this).closest('.form-group').remove();
	});
	}
</script>

</body>
</html>
