<div class="pageheader">
    <h2><i class="fa fa-pencil"></i> Cadastrar FAQ</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/') ?>">DM9 Mentoring</a></li>
            <li>Configurações</li>
            <li class="active"> Redes Sociais</li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">FAQ</h3>
            <p>Aqui você cadastra o FAQ para o site.</p>
        </div>

        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/configuracoes/addFaq_veiculos') ?>" method="post" enctype="multipart/form-data">
            <div class="panel-body panel-body-nopadding">
                <div class="form-group">
                    <div class="col-sm-12">
                      <label class="control-label">Mostrar "Enviar sua dúvida"</label>
                      <br>
                      <input type="radio" name="mostrarQ" value="0" <?php echo ($config->mostrarQ == 0) ? 'checked': ''; ?>/>Sim <br>
                      <input type="radio" name="mostrarQ" value="1" <?php echo ($config->mostrarQ == 1) ? 'checked': ''; ?>/>Não
                      <hr class="col-sm-12">
                    </div>
                    <div class="col-sm-12">
                      <label class="control-label">E-mail "Enviar sua dúvida"</label>
                      <input type="text" name="email" class="form-control" value="<?php echo $config->email; ?>" />
                      <hr class="col-sm-12">
                    </div>
                    <div class="col-sm-12">
                        <div id="interesses">
                          <?php if ($faq): ?>
                            <?php foreach ($faq as $key => $value): ?>
                              <div class="row relac">
                                <input type="hidden" name="id[]" value="<?php echo $faq[$key]->id; ?>" />
                                <div class="col-sm-11">
                                  <label class="control-label">Pergunta</label>
                                  <input type="text" name="pergunta[]" class="form-control" value="<?php echo $faq[$key]->pergunta; ?>" />
                                </div>
                                <div class="col-sm-1">
                                  <a href="#" title="Remover" style="margin-top:35px" class="btn btn-default btn remover"><i class="glyphicon glyphicon-trash"></i></a>
                                </div>
                                <?php /*
                                <div class="col-sm-12">
                                  <label class="control-label">Imagem da Resposta</label><br />
                                  <?php if (isset($faq[$key]->imgfaq)): ?>
                                    <img src="<?php echo base_url(IMAGENS_FOLDER.$faq[$key]->imgfaq); ?>" alt="" />
                                  <?php endif; ?>
                                  <input type="file" name="imgfaq[]" class="form-control" />
                                </div>
                                */ ?>
                                <div class="col-sm-12">
                                  <label class="control-label">Resposta</label>
                                  <textarea type="text" name="resposta[]" class="form-control wycs" cols="200" rows="100" style="height: 150px;"><?php echo $faq[$key]->resposta; ?></textarea>
                                  <hr class="col-sm-12">
                                </div>
                                <br />
                                <br />
                              </div>
                            <?php endforeach; ?>
                          <?php else: ?>
                            <div class="row relac">
                              <div class="col-sm-11">
                                <label class="control-label">Pergunta</label>
                                <input type="text" name="pergunta[]" class="form-control" />
                              </div>
                              <div class="col-sm-1">
                                <a href="#" title="Remover" style="margin-top:35px" class="btn btn-default btn remover"><i class="glyphicon glyphicon-trash"></i></a>
                              </div>
                              <?php /*
                              <div class="col-sm-12">
                                <label class="control-label">Imagem da Resposta</label>
                                <input type="file" name="imgfaq[]" class="form-control"/>
                              </div>
                              <div class="col-sm-12">
                                <label class="control-label">Resposta</label>
                                <textarea type="text" name="resposta[]" class="form-control wycs" style="heigth: 150px;"></textarea>
                                <hr class="col-sm-12">
                              </div>
                              <br />
                              <br />
                            </div>
                            */ ?>
                          <?php endif; ?>
                        </div>
                        <div class="row" style="margin-top:35px">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-primary" id="addInt">Adicionar</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                    </div>
                </div>
            </div>

        </form>
    </div>

</div>
<!-- contentpanel -->

</div>
<!-- mainpanel -->

</section>

<style media="screen">
    .img img {
        display: block;
        width: 100%;
    }
</style>
<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.tagsinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/colorpicker.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/wysihtml5-0.3.0.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-wysihtml5.js') ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
    $(document).ready(function($) {

        "use strict";

        $(".select2").select2({
            width: '100%'
        });

        $(document).on('click', ".remover", function(e) {
            e.preventDefault();
            var el = $(this);
            var val = $(this).closest('.relac').find('input[type="hidden"]').val();
            $(this).closest('.relac').hide().find('input[type="hidden"]').val('del'+val);
        });
        $("#addInt").click(function(e) {
            e.preventDefault();
            var clone = $('#interesses .relac:first').clone();
            $('#interesses').append(
                '<div class="row relac"><div class="col-sm-11"><label class="control-label">Pergunta</label><input type="text" name="pergunta[]" class="form-control" /></div><div class="col-sm-1"><a style="margin-top:35px" href="#" title="Remover" class="btn btn-default btn remover"><i class="glyphicon glyphicon-trash"></i></a></div><div class="col-sm-12"><label class="control-label">Imagem da Resposta</label><input type="file" name="imgfaq[]" class="form-control"/></div><div class="col-sm-12"><label class="control-label">Resposta</label><textarea type="text" name="resposta[]" class="form-control" style="height:150px"></textarea></div><br /><br /></div>'
            ).find('textarea').filter(function(){
              return $(this).is(':visible');
            }).wysihtml5({
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": false //Button to insert an image. Default true,
            });
        });

        $('.wycs').wysihtml5({
            "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true //Button to insert an image. Default true,
        });


    });
</script>

</body>

</html>
