    <div class="pageheader">
      <h2><i class="fa fa-plane"></i> Detalhes de Viagens</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
          <li><a href="<?php echo base_url('admin/viagens/') ?>">Listagem</a></li>
          <li class="active">Detalhes</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Conteúdos</h3>
          <p>Visualize os dados completos de um conteúdo.</p>
        </div>
        
        <div class="panel-body">   
            <div class="row">
              <div class="col-sm-12">
                <strong>Foto passaporte</strong>
                <p><img src="<?php echo base_url(PASSAPORTE.'/'.$cadastros->foto_passaporte);?>" width="150" alt="<?php echo $cadastros->nome;?>"></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <strong>Nome</strong>
                 <p><?php echo $cadastros->nome; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Data de adesão:</strong>
                <p><?php 
                    $splited = explode(' ',$cadastros->data_add);
                    $timeS = explode(':',$splited[1]);
                    $time = $timeS[0].':'.$timeS[1];
                    $data = implode('/',array_reverse(explode('-', $splited[0])));
                    $join = $data." - ".$time; 
                    echo $join;
                    ?></p>
              </div>                
            </div>
            <div class="row">
            	<div class="col-sm-4">
                <strong>Nome Conforme Passaporte</strong>
                 <p><?php echo $cadastros->nome_passaporte; ?></p>
              </div>
              <div class="col-sm-4">
                <strong>Nº Passaporte</strong>
                 <p><?php echo $cadastros->numero_passaporte; ?></p>
              </div>
              <div class="col-sm-4">
                <strong>Validade Passaporte</strong>
                 <p><?php 
                     $dataPass = implode('/',array_reverse(explode('-', $cadastros->data_nascimento)));
                     echo $dataPass; 
                    ?>
                </p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <strong>CPF</strong>
                 <p><?php echo $cadastros->cpf; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Data nascimento</strong>
                 <p><?php 
                     $dataNasc = implode('/',array_reverse(explode('-', $cadastros->data_nascimento)));
                     echo $dataNasc; 
                    ?>
                </p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <strong>Sexo</strong>
                 <p><?php echo $cadastros->sexo; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>E-mail comercial</strong>
                 <p><?php echo $cadastros->email_comercial; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>E-mail pessoal</strong>
                 <p><?php echo $cadastros->email_pessoal; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Telefone comercial</strong>
                 <p><?php echo $cadastros->telefone_comercial; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Telefone pessoal</strong>
                 <p><?php echo $cadastros->telefone_residencial; ?></p>
              </div>
              <div class="col-sm-12">
                <strong>Telefone celular</strong>
                <p><?php echo $cadastros->telefone_celular; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Endereço comercial</strong>
                <p><?php echo $cadastros->endereco_comercial.', '.$cadastros->numero_comercial.((strlen($cadastros->complemento_comercial) > 0)?' - '.$cadastros->complemento_comercial:""); ?></p>
              </div>
              <div class="col-sm-6">
                <strong>CEP comercial</strong>
                <p><?php echo $cadastros->cep_comercial; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Bairro comercial</strong>
                <p><?php echo $cadastros->bairro_comercial; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Cidade comercial</strong>
                <p><?php echo $cadastros->cidade_comercial; ?></p>
              </div>
              <div class="col-sm-12">
                <strong>Estado comercial</strong>
                <p><?php echo $cadastros->estado_comercial; ?></p>
              </div>                
              <div class="col-sm-6">
                <strong>Endereço residencial</strong>
                <p><?php echo $cadastros->endereco_residencial.', '.$cadastros->numero_residencial.((strlen($cadastros->complemento_residencial) > 0)?' - '.$cadastros->complemento_residencial:""); ?></p>
              </div>
              <div class="col-sm-6">
                <strong>CEP residencial</strong>
                <p><?php echo $cadastros->cep_residencial; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Bairro residencial</strong>
                <p><?php echo $cadastros->bairro_residencial; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Cidade residencial</strong>
                <p><?php echo $cadastros->cidade_residencial; ?></p>
              </div>
              <div class="col-sm-12">
                <strong>Estado residencial</strong>
                <p><?php echo $cadastros->estado_residencial; ?></p>
              </div>  
              <div class="col-sm-6">
                <strong>Aeroporto</strong>
                <p><?php echo $cadastros->aeroporto; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Distância do Aeroporto</strong>
                <p><?php echo $cadastros->distancia_aero; ?></p>
              </div>
              <div class="col-sm-<?php echo ($cadastros->mesmo_aero=='S')?12:6; ?>">
                <strong>Aeroporto de retorno é o mesmo de origem?</strong>
                <p><?php
                    $mesmo = ($cadastros->mesmo_aero=='S')?"Sim":"Não";
                    echo $mesmo ?></p>
              </div> 
             <?php if($cadastros->mesmo_aero == "N"){ ?>
              <div class="col-sm-6">
                <strong>Aeroporto de Retorno:</strong>
                <p><?php echo $cadastros->aeroporto_retorno; ?></p>
              </div>                
             <?php } ?>
              <div class="col-sm-12">
                <strong>Contato emergencial</strong>
                <p>
                    Falar com <?php echo $cadastros->contato_emergencial.' - '.$cadastros->telefone_emergencial; ?>
                </p>
              </div>
            <div class="col-sm-<?php echo ($cadastros->plano_saude=='N')?12:6; ?>">
                <strong>Possui plano de saúde?</strong>
                <p><?php
                    $mesmo = ($cadastros->plano_saude=='S')?"Sim":"Não";
                    echo $mesmo ?></p>
              </div> 
            <?php if($cadastros->plano_saude == "S"){ ?>
              <div class="col-sm-6">
                <strong>Cobertura:</strong>
                <p><?php echo $cadastros->cobertura; ?></p>
              </div>                
             <?php } ?>
            <div class="col-sm-<?php echo ($cadastros->gestante=='N')?12:6; ?>">
                <strong>É gestante?</strong>
                <p><?php
                    $mesmo = ($cadastros->gestante=='S')?"Sim":"Não";
                    echo $mesmo ?></p>
              </div> 
             <?php if($cadastros->gestante == "S"){ ?>
              <div class="col-sm-6">
                <strong>Qual o período?</strong>
                <p><?php echo $cadastros->periodo; ?></p>
              </div>                
             <?php } ?>
             <div class="col-sm-<?php echo ($cadastros->tem_limitacao=='N')?12:6; ?>">
                <strong>Possui alguma limitação física?</strong>
                <p><?php
                    $mesmo = ($cadastros->tem_limitacao=='S')?"Sim":"Não";
                    echo $mesmo ?></p>
              </div> 
            <?php if($cadastros->tem_limitacao == "S"){ ?>
              <div class="col-sm-6">
                <strong>Qual?</strong>
                <p><?php echo $cadastros->limitacao_fisica; ?></p>
              </div>                
             <?php } ?>
            <div class="col-sm-6">
                <strong>Tamanho de camiseta:</strong>
                <p><?php echo $cadastros->camiseta ?></p>
              </div>
             <div class="col-sm-6">
                <strong>É fumante?</strong>
                <p><?php echo $cadastros->fumante ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Porco:</strong>
                <p><?php echo $cadastros->porco ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Bovino:</strong>
                <p><?php echo $cadastros->bovino ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Frango:</strong>
                <p><?php echo $cadastros->frango ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Peixe:</strong>
                <p><?php echo $cadastros->peixe ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Frutos mar:</strong>
                <p><?php echo $cadastros->frutos_mar ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Tem alguma restrição alimentar?</strong>
                <p><?php echo $cadastros->restricao_alimentar ?></p>
              </div>
            </div>                        
        </div>            
     </div>
            
     <div class="row">
        <div class="col-sm-6">
          <a href="javascript: window.history.go(-1)" class="btn btn-primary">Voltar</a>
        </div>
     </div>
           
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

</body>
</html>