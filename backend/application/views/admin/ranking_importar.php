    <div class="pageheader">
      <h2><i class="fa fa-sort"></i> Importar Ranking</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
          <li>Ranking</li>
          <li class="active">Importar Ranking</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Ranking</h3>
          <p>Aqui você pode atualizar o ranking enviando um arquivo xls ou xlsx.</p>
        </div>

        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/ranking/import_data') ?>" method="post" enctype="multipart/form-data">
            <div class="panel-body panel-body-nopadding">


            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label">Arquivo</label>
                <input type="file" name="arquivo" id="arquivo" class="form-control" required />
              </div>
              <div class="col-sm-6">
                <label class="control-label">Mês do Ranking</label>
                <select class="select form-control" name="mes">
                  <option value="8">Agosto</option>
                  <option value="9">Setembro</option>
                  <option value="10">Outubro</option>
                  <option value="11">Novembro</option>
                  <option value="12">Dezembro</option>
                </select>
              </div>
            </div>
          </div><!-- panel-body -->

          <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Importar</button>
                  </div>
               </div>
            </div>

        </form>
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";

	});


  });

</script>

</body>
</html>
