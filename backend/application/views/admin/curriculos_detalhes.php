    <div class="pageheader">
      <h2><i class="fa fa-file-text-o"></i> Detalhes de Currículos</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">BRF - Banco de Currículos</a></li>
          <li>Currículos</li>
          <li><a href="<?php echo base_url('admin/curriculos/listagem') ?>">Listagem</a></li>
          <li class="active">Detalhes</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Currículos</h3>
          <p>Visualize os dados completos de um currículo.</p>
        </div>
        
            <div class="panel-body">
          
            
            <div class="row">
              <div class="col-sm-8">
                <strong>Nome</strong>
                <p><?php echo $curriculo->nome;?></p>
              </div>
              <div class="col-sm-4">
                <strong>Data de Inclusão</strong>
                 <p><?php echo implode('/',array_reverse(explode('-',substr($curriculo->data_add,0,10)))).' '.substr($curriculo->data_add,11,5);?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-6">
                <strong>E-mail</strong>
                 <p><?php echo $curriculo->email;?></p>
              </div>
              <div class="col-sm-6">
                <strong>CPF</strong>
                <p><?php echo $curriculo->cpf;?></p>
              </div>
              <div class="col-sm-6">
                <strong>Data de Nascimento</strong>
                <p><?php echo implode('/',array_reverse(explode('-',$curriculo->nascimento)));?></p>
              </div>
              
            </div>
            
            </div>
     </div>
	<?php if(isset($pessoais)) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Dados Pessoais</h3>
        </div>

            
            <div class="panel-body">
            <div class="row">
              <div class="col-sm-3">
                <strong>Nacionalidade</strong>
                 <p><?php echo $pessoais->nacionalidade;?></p>
              </div>
              <div class="col-sm-3">
                <strong>Sexo</strong>
                <p><?php echo $pessoais->sexo;?></p>
              </div>
              <div class="col-sm-3">
                <strong>Estado Civil</strong>
                 <p><?php switch($pessoais->estado_civil) {
							case 1: echo 'Solterio(a)'; break; 
							case 2: echo 'Casado(a)'; break; 
							case 3: echo 'Separado(a)'; break; 
							case 4: echo 'Divorciado(a)'; break; 
							case 5: echo 'Viúvo(a)'; break; 
				 };?></p>
              </div>
              <div class="col-sm-3">
                <strong>Filhos</strong>
                <p><?php echo $pessoais->filhos;?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-6">
                <strong>Identidade</strong>
                 <p><?php echo $pessoais->identidade;?></p>
              </div>
              <div class="col-sm-3">
                <strong>Possui deficiência?</strong>
                <p><?php switch($pessoais->deficiencia) {
							case 'S': echo 'Sim'; break; 
							case 'N': echo 'Não'; break; 
				 };?></p>
              </div>
              <div class="col-sm-3">
                <strong>Tipo de Deficiência</strong>
                 <p><?php echo $pessoais->tipo_dficiencia;?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12">
                <strong>Observações</strong>
                 <p><?php echo nl2br($pessoais->obs);?></p>
              </div>
            </div>
            
            </div>
     </div>
	<?php } ?>
            
            
	<?php if(isset($contato)) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Dados de Contato</h3>
        </div>

            
            <div class="panel-body">
            
            <div class="row">
              <div class="col-sm-3">
                <strong>Endereço</strong>
                 <p><?php echo $contato->endereco;?></p>
              </div>
              <div class="col-sm-3">
                <strong>Complemento</strong>
                <p><?php echo $contato->complemento;?></p>
              </div>
              <div class="col-sm-3">
                <strong>Bairro</strong>
                 <p><?php echo $contato->bairro;?></p>
              </div>
              <div class="col-sm-3">
                <strong>CEP</strong>
                <p><?php echo $contato->cep;?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-4">
                <strong>Cidade</strong>
                 <p><?php echo $cidade->nome;?></p>
              </div>
              <div class="col-sm-4">
                <strong>Estado</strong>
                <p><?php echo $estado->nome;?></p>
              </div>
              <div class="col-sm-4">
                <strong>Pais</strong>
                 <p><?php echo $contato->pais;?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-4">
                <strong>Telefone</strong>
                 <p><?php echo $contato->telefone;?></p>
              </div>
              <div class="col-sm-4">
                <strong>Site</strong>
                <p><a href="<?php echo $contato->site;?>" target="_blank"><?php echo $contato->site;?></a></p>
              </div>
              <div class="col-sm-4">
                <strong>Outro</strong>
                 <p><?php echo $contato->outro;?></p>
              </div>
            </div>
            
            </div>
     </div>
	<?php } ?>
            
	<?php if(isset($objetivo)) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Objetivo Profissional</h3>
        </div>

            
        <div class="panel-body">
            
            <div class="row">
              <div class="col-sm-12">
                <strong>Objetivo Profissional</strong>
                 <p><?php echo $objetivo->objetivo;?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-8">
                <strong>Perfil Profissional</strong>
                 <p><?php echo nl2br($objetivo->perfil_profissional);?></p>
              </div>
              <div class="col-sm-4">
                <strong>Pretensão Salarial</strong>
                 <p><?php echo $objetivo->pretensao_salarial;?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-4">
                <strong>Região de Interesse</strong>
                <p><?php echo $objestado->nome;?> - <?php echo $objcidade->nome;?></p>
              </div>
              <div class="col-sm-4">
                <strong>Consideraria trabalhar em outra Cidade?</strong>
                <p><?php echo ($objetivo->outra_cidade=='S') ? 'Sim' : 'Não';?></p>
              </div>
              <div class="col-sm-4">
                <strong>Aceitaria viajar pela empresa?</strong>
                <p><?php echo ($objetivo->viagem=='S') ? 'Sim' : 'Não';?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12">
                <strong>Informações Complementares</strong>
                 <p><?php echo nl2br($objetivo->infos_comp);?></p>
              </div>
            </div>
            
            </div>
     </div>
	<?php } ?>
            
	<?php if(isset($anexo)) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Arquivo de Currículo</h3>
        </div>

            
        <div class="panel-body">
            <div class="row">
              <div class="col-sm-4">
                <strong>Arquivo</strong>
                 <p><a href="<?php echo base_url(UPLOADS_FOLDER.$anexo->nome); ?>" target="_blank">Visualizar</a></p>                                        
              </div>
            </div>
        </div>
     </div>
	<?php } ?>
            
	<?php if(isset($areas) && count($areas)>0) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Áreas de interesse</h3>
        </div>

            
        <div class="panel-body">
            <?php foreach($areas as $area) {?>
            <div class="row">
              <div class="col-sm-4">
                <strong>Nível</strong>
                 <p><?php switch($area->nivel) {
							case 1: echo 'Estagiário'; break; 
							case 2: echo 'Menor Aprendiz'; break; 
							case 3: echo 'Assistente'; break; 
							case 4: echo 'Analista'; break; 
							case 5: echo 'Coordenador/Especialista/Supervisor'; break; 
							case 6: echo 'Gerente'; break; 
							case 7: echo 'Diretor'; break; 
							case 8: echo 'Promotor'; break; 
							case 9: echo 'Vendedor'; break; 
							case 10: echo 'Operador'; break; 
				 };?></p>                                        
              </div>
              <div class="col-sm-8">
                <strong>Área</strong>
                 <p><?php echo (isset($area->nomeArea->nome)) ? $area->nomeArea->nome : '';?></p>
              </div>
            </div>
            <?php } ?>
        </div>
     </div>
	<?php } ?>
            
	<?php if(isset($experiencia)) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Experiência Profissional</h3>
        </div>

        <div class="panel-body">
            <div class="row">
              <div class="col-sm-4">
                <strong>Último Salário</strong>
                 <p><?php echo $experiencia->ultimo_salario;?></p>
              </div>
              <div class="col-sm-8">
                <strong>Data de Referência</strong>
                 <p><?php echo implode('/',array_reverse(explode('-',$experiencia->data_referencia)));?></p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-12">
                <strong>Outros Benefícios</strong>
                 <p><?php echo nl2br($experiencia->outros_beneficios);?></p>
              </div>
            </div>
        </div>
     </div>
	<?php } ?>
            
	<?php if(isset($trajetoria) && count($trajetoria)>0) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Trajetória Profissional</h3>
        </div>

            
        <div class="panel-body">
            <?php foreach($trajetoria as $traj) {?>
            <div class="row">
              <div class="col-sm-12">
                <strong>Empresa</strong>
                 <p><?php echo $traj->empresa;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <strong>Nacionalidade</strong>
                 <p><?php switch($traj->nacionalidade) {
							case 1: echo 'Nacional'; break; 
							case 2: echo 'Multinacional'; break;
				 };?></p>
              </div>
              <div class="col-sm-6">
                <strong>País</strong>
                 <p><?php echo $traj->pais;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <strong>Segmento</strong>
                 <p><?php switch($traj->segmento) {
							case 1: echo '(não classificado)'; break; 
							case 2: echo 'Agricultura e Pecuária'; break;
							case 3: echo 'Alimentos'; break;
							case 4: echo 'Arquitetura'; break;
							case 5: echo 'Assistência Médica'; break;
							case 6: echo 'Associações'; break;
							case 7: echo 'Auditoria'; break;
							case 8: echo 'Automotivo'; break;
							case 9: echo 'Autopeças'; break;
							case 10: echo 'Bancos'; break;
							case 11: echo 'Bebidas'; break;
							case 12: echo 'Brindes'; break;
							case 13: echo 'Brinquedos'; break;
							case 14: echo 'Calçados e Couro'; break;
							case 15: echo 'Cama, mesa e banho'; break;
							case 16: echo 'Comércio Atacadista'; break;
							case 17: echo 'Comércio Exterior'; break;
							case 18: echo 'Comércio Varejista'; break;
							case 19: echo 'Construção Civil'; break;
							case 20: echo 'Consultoria'; break;
							case 21: echo 'Contabilidade'; break;
							case 22: echo 'Corretoras'; break;
							case 23: echo 'Distribuidores'; break;
							case 24: echo 'Eletrodomésticos'; break;
							case 25: echo 'Embalagens'; break;
							case 26: echo 'Energia'; break;
							case 27: echo 'Engenharia'; break;
							case 28: echo 'Ensino e Pesquisa'; break;
							case 29: echo 'Entretenimento – Cultura e Lazer'; break;
							case 30: echo 'Esporte'; break;
							case 31: echo 'Farmacêutico'; break;
							case 32: echo 'Ferramentas'; break;
							case 33: echo 'Finanças'; break;
							case 34: echo 'Franquias'; break;
							case 35: echo 'Fumo'; break;
							case 36: echo 'Governo'; break;
							case 37: echo 'Gráfica'; break;
							case 38: echo 'Higiene e Limpeza'; break;
							case 39: echo 'Hoteleiro'; break;
							case 40: echo 'Imobiliário'; break;
							case 41: echo 'Imprensa e Comunicação'; break;
							case 42: echo 'Indústria'; break;
							case 43: echo 'Internet'; break;
							case 44: echo 'Jurídico'; break;
							case 45: echo 'Madeira'; break;
							case 46: echo 'Máquinas e Equipamentos'; break;
							case 47: echo 'Material de construção'; break;
							case 48: echo 'Material de escritório'; break;
							case 49: echo 'Material eletrônico (componentes eletrônicos)'; break;
							case 50: echo 'Mecânica'; break;
							case 51: echo 'Meio Ambiente'; break;
							case 52: echo 'Metalúrgico, Siderúrgico'; break;
							case 53: echo 'Mineração'; break;
							case 54: echo 'Móveis e Decoração'; break;
							case 55: echo 'Não Metálicos'; break;
							case 56: echo 'Papel e Celulose'; break;
							case 57: echo 'Perfumaria e Cosméticos'; break;
							case 58: echo 'Petroquímica'; break;
							case 59: echo 'Plástico e Borracha'; break;
							case 60: echo 'Publicidade'; break;
							case 61: echo 'Química'; break;
							case 62: echo 'Recursos Humanos'; break;
							case 63: echo 'Saúde, Hospitalar e Laboratorial'; break;
							case 64: echo 'Seguradoras e Previdência Privada'; break;
							case 65: echo 'Serviços (outros)'; break;
							case 66: echo 'Serviços Públicos'; break;
							case 67: echo 'Tecnologia e Informática'; break;
							case 68: echo 'Telecomunicações'; break;
							case 69: echo 'Telemarketing / Call Center'; break;
							case 70: echo 'Terceiro setor / ONG'; break;
							case 71: echo 'Têxtil'; break;
							case 72: echo 'Transporte e Logística'; break;
							case 73: echo 'Turismo'; break;
							case 74: echo 'Utilidades Domésticas'; break;
							case 75: echo 'Vestuário'; break;
				 };?></p>
              </div>
              <div class="col-sm-6">
                <strong>Porte</strong>
                 <p><?php switch($traj->porte) {
							case 1: echo 'Empresa de grande porte'; break; 
							case 2: echo 'Empresa de médio porte'; break;
							case 3: echo 'Empresa de pequeno porte'; break;
							case 4: echo 'Microempresa'; break;
							case 5: echo 'Atividade autônoma'; break;
				 };?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <strong>Início</strong>
                 <p><?php echo implode('/',array_reverse(explode('-',$traj->inicio)));?></p>
              </div>
              <div class="col-sm-6">
                <strong>Término</strong>
                 <p><?php echo implode('/',array_reverse(explode('-',$traj->termino)));?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <strong>Último Cargo</strong>
                 <p><?php echo $traj->ultimo_cargo;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <strong>Descrição</strong>
                 <p><?php echo nl2br($traj->descricao);?></p>
              </div>
            </div>
            <?php if(isset($traj->cargos) && count($traj->cargos)>0) {?>
            <div class="row">
              <div class="col-sm-12">
                <h5 class="subtitle mb5">Principais Ocupações</h5>
                <div class="table-responsive">
                  <table class="table table-striped mb30">
                    <thead>
                      <tr>
                        <th>Nível</th>
                        <th>Área</th>
                        <th>Inicio</th>
                        <th>Fim</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($traj->cargos as $cargo) {?>
                      <tr>
                        <td><?php echo $cargo->nivel;?></td>
                        <td><?php echo $cargo->area;?></td>
                        <td><?php echo implode('/',array_reverse(explode('-',$cargo->inicio)));?></td>
                        <td><?php echo implode('/',array_reverse(explode('-',$cargo->fim)));?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  </div>
              </div>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
     </div>
	<?php } ?>
            
	<?php if(isset($escolaridade)) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Escolaridade</h3>
        </div>
            
        <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
                <strong>Nivel de escolaridade</strong>
                 <p><?php switch($escolaridade->nivel_escolaridade) {
							case 1: echo 'Ensino Fundamental (1o. Grau) interrompido'; break; 
							case 2: echo 'Ensino Fundamental (1o. Grau) cursando'; break;
							case 3: echo 'Ensino Fundamental (1o. Grau) completo'; break;
							case 4: echo 'Ensino Médio (2o. Grau) interrompido'; break;
							case 5: echo 'Ensino Médio (2o. Grau) cursando'; break;
							case 6: echo 'Ensino Médio (2o. Grau) Profissionalizante cursando'; break;
							case 7: echo 'Ensino Médio (2o. Grau) completo'; break;
							case 8: echo 'Ensino Médio (2o. Grau) Profissionalizante completo'; break;
							case 9: echo 'Formação superior interrompida'; break;
							case 10: echo 'Formação superior (cursando)'; break;
							case 11: echo 'Formação superior completa'; break;
							case 12: echo 'Pós-graduação no nível Especialização'; break;
							case 13: echo 'Pós-graduação no nível Mestrado'; break;
							case 14: echo 'Pós-graduação no nível Doutorado'; break;
				 };?></p>
              </div>
            </div>
            
            
        </div>
     </div>
	<?php } ?>
            
	<?php if(isset($idiomas) && count($idiomas)>0) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Idiomas</h3>
        </div>

            
        <div class="panel-body">
			<?php foreach($idiomas as $idioma) {?>
            <div class="row">
              <div class="col-sm-6">
                <strong>Idioma</strong>
                 <p><?php switch($idioma->idioma) {
							case 1: echo 'Africânder'; break; 
							case 2: echo 'Albanês'; break;
							case 3: echo 'Alemão'; break;
							case 4: echo 'Árabe'; break;
							case 5: echo 'Azeri'; break;
							case 6: echo 'Bengalês'; break;
							case 7: echo 'Bielorusso'; break;
							case 8: echo 'Búlgaro'; break;
							case 9: echo 'Checo'; break;
							case 10: echo 'Chinês (cantonês)'; break;
							case 11: echo 'Chinês (mandarim)'; break;
							case 12: echo 'Coreano'; break;
							case 13: echo 'Croata'; break;
							case 14: echo 'Dinamarquês'; break;
							case 15: echo 'Escocês'; break;
							case 16: echo 'Eslovaco'; break;
							case 17: echo 'Esloveno'; break;
							case 18: echo 'Espanhol'; break;
							case 19: echo 'Estônio'; break;
							case 20: echo 'Farsi'; break;
							case 21: echo 'Finlandês'; break;
							case 22: echo 'Francês'; break;
							case 23: echo 'Georgiano'; break;
							case 24: echo 'Grego'; break;
							case 25: echo 'Hebraico'; break;
							case 26: echo 'Hindi'; break;
							case 27: echo 'Holandês'; break;
							case 28: echo 'Húngaro'; break;
							case 29: echo 'Indonésio'; break;
							case 30: echo 'Inglês'; break;
							case 31: echo 'Irlandês'; break;
							case 32: echo 'Islandês'; break;
							case 33: echo 'Italiano'; break;
							case 34: echo 'Japonês'; break;
							case 35: echo 'Lituano'; break;
							case 36: echo 'Macedônio'; break;
							case 37: echo 'Malaio'; break;
							case 38: echo 'Mongol'; break;
							case 39: echo 'Nepalês'; break;
							case 40: echo 'Norueguês'; break;
							case 41: echo 'Polaco'; break;
							case 42: echo 'Português'; break;
							case 43: echo 'Romeno'; break;
							case 44: echo 'Russo'; break;
							case 45: echo 'Sérvio'; break;
							case 46: echo 'Sueco'; break;
							case 47: echo 'Swahili'; break;
							case 48: echo 'Tailandês'; break;
							case 49: echo 'Tibetano'; break;
							case 50: echo 'Turco'; break;
							case 51: echo 'Ucraniano'; break;
							case 52: echo 'Urdu'; break;
							case 53: echo 'Usbeque'; break;
							case 54: echo 'Vietnamita'; break;
							case 55: echo 'Zulu'; break;
				 };?></p>
              </div>
              <div class="col-sm-2">
                <strong>Leitura</strong>
                 <p><?php switch($idioma->leitura) {
							case 1: echo '(não tem)'; break; 
							case 2: echo 'Básica'; break;
							case 3: echo 'Intermediária'; break;
							case 4: echo 'Avançada'; break;
							case 5: echo 'Fluente'; break;
				 };?></p>
              </div>
              <div class="col-sm-2">
                <strong>Escrita</strong>
                 <p><?php switch($idioma->escrita) {
							case 1: echo '(não tem)'; break; 
							case 2: echo 'Básica'; break;
							case 3: echo 'Intermediária'; break;
							case 4: echo 'Avançada'; break;
							case 5: echo 'Fluente'; break;
				 };?></p>
              </div>
              <div class="col-sm-2">
                <strong>Conversação</strong>
                 <p><?php switch($idioma->conversacao) {
							case 1: echo '(não tem)'; break; 
							case 2: echo 'Básica'; break;
							case 3: echo 'Intermediária'; break;
							case 4: echo 'Avançada'; break;
							case 5: echo 'Fluente'; break;
				 };?></p>
              </div>
            </div>
    		<?php } ?>
        </div>
     </div>
    <?php } ?>
    
    <?php if(isset($formacao) && count($formacao)>0) {?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Formação</h3>
        </div>

            
        <div class="panel-body">
            <?php foreach($formacao as $form) {?>
            <div class="row">
              <div class="col-sm-6">
                <strong>Curso</strong>
                 <p><?php echo $form->curso;?></p>
              </div>
              <div class="col-sm-6">
                <strong>Instituição</strong>
                 <p><?php echo $form->instituicao;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <strong>Estado</strong>
                 <p><?php echo $form->estado;?></p>
              </div>
              <div class="col-sm-6">
                <strong>País</strong>
                 <p><?php echo $form->pais;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <strong>Tipo de Curso</strong>
                 <p><?php echo $form->tipo_curso;?></p>
              </div>
              <div class="col-sm-4">
                <strong>Classificação do curso</strong>
                 <p><?php echo $form->classificacao_curso;?></p>
              </div>
              <div class="col-sm-4">
                <strong>Área</strong>
                 <p><?php echo $form->area;?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <strong>Duração total</strong>
                 <p><?php echo $form->duracao_total;?></p>
              </div>
              <div class="col-sm-6">
                <strong>Conclusão</strong>
                 <p><?php echo ($form->cursando==1) ? 'Cursando' : $form->conclusao;?></p>
              </div>
            </div>
            <?php } ?>
        </div>
     </div>
	<?php } ?>
                
           <div class="row">
              <div class="col-sm-6">
                <a href="javascript: window.history.go(-1)" class="btn btn-primary">Voltar</a>
              </div>
           </div>
           
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

</body>
</html>
