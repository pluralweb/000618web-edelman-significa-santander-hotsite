<div class="pageheader">
    <h2><i class="fa fa-pencil"></i> Configurações</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
            <li>Configurações</li>
            <li class="active"> Gerais</li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Gerais</h3>
            <p>Aqui você cadastra o informações gerais para o site.</p>
        </div>

        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/configuracoes/addGerais_cdc') ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="update" value="<?php echo $update; ?>">
            <div class="panel-body panel-body-nopadding">
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Inicio da campanha</label>
                    </div>
                    <div class="col-sm-12">
                      <label class="control-label">CDC</label>
                      <input type="text" name="cdc" value="<?php echo $info->cdc; ?>" class="form-control datepicker" />
                    </div>
                    <div class="col-sm-12">
                      <label class="control-label">Veículos</label>
                      <input type="text" name="veiculo" value="<?php echo $info->veiculo; ?>" class="form-control datepicker" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Título</label>
                        <input type="text" name="titulo" value="<?php echo $info->titulo; ?>" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Descrição</label>
                        <input type="text" name="descricao" value="<?php echo $info->descricao; ?>" class="form-control " />
                    </div>
                </div>
                <div class="panel-body panel-body-nopadding">
                 <div class="form-group">
                  <div class="col-sm-12">
                    <label class="control-label" for="descricao_br">Regulamento</label>
                    <textarea id="bio" placeholder="Digite o regulamento aqui..." name="config" class="form-control wycs" rows="10"><?php echo $info->regulamento; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <label class="checkboxCustom">
                      <input type="checkbox" class="youtube" name="youtube" <?php echo ($info->youtube) ? 'checked' : ''; ?> >
                      <span class="mr"></span>
                      Vídeo do youtube?
                    </label>
                    <label class="control-label">Vídeo da Campanha</label>
                    <input type="text" name="linkPromocao" value="<?php echo $info->linkPromocao; ?>" class="form-control changeUp" <?php echo ($info->youtube == 0) ? 'style="display:none"' : ''; ?>/>
                    <?php if ($info->youtube == 0): ?>
                      <video src="<?php echo base_url(VIDEOS_FOLDER.$info->linkPromocao) ?>" controls class="form-control"></video>
                      <br>
                    <?php endif; ?>
                    <label class="control-label warningVideo">*O vídeo não deve ser maior que 20Mb e em formato ".mp4"</label>
                    <input type="file" name="linkVideo" value="<?php echo $info->linkPromocao; ?>" class="form-controlchangeUp" <?php echo ($info->youtube == 1) ? 'style="display:none"' : ''; ?>/>
                  </div>
                  <div class="col-sm-12">
                    <label class="control-label"></label>
                    <label class="checkboxCustom">
                      <input type="checkbox" name="autoplay" <?php echo ($info->autoplay == 1) ? 'checked' : ''; ?> >
                      <span class="mr"></span>
                      Autoplay?
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <label class="control-label">Link Google Plus</label>
                    <input type="text" name="plus" value="<?php echo $info->plus; ?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <label class="control-label">Link Facebook</label>
                    <input type="text" name="linkFacebook" value="<?php echo $info->linkFacebook; ?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <label class="control-label">Link Twitter</label>
                    <input type="text" name="linkTwitter" value="<?php echo $info->linkTwitter; ?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <label class="control-label">Link Youtube</label>
                    <input type="text" name="linkYoutube" value="<?php echo $info->linkYoutube; ?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <?php if ($info->imgFacebook): ?>
                    <div class="col-sm-4 img" style="margin: 0 auto; float: none">
                      <img src="<?php echo base_url(IMAGENS_FOLDER.$info->imgFacebook) ?>" alt="" />
                    </div>
                  <?php endif; ?>
                  <div class="col-sm-12">
                    <label class="control-label" for="imagem">Imagem Share Facebook (ideal 600×600)</label>
                    <input type="file" name="imgFacebook" id="imgFacebook" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <?php if ($info->imgTwitter): ?>
                    <div class="col-sm-4 img" style="margin: 0 auto; float: none">
                      <img src="<?php echo base_url(IMAGENS_FOLDER.$info->imgTwitter) ?>" alt="" />
                    </div>
                  <?php endif; ?>
                  <div class="col-sm-12">
                    <label class="control-label" for="imagem">Imagem Share Twitter (mínimo 280x150px)</label>
                    <input type="file" name="imgTwitter" id="imgTwitter" class="form-control" />
                  </div>
                </div>

            </div>
            <!-- panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                    </div>
                </div>
            </div>

        </form>
    </div>

</div>
<!-- contentpanel -->

</div>
<!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.tagsinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/colorpicker.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/wysihtml5-0.3.0.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-wysihtml5.js') ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
    $(document).ready(function($) {

        "use strict";

        $(".select2").select2({
            width: '100%'
        });

        $(document).on('click', ".remover", function(e) {
            e.preventDefault();
            var el = $(this);
            var qtde = el.closest('#interesses').find('.relac').length;
            if (qtde > 1) {
                $(this).closest('.relac').remove();
            }
        });
        $("#addInt").click(function(e) {
            e.preventDefault();
            var clone = $('#interesses .relac:first').clone();
            $('#interesses').append(
                '<div class="row relac"><div class="col-sm-5"><input type="text" name="interesse[]" class="form-control" /></div><div class="col-sm-5"><select name="tipo[]" data-placeholder="Selecione" title="Selecione a opção" class="select2 cenarios_rel"><option>Selecione o interesse...</option><option value="1">Livros</option><option value="2">Filmes</option><option value="3">Cantores</option></select></div><div class="col-sm-2"><a href="#" title="Remover" class="btn btn-default btn remover"><i class="glyphicon glyphicon-trash"></i></a></div><br /><br /></div>'
            ).find('select').select2({
                width: '100%'
            });
        });

        $('.wycs').wysihtml5({
            "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": true, //Button which allows you to edit the generated HTML. Default false
            "link": true, //Button to insert a link. Default true
            "image": true //Button to insert an image. Default true,
        });

        $('.youtube').on('change', function(){
            var p = $('input[name="linkPromocao"]');
            var video = $('video');
            var w = $('.warningVideo');
            var v = $('input[name="linkVideo"]');

            if(!$(this).is(':checked')){
              p.hide();
              v.show();
              video.show();
              w.show();
            }else{
              video.hide();
              w.hide();
              v.hide();
              p.show();
            }

        })


    });
</script>

</body>

</html>
