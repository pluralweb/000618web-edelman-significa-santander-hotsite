    <div class="pageheader">
      <h2><i class="fa fa-group"></i> Ranking</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
          <li>Ranking</li>
          <li class="active">Listagem</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Ranking</h3>
          <p>Esta lista mostra todos os cadastros do ranking.</p>
        </div>
        <div class="panel-body">
          <?php if($importRanking) {?>
          <div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <strong>Atenção!</strong> Lista de ranking atualizada com sucesso!
          </div>
          <?php } ?>
          <!-- table-responsive -->
          <div class="table-responsive">
          <table class="table table-striped" id="listagem">
              <thead>
                 <tr>
                    <th>Mês</th>
                    <th>Nome</th>
                    <th>Matrícula</th>
                    <th>Elegível</th>
                    <th>Colocação</th>
                    <th>Pontuação</th>
                    <th>Pontos Primeiro</th>
                    <th>Pontos Anterior</th>
                    <th>Pontos Posterior</th>
                 </tr>
              </thead>
              <tbody>
                <?php if ($ranking): ?>
                  <?php foreach($ranking as $func) {?>
                    <tr>
                      <td><?php echo $func->mes;?></td>
                      <td><?php echo $func->nome;?></td>
                      <td><?php echo $func->matricula;?></td>
                      <td><?php echo $func->elegivel;?></td>
                      <td><?php echo $func->colocacao;?></td>
                      <td><?php echo $func->pontuacao;?></td>
                      <td><?php echo $func->pontos_primeiro;?></td>
                      <td><?php echo $func->pontos_anterior;?></td>
                      <td><?php echo $func->pontos_depois;?></td>
                    </tr>
                    <?php } ?>

                <?php endif; ?>

              </tbody>
           </table>
          </div><!-- table-responsive -->

        </div><!-- panel-body -->
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";

    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });

    // Select2
    jQuery('select').select2({
    });

    jQuery('select').removeClass('form-control');


  });
</script>

</body>
</html>
