    <div class="pageheader">
      <h2><i class="fa fa-mobile"></i> Listagem de Telefones</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
          <li>Telefones</li>
          <li class="active">Listagem</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Telefones</h3>
          <p>Esta lista mostra todos os telefones que estão cadastrados no sistema.</p>
          <a href="<?php echo base_url('admin/telefones/exportar') ?>" class="btn btn-sm btn-primary">Exportar Excel</a>
        </div>
        <div class="panel-body">
          <!-- table-responsive -->
          <div class="table-responsive">
          <table class="table table-striped" id="listagem">
              <thead>
                 <tr>
                    <th>Telefone</th>
                    <th>Data de Cadastro</th>
                 </tr>
              </thead>
              <tbody>
                 <?php foreach($telefones as $tel) {?>
                 <tr>
                    <td><?php echo $tel->telefone;?></td>
                    <td data-order="<?php echo (isset($tel->data_add)) ? $tel->data_add : ''; ?>"><?php echo implode('/',array_reverse(explode('-',substr($tel->data_add,0,10)))).' '.substr($tel->data_add,11,5); ?></td>
                 </tr>
                 <?php } ?>
              </tbody>
           </table>
          </div><!-- table-responsive -->
          
        </div><!-- panel-body -->
      </div>
      
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {
    
    "use strict";
    
    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });
    
    // Select2
    jQuery('select').select2({
    });
    
    jQuery('select').removeClass('form-control');
    
    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });
        
        return false;
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
  
  
  });
</script>

</body>
</html>
