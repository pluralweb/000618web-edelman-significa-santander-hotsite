    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> Listagem de Vagas</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">BRF - Banco de Currículos</a></li>
          <li>Vagas</li>
          <li><a href="<?php echo base_url('admin/vagas/listagem') ?>">Listagem</a></li>
          <li class="active">Detalhes</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Vagas</h3>
          <p>Visualize os dados completos de uma vaga.</p>
        </div>
        
            <div class="panel-body">
          
            
            <div class="row">
              <div class="col-sm-4">
                <strong>Código</strong>
                <p><?php echo $vaga->codigo;?></p>
              </div>
              <div class="col-sm-4">
                <strong>Data de Inclusão</strong>
                 <p><?php echo implode('/',array_reverse(explode('-',substr($vaga->data_add,0,10)))).' '.substr($vaga->data_add,11,5);?></p>
              </div>
              <div class="col-sm-4">
                <strong>Status</strong>
                   <p><?php echo ($vaga->status) ? 'Ativo' : 'Inativo';?></p>
             </div>
            </div>
            
            <div class="row">
              <div class="col-sm-6">
                <strong>Nome da vaga</strong>
                 <p><?php echo $vaga->nome;?></p>
              </div>
              <div class="col-sm-6">
                <strong>Área</strong>
                <p><?php echo $vaga->area;?></p>
              </div>
              
            </div>
            
            <div class="row">
              <div class="col-sm-6">
                  <strong>Localização</strong>
                  <p><?php echo $vaga->localizacao; ?></p>
              </div>
              <div class="col-sm-6">
                <strong>Link</strong>
                <p><a href="<?php echo $vaga->link; ?>" target="_blank"><?php echo $vaga->link; ?></a></p>
              </div>
            </div>  
            <div class="row">
            <div class="col-sm-12">
                  <strong>Descrição</strong>
                    <p><?php echo $vaga->texto; ?></p>
              </div>
            </div>
             <div class="row">
                <div class="col-sm-6">
                  <a href="javascript: window.history.go(-1)" class="btn btn-primary">Voltar</a>
                </div>
             </div>
           
          </div><!-- panel-body -->
        
      </div>
      
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

</body>
</html>
