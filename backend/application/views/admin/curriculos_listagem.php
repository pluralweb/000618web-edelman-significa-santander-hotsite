    <div class="pageheader">
      <h2><i class="fa fa-file-text-o"></i> Listagem de Currículos</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">BRF - Banco de Currículos</a></li>
          <li>Currículos</li>
          <li class="active">Listagem</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Currículos</h3>
          <p>Esta lista mostra todos os currículos que estão cadastrados no sistema.</p>
        </div>
        <div class="panel-body">
        	<form action="<?php echo base_url('admin/curriculos/filtro/'); ?>" method="get">
            <div class="boxFilter">
            	<div class="col-xs-12 col-sm-2">
                	<label class="control-label">Gênero</label><br />
                    <select name="genero" class="" style="width:100%">
                        <option value="">Selecione</option>
                        <option value="F"<?php if($this->input->get('genero')=='F') {?> selected="selected"<?php } ?>>Feminino</option>
                        <option value="M"<?php if($this->input->get('genero')=='M') {?> selected="selected"<?php } ?>>Masculino</option>
                        <option value="O"<?php if($this->input->get('genero')=='O') {?> selected="selected"<?php } ?>>Prefiro não informar</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-1">
                    <label class="control-label">Idade entre </label><br />
                    <input type="number" class="form-control" id="" name="idade1" value="<?php echo $this->input->get('idade1'); ?>"> e 
                    <input type="number" class="form-control" id="" name="idade2" value="<?php echo $this->input->get('idade2'); ?>">
                </div>
                <div class="col-xs-12 col-sm-3">
                	<label class="control-label">Nível de escolaridade:</label><br />
                    <select name="escolaridade" class="form-control" style="width:100%">
                        <option value="">Selecione</option>
                        <option value="1"<?php if($this->input->get('escolaridade')==1) {?> selected="selected"<?php } ?>>Ensino Fundamental (1o. Grau) interrompido</option>
                        <option value="2"<?php if($this->input->get('escolaridade')==2) {?> selected="selected"<?php } ?>>Ensino Fundamental (1o. Grau) cursando</option>
                        <option value="3"<?php if($this->input->get('escolaridade')==3) {?> selected="selected"<?php } ?>>Ensino Fundamental (1o. Grau) completo</option>
                        <option value="4"<?php if($this->input->get('escolaridade')==4) {?> selected="selected"<?php } ?>>Ensino Médio (2o. Grau) interrompido</option>
                        <option value="5"<?php if($this->input->get('escolaridade')==5) {?> selected="selected"<?php } ?>>Ensino Médio (2o. Grau) cursando</option>
                        <option value="6"<?php if($this->input->get('escolaridade')==6) {?> selected="selected"<?php } ?>>Ensino Médio (2o. Grau) Profissionalizante cursando</option>
                        <option value="7"<?php if($this->input->get('escolaridade')==7) {?> selected="selected"<?php } ?>>Ensino Médio (2o. Grau) completo</option>
                        <option value="8"<?php if($this->input->get('escolaridade')==8) {?> selected="selected"<?php } ?>>Ensino Médio (2o. Grau) Profissionalizante completo</option>
                        <option value="9"<?php if($this->input->get('escolaridade')==9) {?> selected="selected"<?php } ?>>Formação superior interrompida</option>
                        <option value="10"<?php if($this->input->get('escolaridade')==10) {?> selected="selected"<?php } ?>>Formação superior (cursando)</option>
                        <option value="11"<?php if($this->input->get('escolaridade')==11) {?> selected="selected"<?php } ?>>Formação superior completa</option>
                        <option value="12"<?php if($this->input->get('escolaridade')==12) {?> selected="selected"<?php } ?>>Pós-graduação no nível Especialização</option>
                        <option value="13"<?php if($this->input->get('escolaridade')==13) {?> selected="selected"<?php } ?>>Pós-graduação no nível Mestrado</option>
                        <option value="14"<?php if($this->input->get('escolaridade')==14) {?> selected="selected"<?php } ?>>Pós-graduação no nível Doutorado</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-2">
	                <label class="control-label">Área de interesse:</label><br />
                    <select name="area" class="" style="width:100%">
                        <option value="">Selecione</option>
                        <option value="16"<?php if($this->input->get('area')==16) {?> selected="selected"<?php } ?>>Agropecuária</option>
                        <option value="1"<?php if($this->input->get('area')==1) {?> selected="selected"<?php } ?>>Comercial</option>
                        <option value="8"<?php if($this->input->get('area')==8) {?> selected="selected"<?php } ?>>Compras</option>
                        <option value="18"<?php if($this->input->get('area')==18) {?> selected="selected"<?php } ?>>Comunicação</option>
                        <option value="7"<?php if($this->input->get('area')==7) {?> selected="selected"<?php } ?>>Fábrica</option>
                        <option value="6"<?php if($this->input->get('area')==6) {?> selected="selected"<?php } ?>>Finanças</option>
                        <option value="3"<?php if($this->input->get('area')==3) {?> selected="selected"<?php } ?>>Gestão de Processos</option>
                        <option value="10"<?php if($this->input->get('area')==10) {?> selected="selected"<?php } ?>>Jurídico</option>
                        <option value="9"<?php if($this->input->get('area')==9) {?> selected="selected"<?php } ?>>Logística</option>
                        <option value="4"<?php if($this->input->get('area')==4) {?> selected="selected"<?php } ?>>Marketing</option>
                        <option value="12"<?php if($this->input->get('area')==12) {?> selected="selected"<?php } ?>>Pesquisa e Desenvolvimento</option>
                        <option value="2"<?php if($this->input->get('area')==2) {?> selected="selected"<?php } ?>>Pricing</option>
                        <option value="13"<?php if($this->input->get('area')==13) {?> selected="selected"<?php } ?>>Qualidade</option>
                        <option value="11"<?php if($this->input->get('area')==11) {?> selected="selected"<?php } ?>>RH</option>
                        <option value="15"<?php if($this->input->get('area')==15) {?> selected="selected"<?php } ?>>Saúde e Segurança do Trabalho</option>
                        <option value="14"<?php if($this->input->get('area')==14) {?> selected="selected"<?php } ?>>Sustentabilidade</option>
                        <option value="5"<?php if($this->input->get('area')==5) {?> selected="selected"<?php } ?>>Trade Marketing</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-2">
                	<label class="control-label">Nível Hierárquico Profissional:</label><br />
                    <select name="nivel" class="" style="width:100%">
                        <option value="">Selecione</option>
                        <option value="1"<?php if($this->input->get('nivel')==1) {?> selected="selected"<?php } ?>>Estagiário</option>
                        <option value="2"<?php if($this->input->get('nivel')==2) {?> selected="selected"<?php } ?>>Menor Aprendiz</option>
                        <option value="3"<?php if($this->input->get('nivel')==3) {?> selected="selected"<?php } ?>>Assistente</option>
                        <option value="4"<?php if($this->input->get('nivel')==4) {?> selected="selected"<?php } ?>>Analista</option>
                        <option value="5"<?php if($this->input->get('nivel')==5) {?> selected="selected"<?php } ?>>Coordenador/Especialista/Supervisor</option>
                        <option value="6"<?php if($this->input->get('nivel')==6) {?> selected="selected"<?php } ?>>Gerente</option>
                        <option value="7"<?php if($this->input->get('nivel')==7) {?> selected="selected"<?php } ?>>Diretor</option>
                        <option value="8"<?php if($this->input->get('nivel')==8) {?> selected="selected"<?php } ?>>Promotor</option>
                        <option value="9"<?php if($this->input->get('nivel')==9) {?> selected="selected"<?php } ?>>Vendedor</option>
                        <option value="10"<?php if($this->input->get('nivel')==10) {?> selected="selected"<?php } ?>>Operador</option>
                    </select>
                </div>
                
                <div class="col-xs-12 col-sm-2">
                	<label class="control-label">É portador de deficiência</label><br />
                    <select name="deficiente" class="" style="width:100%">
                        <option value="">Selecione</option>
                        <option value="S"<?php if($this->input->get('deficiente')=='S') {?> selected="selected"<?php } ?>>Sim</option>
                        <option value="N"<?php if($this->input->get('deficiente')=='N') {?> selected="selected"<?php } ?>>Não</option>
                    </select>
                </div>
                
                <div class="col-sm-12">
                	<label class="control-label">&nbsp;</label>
                </div>
                
                <div class="col-xs-12 col-sm-2">
                	<label class="control-label">Estado:</label><br />
                    <select name="estado" id="estadoChange" class="" style="width:100%">
                        <option value="">Selecione</option>
                        <?php foreach($estados as $est) {?>
                        <option value="<?php echo $est->id;?>"<?php if($this->input->get('estado')==$est->id) {?> selected="selected"<?php } ?>><?php echo $est->nome;?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-2">
                	<label class="control-label">Cidade:</label><br />
                    <select name="cidade" id="cidadeChange" class="" style="width:100%">
                    	<option value="">Selecione</option>
                    	<?php foreach($cidades as $cid) {?>
                        <option value="<?php echo $cid->id;?>"<?php if($this->input->get('cidade')==$cid->id) {?> selected="selected"<?php } ?>><?php echo $cid->nome;?></option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class="col-xs-12 col-sm-3">
                	<label class="control-label">Disponibilidade para mudança:</label><br />
                    <select name="mudanca" class="" style="width:100%">
                        <option value="">Selecione</option>
                        <option value="S"<?php if($this->input->get('mudanca')=='S') {?> selected="selected"<?php } ?>>Sim</option>
                        <option value="N"<?php if($this->input->get('mudanca')=='N') {?> selected="selected"<?php } ?>>Não</option>
                    </select>
                </div>
                
                <div class="col-xs-12 col-sm-3">
                	<label class="control-label">Disponibilidade para viagens:</label><br />
                    <select name="viagens" class="" style="width:100%">
                        <option value="">Selecione</option>
                        <option value="S"<?php if($this->input->get('viagens')=='S') {?> selected="selected"<?php } ?>>Sim</option>
                        <option value="N"<?php if($this->input->get('viagens')=='N') {?> selected="selected"<?php } ?>>Não</option>
                    </select>
                </div>
                
                <div class="col-xs-12 col-sm-2">
                	<label class="control-label">&nbsp;</label><br />
                	<input type="submit" class="btn btn-primary" value="Filtrar" style="width:100%">
                </div>
                
                <div class="col-sm-12">
                	<label class="control-label">&nbsp;</label>
                </div>
                
            </div>
            </form>
          <!-- table-responsive -->
          <div class="table-responsive">
          <table class="table table-striped" id="listagem">
              <thead>
                 <tr>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>CPF</th>
                    <th>Data de Nascimento</th>
                    <th>Data de Cadastro</th>
                    <th>Ações</th>
                 </tr>
              </thead>
              <tbody>
                 <?php foreach($curriculos as $curriculo) {?>
                 <tr>
                    <td><?php echo $curriculo->nome;?></td>
                    <td><?php echo $curriculo->email; ?></td>
                    <td><?php echo $curriculo->cpf; ?></td>
                    <td data-order="<?php echo (isset($curriculo->nascimento)) ? $curriculo->nascimento : ''; ?>"><?php echo implode('/',array_reverse(explode('-',$curriculo->nascimento))); ?></td>
                    <td data-order="<?php echo (isset($curriculo->data_add)) ? $curriculo->data_add : ''; ?>"><?php echo implode('/',array_reverse(explode('-',substr($curriculo->data_add,0,10)))).' '.substr($curriculo->data_add,11,5); ?></td>
                    <td>
                      <a href="<?php echo base_url('admin/curriculos/detalhes/'.$curriculo->id)?>" title="Detalhes" class="btn btn-default btn"><i class="glyphicon glyphicon-search"></i></a>
                    </td>
                 </tr>
                 <?php } ?>
              </tbody>
           </table>
          </div><!-- table-responsive -->
          
        </div><!-- panel-body -->
      </div>
      
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {
    
    "use strict";
    
    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });
    
    // Select2
    jQuery('select').select2({
    });
    
    jQuery('select').removeClass('form-control');
    
    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });
        
        return false;
    });
    
    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });
	
  
  });
</script>

</body>
</html>
