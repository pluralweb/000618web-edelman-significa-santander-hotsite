<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png') ?>" type="image/png">

  <title>Santander - Desafio 2016</title>

  <link href="<?php echo base_url('assets/css/style.default.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/style.katniss.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/jquery.datatables.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap-editable.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/font.roboto.css'); ?>" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url('assets/js/html5shiv.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/respond.min.js') ?>"></script>
  <![endif]-->
</head>

<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

  <div class="leftpanel">

    <div class="logopanel">
        <h1><span>[</span> Santander <span>]</span></h1>
    </div><!-- logopanel -->

    <div class="leftpanelinner">

        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">
            <div class="media userlogged">
                <img alt="" src="<?php echo base_url('assets/images/photos/user1.png') ?>" class="media-object">
                <div class="media-body">
                    <h4><?php echo $nome; ?></h4>
                </div>
            </div>

            <h5 class="sidebartitle actitle">Conta</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
              <li><a href="<?php echo base_url('admin/home/perfil') ?>"><i class="fa fa-user"></i> <span>Perfil</span></a></li>
              <li><a href="<?php echo base_url('admin/home/logout') ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
            </ul>
        </div>

      <h5 class="sidebartitle">Menu</h5>
      <ul class="nav nav-pills nav-stacked nav-bracket">
        <li<?php if($sessao=='home') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/') ?>"><i class="fa fa-home"></i> <span>Home</span></a></li>
        <li class="<?php if($sessao=='funcionarios') {?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-group"></i> <span>Funcionários</span></a>
        	<ul class="children"<?php if($sessao=='funcionarios') {?> style="display:block"<?php } ?>>
            <li<?php if($sessao=='funcionarios' && $subsessao=='listagem') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/funcionarios/index') ?>"><i class="fa fa-caret-right"></i> Ver Todos</a></li>
            <li<?php if($sessao=='funcionarios' && $subsessao=='importar') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/funcionarios/importar') ?>"><i class="fa fa-caret-right"></i> Importar</a></li>
            <li<?php if($sessao=='funcionarios' && $subsessao=='inserir') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/funcionarios/inserir') ?>"><i class="fa fa-caret-right"></i> Inserir</a></li>
          </ul>
        </li>
        <li class="<?php if($sessao=='ranking') {?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-sort"></i> <span>Ranking</span></a>
        	<ul class="children"<?php if($sessao=='ranking') {?> style="display:block"<?php } ?>>
            <li<?php if($sessao=='ranking' && $subsessao=='listagem') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/ranking/index') ?>"><i class="fa fa-caret-right"></i> Ver Todos</a></li>
            <li<?php if($sessao=='ranking' && $subsessao=='importar') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/ranking/importar') ?>"><i class="fa fa-caret-right"></i> Importar</a></li>
          </ul>
        </li>
        <li class="<?php if($sessao=='metas') {?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-sort"></i> <span>Metas</span></a>
        	<ul class="children"<?php if($sessao=='metas') {?> style="display:block"<?php } ?>>
            <li<?php if($sessao=='metas' && $subsessao=='listagem') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/metas/index') ?>"><i class="fa fa-caret-right"></i> Ver Todos</a></li>
            <li<?php if($sessao=='metas' && $subsessao=='importar') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/metas/importar') ?>"><i class="fa fa-caret-right"></i> Importar</a></li>
          </ul>
        </li>
        <li class="<?php if($sessao=='videos') {?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-film"></i> <span>Mídia</span></a>
        	<ul class="children"<?php if($sessao=='videos') {?> style="display:block"<?php } ?>>
            <li<?php if($sessao=='videos' && $subsessao=='listagem') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/videos/index') ?>"><i class="fa fa-caret-right"></i> Ver Todos</a></li>
            <li<?php if($sessao=='videos' && $subsessao=='inserir') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/videos/inserir') ?>"><i class="fa fa-caret-right"></i> Inserir</a></li>
          </ul>
        </li>
          <li class="<?php if ($sessao == 'configuracoes_cdc') { ?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-gear"></i> <span>Configurações CDC</span></a>
              <ul class="children"<?php if ($sessao == 'configuracoes_cdc') { ?> style="display:block"<?php } ?>>
                <li<?php if ($sessao == 'configuracoes_cdc' && $subsessao == 'geral_cdc') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/configuracoes/geral_cdc') ?>"><i class="fa fa-caret-right"></i>Geral</a></li>
                <li<?php if ($sessao == 'configuracoes_cdc' && $subsessao == 'faq_cdc') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/configuracoes/faq_cdc') ?>"><i class="fa fa-caret-right"></i> FAQ</a></li>
            </ul>
          </li>
          <li class="<?php if ($sessao == 'configuracoes_veiculos') { ?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-gear"></i> <span>Configurações Veículos</span></a>
              <ul class="children"<?php if ($sessao == 'configuracoes_veiculos') { ?> style="display:block"<?php } ?>>
                <li<?php if ($sessao == 'configuracoes_veiculos' && $subsessao == 'geral_veiculos') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/configuracoes/geral_veiculos') ?>"><i class="fa fa-caret-right"></i>Geral</a></li>
                <li<?php if ($sessao == 'configuracoes_veiculos' && $subsessao == 'faq_veiculos') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/configuracoes/faq_veiculos') ?>"><i class="fa fa-caret-right"></i> FAQ</a></li>
            </ul>
          </li>
          <li class="<?php if ($sessao == 'usuarios') { ?>nav-active <?php } ?>nav-parent"><a href=""><i class="fa fa-user"></i> <span>Usuários</span></a>
              <ul class="children"<?php if ($sessao == 'usuarios') { ?> style="display:block"<?php } ?>>
                  <li<?php if ($sessao == 'usuarios' && $subsessao == 'listagem') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/usuarios/index') ?>"><i class="fa fa-caret-right"></i> Ver Todos</a></li>
                  <li<?php if ($sessao == 'usuarios' && $subsessao == 'inserir') { ?> class="active"<?php } ?>><a href="<?php echo base_url('admin/usuarios/inserir') ?>"><i class="fa fa-caret-right"></i> Inserir</a></li>
              </ul>
          </li>
          <?php if (false): ?>
            <li<?php if($sessao=='telefones') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/telefones/') ?>"><i class="fa fa-mobile"></i> <span>Telefones</span></a></li>
            <li<?php if($sessao=='viagens') {?> class="active"<?php } ?>><a href="<?php echo base_url('admin/Viagens') ?>"><i class="fa fa-plane"></i> <span>Viagens</span></a></li>
          <?php endif; ?>
      </ul>

    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->

  <div class="mainpanel">

  <div class="headerbar">

    <a class="menutoggle"><i class="fa fa-bars"></i></a>

    <div class="header-right">
      <ul class="headermenu">
        <li>
          <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/images/photos/user1.png') ?>" alt="" />
              <?php echo $nome; ?>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
              <li><a href="<?php echo base_url('admin/home/perfil') ?>"><i class="glyphicon glyphicon-user"></i> Meu Perfil</a></li>
              <li><a href="<?php echo base_url('admin/home/logout') ?>"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </div><!-- header-right -->

  </div><!-- headerbar -->
