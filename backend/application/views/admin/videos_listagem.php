    <div class="pageheader">
      <h2><i class="fa fa-film"></i> Listagem de Vídeos</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
          <li>Vídeos</li>
          <li class="active">Listagem</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Vídeos</h3>
          <p>Esta lista mostra todos os vídeos que estão cadastrados no sistema.</p>
        </div>
        <div class="panel-body">
          <!-- table-responsive -->
          <div class="table-responsive">
          <table class="table table-striped" id="listagem">
              <thead>
                 <tr>
                    <th>Tipo de Mídia</th>
                    <th>Mídia</th>
                    <th>Ações</th>
                 </tr>
              </thead>
              <tbody>
                 <?php foreach($videos as $video) {?>
                 <tr>
                    <td><?php echo ($video->video == 1) ? 'Vídeo' : 'Imagem';?></td>
                    <td>
                      <?php if ($video->video == 1): ?>
                        <?php
                        $link = $video->link;
                        if($link) {
                          preg_match('/v=([^&]*)/',$link,$matches);
                          if(!$matches[1]) {
                            preg_match('/youtu\.be\/(.+)/',$link,$matches2);
                            $viid = $matches2[1];
                          } else {
                            $viid = $matches[1];
                          }
                          if($viid) {?>
                            <iframe width="350" height="197" src="https://www.youtube.com/embed/<?php echo $viid; ?>" frameborder="0" allowfullscreen></iframe>
                            <?php } else {
                              preg_match('/vimeo\.com\/(.+)/',$link,$matches);
                              if($matches[1]) {
                                $viid = $matches[1]; ?>
                                <iframe src="//player.vimeo.com/video/<?php echo $viid; ?>?byline=0&amp;portrait=0&amp;badge=0&amp;color=fcf7f7" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                <?php }
                              }
                            }?>
                      <?php else: ?>
                        <img src="<?php echo base_url(IMAGENS_FOLDER.$video->link) ?>" alt="" width="200" />
                      <?php endif; ?>
                    </td>
                    <td>
                      <a href="<?php echo base_url('admin/videos/editar/'.$video->id)?>" title="Editar" class="btn btn-default btn"><i class="glyphicon glyphicon-edit"></i></a>
                      <a href="<?php echo base_url('admin/videos/remover/'.$video->id)?>" title="Remover" class="btn btn-default btn"><i class="glyphicon glyphicon-remove"></i></a>
                    </td>
                 </tr>
                 <?php } ?>
              </tbody>
           </table>
          </div><!-- table-responsive -->

        </div><!-- panel-body -->
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";

    jQuery('#listagem').dataTable({
      "pagingType": "simple_numbers",
	  "stateSave": true,
	  "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
    });

    // Select2
    jQuery('select').select2({
    });

    jQuery('select').removeClass('form-control');

    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });

        return false;
    });

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });


  });
</script>

</body>
</html>
