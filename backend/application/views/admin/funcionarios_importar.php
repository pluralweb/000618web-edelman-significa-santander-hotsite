    <div class="pageheader">
      <h2><i class="fa fa-sort"></i> Cadastrar Funcionários</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
          <li>Funcionários</li>
          <li class="active">Importar Funcionários</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Funcionários</h3>
          <p>Aqui você pode atualizar a lista de funcionários enviando um arquivo xls ou xlsx. Atenção: os funcionários que não constem nessa nova importação serão automaticamente desativados do sistema.</p>
        </div>
        
        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/funcionarios/import_data') ?>" method="post" enctype="multipart/form-data">
            <div class="panel-body panel-body-nopadding">
          
            
            <div class="form-group">
              <div class="col-sm-12">
                <label class="control-label">Arquivo</label>
                <input type="file" name="arquivo" id="arquivo" class="form-control" required />
              </div>
            </div>
          </div><!-- panel-body -->
        
          <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Importar</button>
                  </div>
               </div>
            </div>
          
        </form>
      </div>
      
    </div><!-- contentpanel -->
    
  </div><!-- mainpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {
    
    "use strict";
    
	});
	
	
  });
	
</script>

</body>
</html>
