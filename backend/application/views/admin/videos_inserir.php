    <div class="pageheader">
      <h2><i class="fa fa-film"></i> Inserir Vídeo</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
          <li>Vídeos</li>
          <li class="active">Inserir Vídeo</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Vídeos</h3>
          <p>Aqui você cadastra um novo vídeo para o site.</p>
        </div>

        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/videos/addVideo') ?>" method="post" enctype="multipart/form-data">
            <div class="panel-body panel-body-nopadding">


              <div class="form-group">
                <div class="col-sm-12">
                  <label class="control-label">Midia</label>
                  <div class="col-sm-12">
                    <input type="checkbox" name="midia" class="midia" value="1" checked>Video?
                  </div>
                </div>
              </div>

              <div class="form-group link">
                <div class="col-sm-12">
                  <label class="control-label">Link</label>
                  <input type="text" name="link" id="link" class="form-control" />
                </div>

              </div>

              <div class="form-group linkImg" style="display:none">
                <div class="col-sm-12">
                  <label class="control-label">Link</label>
                  <input type="file" name="linkImg" id="linkImg" class="form-control" />
                </div>

              </div>
          </div><!-- panel-body -->

          <div class="panel-footer">
               <div class="row">
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                  </div>
               </div>
            </div>

        </form>
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";
    $('.midia').on('change', function(event) {
      event.preventDefault();
      if(!$(this).is(':checked')){
        $('.link').hide()
        $('.linkImg').show()
      }else{
        $('.link').show()
        $('.linkImg').hide()

      }
      });

  });

</script>

</body>
</html>
