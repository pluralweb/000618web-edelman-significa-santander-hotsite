    <div class="pageheader">
      <h2><i class="fa fa-film"></i> Inserir Funcionário</h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Você está aqui:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('admin/') ?>">Santander - Desafio 2016</a></li>
          <li>Funcionário</li>
          <li class="active">Inserir Funcionário</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Funcionário</h3>
          <p>Aqui você cadastra um novo funcionário para o site.</p>
        </div>

        <form class="form-horizontal form-bordered" action="<?php echo base_url('admin/funcionarios/addfuncionario') ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id_rede_comercial" value="1" class="form-control" />
          <div class="panel-body panel-body-nopadding">
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label">Nome</label>
                <input type="text" name="nome" id="nome" class="form-control" />
              </div>
              <div class="col-sm-6">
                <label class="control-label">CPF</label>
                <input type="text" name="cpf" id="cpf" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label">Matricula</label>
                <input type="text" name="matricula" id="matricula" class="form-control" />
              </div>
              <div class="col-sm-6">
                <label class="control-label">E-mail</label>
                <input type="text" name="email" id="email" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6">
                <label class="control-label">Regional</label>
                <select class="form-control" name="id_regional">						
									<?php foreach ($regional as $r) { ?>
										<option value="<?php echo $r->id; ?>"><?php echo $r->nome; ?></option>
									<?php } ?>
								</select>				
              </div>   
              <div class="col-sm-6">
                <label class="control-label">Campanha</label>
                <select class="form-control" name="campanha">                                  
                  <option value="CDC">CDC</option>
                  <option value="VEICULOS">VEICULOS</option>
                </select>       
              </div>         
            </div>
          </div><!-- panel-body -->
          <div class="panel-footer">
             <div class="row">
                <div class="col-sm-6">
                  <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
             </div>
          </div>
        </form>
      </div>

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.maskMoney.js') ?>"></script>

<script src="<?php echo base_url('assets/js/raphael-2.1.0.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>

<script>
  jQuery(document).ready(function() {

    "use strict";
    $('.midia').on('change', function(event) {
      event.preventDefault();
      if(!$(this).is(':checked')){
        $('.link').hide()
        $('.linkImg').show()
      }else{
        $('.link').show()
        $('.linkImg').hide()

      }
      });

  });

</script>

</body>
</html>
