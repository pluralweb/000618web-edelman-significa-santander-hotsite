<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png') ?>" type="image/png">

  <title>BRF - Banco de Currículos</title>

  <link href="<?php echo base_url('assets/css/style.default.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/font.roboto.css'); ?>" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url('assets/js/html5shiv.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/respond.min.js') ?>"></script>
  <![endif]-->
</head>

<body class="notfound">


<section>
  
  <div class="notfoundpanel">
    <h1>404!</h1>
    <h3>Página não encontrada!</h3>
  </div><!-- notfoundpanel -->
  
</section>


<script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.sparkline.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cookies.js') ?>"></script>

<script src="<?php echo base_url('assets/js/toggles.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/retina.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
</body>
</html>
