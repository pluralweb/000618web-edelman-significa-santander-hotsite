<body class="firstAccess">
    <div class="container">
        <div class="centerVertically">
            <div class="row">
              <div class="brand col-xs-12 col-md-6 col-sm-6" data-kui-anim="tada">
                <img class="clock" src="<?php echo base_url('imgs/clock.gif'); ?>" alt="">
                <img class="clock-lines" data-kui-anim="bounceIn" src="<?php echo base_url("imgs/clock-lines.png"); ?>" alt="">
                <img src="<?php echo base_url("imgs/iconCampanha.png"); ?>" alt="Agora é a hora!">
              </div>
                <div class="form showForm col-xs-10 col-sm-6 col-md-6" data-kui-anim="zoomIn" data-form="login">
                    <h5>Acesse agora</h5>
                    <form method="post" class="login">
                        <label for="matricula">Matrícula:</label>
                        <input type="text" name="matricula" class="matricula" maxlength="20" />
                        <label for="cpf">Senha:</label>
                        <input type="password" name="senha" />
                        <div class="row">
                        <div class="link">
                            <a href="javascript:void(0);" class="firstAccessLink">
                                primeiro acesso |
                            </a>
                        </div>
                        <div class="link">
                            <a href="javascript:void(0);" class="forgetPass">
                                esqueci minha senha
                            </a>
                        </div>
                    </div>
                        <button type="submit" value="submit">Próximo</button>
                    </form>
                </div>
                <div class="form brand col-xs-12 col-sm-6 col-md-6" data-form="forget">
                  <div class="closeForm">X</div>
                    <h5>Esqueci minha senha</h5>
                    <form method="post" accept-charset="ISO-8859-1" class="esqueci" style="padding-top: 60px;">
                        <label for="matricula">Matrícula:</label>
                        <input type="text" name="matricula" class="matricula" maxlength="20" />
                        <button type="submit" value="submit" style="margin-top: 65px !important;">Próximo</button>
                    </form>
                </div>
                <div class="form col-xs-12 col-sm-6 col-md-6" data-form="firstAccess">
                    <div class="closeForm">X</div>
                    <h5>Primeiro acesso</h5>
                    <div class="hideDiv no img">
                      <form id="zoneUpload" method="post" enctype="multipart/form-data">
                        <div class="img">
                          <div class="bg"></div>
                          <label>Upload de avatar:</label>
                          <input type="file" name="img" />
                        </div>
                        <div class="img">
                          <label>Ou escolha seu avatar:</label>
                          <label class="radioCustom">
                            <input type="radio" value="0" name="avatar" checked />
                            <img src="<?php echo base_url('imgs/woman.png') ?>" alt="Mulher" />
                    			</label>
                          <label class="radioCustom">
                    				<input type="radio" name="avatar" value="1">
                    				<img src="<?php echo base_url('imgs/man.png') ?>" alt="Homem" />
                    			</label>
                        </div>
                        <button type="submit" class="avatarButton">Próximo</button>
                      </form>
                    </div>
                    <form class="primeiroAcesso active" method="post">
                      <input type="hidden" id="server_response" name="imgResponse" value="NULL">
                      <div class="firstStep active">
                        <label for="matricula">Matrícula*:</label>
                        <input type="text" name="matricula" class="matricula" maxlength="20" />
                        <label for="cpf">CPF*:</label>
                        <input type="text" name="cpf" class="cpf" maxlength="14" />
                        <button type="button" class="firstButton">Próximo</button>
                      </div>
                      <div class="secondStep">
                        <label>Senha*:</label>
                        <input type="password" name="senha" maxlength="14" />
                        <label>Repita a Senha*:</label>
                        <input type="password" name="resenha" maxlength="14" />
                        <button type="button" class="secondButton">Próximo</button>
                      </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="logo col-xs-12 col-sm-12" data-kui-anim="fadeInUp">
                    <a href="https://www.santander.com.br/br/" target="_blank">
                        <img src="<?php echo base_url('imgs/iconSantander.png'); ?>">
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
  var url = "<?php echo base_url('home/cadastraImg') ?>";
</script>
<script src="<?php echo base_url("js/script.min.js"); ?>" charset="utf-8"></script>

</html>
