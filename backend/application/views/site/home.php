<?php
setlocale(LC_MONETARY, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');
/**
 * Formata um numero em notacao de moeda, assim como a funcao money_format do PHP
 * @author Rubens Takiguti Ribeiro
 * @see http://php.net/manual/en/function.money-format.php
 * @param string $formato Formato aceito por money_format
 * @param float $valor Valor monetario
 * @return string Valor formatado
 */
function my_money_format($formato, $valor) {

    // Se a funcao money_format esta disponivel: usa-la
    if (function_exists('money_format')) {
        return money_format($formato, $valor);
    }

    // Se nenhuma localidade foi definida, formatar com number_format
    if (setlocale(LC_MONETARY, 0) == 'C') {
        return number_format($valor, 2);
    }

    // Obter dados da localidade
    $locale = localeconv();

    // Extraindo opcoes do formato
    $regex = '/^'.             // Inicio da Expressao
             '%'.              // Caractere %
             '(?:'.            // Inicio das Flags opcionais
             '\=([\w\040])'.   // Flag =f
             '|'.
             '([\^])'.         // Flag ^
             '|'.
             '(\+|\()'.        // Flag + ou (
             '|'.
             '(!)'.            // Flag !
             '|'.
             '(-)'.            // Flag -
             ')*'.             // Fim das flags opcionais
             '(?:([\d]+)?)'.   // W  Largura de campos
             '(?:#([\d]+))?'.  // #n Precisao esquerda
             '(?:\.([\d]+))?'. // .p Precisao direita
             '([in%])'.        // Caractere de conversao
             '$/';             // Fim da Expressao

    if (!preg_match($regex, $formato, $matches)) {
        trigger_error('Formato invalido: '.$formato, E_USER_WARNING);
        return $valor;
    }

    // Recolhendo opcoes do formato
    $opcoes = array(
        'preenchimento'   => ($matches[1] !== '') ? $matches[1] : ' ',
        'nao_agrupar'     => ($matches[2] == '^'),
        'usar_sinal'      => ($matches[3] == '+'),
        'usar_parenteses' => ($matches[3] == '('),
        'ignorar_simbolo' => ($matches[4] == '!'),
        'alinhamento_esq' => ($matches[5] == '-'),
        'largura_campo'   => ($matches[6] !== '') ? (int)$matches[6] : 0,
        'precisao_esq'    => ($matches[7] !== '') ? (int)$matches[7] : false,
        'precisao_dir'    => ($matches[8] !== '') ? (int)$matches[8] : $locale['int_frac_digits'],
        'conversao'       => $matches[9]
    );

    // Sobrescrever $locale
    if ($opcoes['usar_sinal'] && $locale['n_sign_posn'] == 0) {
        $locale['n_sign_posn'] = 1;
    } elseif ($opcoes['usar_parenteses']) {
        $locale['n_sign_posn'] = 0;
    }
    if ($opcoes['precisao_dir']) {
        $locale['frac_digits'] = $opcoes['precisao_dir'];
    }
    if ($opcoes['nao_agrupar']) {
        $locale['mon_thousands_sep'] = '';
    }

    // Processar formatacao
    $tipo_sinal = $valor >= 0 ? 'p' : 'n';
    if ($opcoes['ignorar_simbolo']) {
        $simbolo = '';
    } else {
        $simbolo = $opcoes['conversao'] == 'n' ? $locale['currency_symbol']
                                               : $locale['int_curr_symbol'];
    }
    $numero = number_format(abs($valor), $locale['frac_digits'], $locale['mon_decimal_point'], $locale['mon_thousands_sep']);

    $sinal = $valor >= 0 ? $locale['positive_sign'] : $locale['negative_sign'];
    $simbolo_antes = $locale[$tipo_sinal.'_cs_precedes'];

    // Espaco entre o simbolo e o numero
    $espaco1 = $locale[$tipo_sinal.'_sep_by_space'] == 1 ? ' ' : '';

    // Espaco entre o simbolo e o sinal
    $espaco2 = $locale[$tipo_sinal.'_sep_by_space'] == 2 ? ' ' : '';

    $formatado = '';
    switch ($locale[$tipo_sinal.'_sign_posn']) {
    case 0:
        if ($simbolo_antes) {
            $formatado = '('.$simbolo.$espaco1.$numero.')';
        } else {
            $formatado = '('.$numero.$espaco1.$simbolo.')';
        }
        break;
    case 1:
        if ($simbolo_antes) {
            $formatado = $sinal.$espaco2.$simbolo.$espaco1.$numero;
        } else {
            $formatado = $sinal.$numero.$espaco1.$simbolo;
        }
        break;
    case 2:
        if ($simbolo_antes) {
            $formatado = $simbolo.$espaco1.$numero.$sinal;
        } else {
            $formatado = $numero.$espaco1.$simbolo.$espaco2.$sinal;
        }
        break;
    case 3:
        if ($simbolo_antes) {
            $formatado = $sinal.$espaco2.$simbolo.$espaco1.$numero;
        } else {
            $formatado = $numero.$espaco1.$sinal.$espaco2.$simbolo;
        }
        break;
    case 4:
        if ($simbolo_antes) {
            $formatado = $simbolo.$espaco2.$sinal.$espaco1.$numero;
        } else {
            $formatado = $numero.$espaco1.$simbolo.$espaco2.$sinal;
        }
        break;
    }

    // Se a string nao tem o tamanho minimo
    if ($opcoes['largura_campo'] > 0 && strlen($formatado) < $opcoes['largura_campo']) {
        $alinhamento = $opcoes['alinhamento_esq'] ? STR_PAD_RIGHT : STR_PAD_LEFT;
        $formatado = str_pad($formatado, $opcoes['largura_campo'], $opcoes['preenchimento'], $alinhamento);
    }

    return $formatado;
}
 ?>
<body class="homeLogada" style="overflow-x: hidden; position: relative">
    <div class="container">
        <div class="intro">
            <div class="centralize">
                <div class="groupLinks" data-kui-anim="fadeInDown">                                                        
                <form id="form_avatar" style="float: left;" action="<?php echo base_url('home/edit_funcionario_img') ?>" method="post" enctype="multipart/form-data">
                    <label style="cursor: pointer;">                      
                        <p class="alignText">
                          <?php if (!empty($user->img)): ?>
                            <?php if ($user->img == 'woman' || $user->img == 'man'): ?>
                              <span class="user" style="background-image: url(<?php echo base_url('imgs/'.$user->img.'.png'); ?>);"></span>
                            <?php else: ?>
                              <span class="user" style="background-image: url(<?php echo base_url(IMAGENS_FOLDER.$user->img); ?>);"></span>
                            <?php endif; ?>
                          <?php else: ?>
                            <span class="user" style="background-image: url(<?php echo base_url('imgs/user.png'); ?>);"></span>
                          <?php endif; ?>
                        </p>
                        <input type="file" name="img" style="display:none;">                                         
                    </label>                  
                  </form> 
                  <p>                      
                      <?php echo $nomeUser; ?>
                      <a href="<?php echo base_url('home/logout') ?>" class="sair">Sair</a>
                  </p>
                  
                    <a href="javascript:void(0);" class="reg-open">Regulamento</a>
                    <a href="javascript:void(0);" class="duv-open">FAQ</a>
                </div>

                <div class="brand" data-kui-anim="tada">
									<img class="clock" src="<?php echo base_url('imgs/clock.gif'); ?>" alt="">
                  <img class="clock-lines" data-kui-anim="bounceIn" src="<?php echo base_url("imgs/clock-lines.png"); ?>" alt="">
                  <?php if ($cdc): ?>
                    <img src="<?php echo base_url("imgs/iconCampanha1.png"); ?>" alt="Agora é a hora!">
                  <?php else: ?>
                    <img src="<?php echo base_url("imgs/iconCampanha1-1.png"); ?>" alt="Agora é a hora!">
                  <?php endif; ?>
                </div>

                <div class="video" data-kui-anim="fadeInUp">
                    <?php
                      preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $linkPromo, $matches);
                    ?>
                    <?php if (count($matches) == 1): ?>
                      <iframe width="382" height="218" src="https://www.youtube.com/embed/<?php echo $matches[0]; ?>?autoplay=<?php echo $autoplay; ?>" frameborder="0" allowfullscreen></iframe>
                    <?php else: ?>
                      <video src="<?php echo base_url(VIDEOS_FOLDER.$linkPromo); ?>" preload="metadata" style="width: 382px; height: 218px;" controls <?php echo ($autoplay == 1) ? 'autoplay': ''; ?>>
                    <?php endif; ?>

                   </video>
                </div>
                <div class="text" data-kui-anim="fadeInUp">
                  <?php if ($cdc): ?>
                    <p>
                      É hora de mostrar nossa força comercial.<br/><br/>
                      Contribua com os resultados da Santander Financiamentos em CDC e concorra a uma viagem para Fernando de Noronha ou a vouchers de viagens para você conhecer lugares incríveis a sua escolha.<br/><br/>
                      Agora é com você: Confira suas metas e o regulamento que está cheio de novidades, acompanhe seu desempenho, envie suas dicas comerciais e bons negócios!
                    </p>
                  <?php else: ?>
                    <p>
                      É hora de mostrar nossa força comercial. <br/><br/>
                      Contribua com os resultados da Santander Financiamentos em Veículos e concorra a uma viagem para Fernando de Noronha ou a vouchers de viagens para você conhecer lugares incríveis a sua escolha.<br/><br/>
                      Agora é com você: Confira suas metas e o regulamento que está cheio de novidades, acompanhe seu desempenho, envie suas dicas comerciais e bons negócios!
                    </p>
                  <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="galeria" data-kui-anim="bounceInLeft">
            <div class="content">
                <h2>Galeria</h2>
                <p>Envie suas fotos e vídeos com dicas comerciais para <a href="mailto:santander.financiamentos.campanhas@santander.com.br">santander.financiamentos.campanhas@santander.com.br</a> eles poderão aparecer aqui na nossa Galeria.</p>
                <div class="slider">
                  <?php if ($midia): ?>
                    <div id="owl-carousel" class="grid owl-carousel">
                      <?php $i = 1; ?>
                      <?php $max = count($midia); ?>
                        <?php foreach ($midia as $m): ?>
                            <?php if ($i == 1): ?>
                              <div class="row">
                            <?php endif; ?>
                            <?php if ($m->video == 1): ?>
                              <?php
                                preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $m->link, $matches);
                              ?>
                              <div class="item">
                                <a href="javascript:void(0)" data-toggle="lightboxVideo" data-target="#youtube" data-youtube="<?php echo $m->link; ?>">
                                  <span class="video"></span>
                                  <span class="bg" style="background-image: url(http://img.youtube.com/vi/<?php echo $matches[0]; ?>/0.jpg);"></span>
                                </a>
                              </div>
                            <?php else: ?>
                              <div class="item">
                                <a href="javascript:void(0);" class="modalButton" data-img="<?php echo base_url(IMAGENS_FOLDER.$m->link) ?>">
                                  <span class="bg" style="background-image: url(<?php echo base_url(IMAGENS_FOLDER.$m->link) ?>);"></span>
                                </a>
                              </div>
                            <?php endif; ?>

                            <?php if ($i%2==0 && $max > ($i)): ?>
                              <?php echo '</div><div class="row">'; ?>
                            <?php endif; ?>
                            <?php $i++; ?>


                          <?php endforeach; ?>
                        </div>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="navigation">
                    <a href="javascript:void(0)" class="prev"><img src="<?php echo base_url('imgs/slide-left.png'); ?>" class="nav"></a>
                    <a href="javascript:void(0)" class="next"><img src="<?php echo base_url('imgs/slide-right.png'); ?>" class="nav"></a>
                </div>
            </div>
        </div>

        <div class="performances" data-kui-anim="bounceInRight">
            <div class="content">
                <div class="colaborador">
                  <?php if (!empty($user->img)): ?>
                    <?php if ($user->img == 'woman' || $user->img == 'man'): ?>
                      <span class="user" style="background-image: url(<?php echo base_url('imgs/'.$user->img.'.png'); ?>);"></span>
                    <?php else: ?>
                      <span class="user" style="background-image: url(<?php echo base_url(IMAGENS_FOLDER.$user->img); ?>);"></span>
                    <?php endif; ?>
                  <?php else: ?>
                    <span class="user" style="background-image: url(<?php echo base_url('imgs/user.png'); ?>);"></span>
                  <?php endif; ?>
                      <p><span><?php echo $nomeUser; ?></span><br/>Confira suas metas do mês de <?php echo $mesExtenso; ?>.</p>
                </div>
                <div class="meta">
                      <div class="circle">
                        <?php $r = (isset($metas) ? $metas->meta_prod : '0.00'); ?>
                        <span>Produção</span> <p><?php echo my_money_format ('%n', $r);?></p>
                      </div>
                      <img src="<?php echo base_url('imgs/arrow-white.png'); ?>">
                      <div class="circle">
                        <?php $r = (isset($metas) ? $metas->meta_tab : '0'); ?>
                        <span>TAB Pagante</span> <p><?php echo $r;?></p>
                      </div>
                      <img src="<?php echo base_url('imgs/arrow-white.png'); ?>">
                      <div class="circle">
                        <?php $r = (isset($metas) ? $metas->meta_cob : '0'); ?>
                        <span>Cobrança</span> <p><?php echo $r;?> </p>
                      </div>
                </div>
            </div>
        </div>
        <div class="ranking" data-kui-anim="zoomIn">
            <div class="centralize">
                <h2>Ranking</h2>
                <div class="text">
                    <p>Veja sua posição no Ranking mensal e acumulado, trace sua estratégia para atingir seus objetivos.</p>
                </div>
                <div class="tabela">
                    <?php if($cdc) :?>
                      <table>
                        <tr class="gray first">
                            <td></td>
                            <td colspan="2">Setembro</td>
                            <td colspan="2">Outubro</td>
                            <td colspan="2">Novembro</td>
                            <td colspan="2">Dezembro</td>
                            <td colspan="2">Acumulado</td>
                        </tr>
                        <tr class="gray">
                            <td></td>
                            <td>Ranking</td>
                            <td>Pontuação</td>
                            <td>Ranking</td>
                            <td>Pontuação</td>
                            <td>Ranking</td>
                            <td>Pontuação</td>
                            <td>Ranking</td>
                            <td>Pontuação</td>
                            <td>Ranking</td>
                            <td>Pontuação</td>
                        </tr>
                        <tr class="gold">
                            <td>Primeiro colocado</td>
                            <td><?php if(isset($ranking9[0])) {echo '1º';}else{echo '-';};?></td>
                            <td><?php if(isset($ranking9[0])) echo $ranking9[0]->pontos_primeiro;?></td>
                            <td><?php if(isset($ranking10[0])) {echo '1º';}else{echo '-';};?></td>
                            <td><?php if(isset($ranking10[0])) echo $ranking10[0]->pontos_primeiro;?></td>
                            <td><?php if(isset($ranking11[0])) {echo '1º';}else{echo '-';};?></td>
                            <td><?php if(isset($ranking11[0])) echo $ranking11[0]->pontos_primeiro;?></td>
                            <td><?php if(isset($ranking12[0])) {echo '1º';}else{echo '-';};?></td>
                            <td><?php if(isset($ranking12[0])) echo $ranking12[0]->pontos_primeiro;?></td>
                            <td><?php if(isset($acumulado)) {echo '1º';}else{echo '-';};?></td>
                            <td><?php if(isset($acumulado)) echo $acumulado->pontos_primeiro;?></td>
                        </tr>
                        <tr class="gold">
                            <td>Colocado Anterior</td>
                            <td><?php if(isset($ranking9[0])) {echo ($ranking9[0]->colocacao -1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($ranking9[0])) echo $ranking9[0]->pontos_anterior;?></td>
                            <td><?php if(isset($ranking10[0])) {echo ($ranking10[0]->colocacao -1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($ranking10[0])) echo $ranking10[0]->pontos_anterior;?></td>
                            <td><?php if(isset($ranking11[0])) {echo ($ranking11[0]->colocacao -1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($ranking11[0])) echo $ranking11[0]->pontos_anterior;?></td>
                            <td><?php if(isset($ranking12[0])) {echo ($ranking12[0]->colocacao -1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($ranking12[0])) echo $ranking12[0]->pontos_anterior;?></td>
                            <td><?php if(isset($acumulado)) {echo ($acumulado->colocacao -1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($acumulado)) echo $acumulado->pontos_anterior;?></td>
                        </tr>
                        <tr class="gold middle">
                            <td>Você</td>
                            <td><?php if(isset($ranking9[0])) {echo $ranking9[0]->colocacao."º";}else{echo '-';}?></td>
                            <td><?php if(isset($ranking9[0])) echo $ranking9[0]->pontuacao;?></td>
                            <td><?php if(isset($ranking10[0])) {echo $ranking10[0]->colocacao."º";}else{echo '-';}?></td>
                            <td><?php if(isset($ranking10[0])) echo $ranking10[0]->pontuacao;?></td>
                            <td><?php if(isset($ranking11[0])) {echo $ranking11[0]->colocacao."º";}else{echo '-';}?></td>
                            <td><?php if(isset($ranking11[0])) echo $ranking11[0]->pontuacao;?></td>
                            <td><?php if(isset($ranking12[0])) {echo $ranking12[0]->colocacao."º";}else{echo '-';}?></td>
                            <td><?php if(isset($ranking12[0])) echo $ranking12[0]->pontuacao;?></td>
                            <td><?php if(isset($acumulado)) {echo $acumulado->colocacao."º";}else{echo '-';}?></td>
                            <td><?php if(isset($acumulado)) echo $acumulado->pontuacao;?></td>
                        </tr>
                        <tr class="gold">
                            <td>Colocado Posterior</b></td>
                            <td><?php if(isset($ranking9[0])) {echo ($ranking9[0]->colocacao +1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($ranking9[0])) echo $ranking9[0]->pontos_depois;?></td>
                            <td><?php if(isset($ranking10[0])) {echo ($ranking10[0]->colocacao +1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($ranking10[0])) echo $ranking10[0]->pontos_depois;?></td>
                            <td><?php if(isset($ranking11[0])) {echo ($ranking11[0]->colocacao +1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($ranking11[0])) echo $ranking11[0]->pontos_depois;?></td>
                            <td><?php if(isset($ranking12[0])) {echo ($ranking12[0]->colocacao +1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($ranking12[0])) echo $ranking12[0]->pontos_depois;?></td>
                            <td><?php if(isset($acumulado)) {echo ($acumulado->colocacao +1).'º';}else{echo '-';}?></td>
                            <td><?php if(isset($acumulado)) echo $acumulado->pontos_depois;?></td>
                        </tr>
                    </table>
                    <?php else : ?>
                        <table>
                            <tr class="gray first">
                                <td></td>
                                <td colspan="2">Novembro</td>
                                <td colspan="2">Dezembro</td>
                                <td colspan="2">Janeiro</td>
                                <td colspan="2">Fevereiro</td>
                                <td colspan="2">Acumulado</td>
                            </tr>
                            <tr class="gray">
                                <td></td>
                                <td>Ranking</td>
                                <td>Pontuação</td>
                                <td>Ranking</td>
                                <td>Pontuação</td>
                                <td>Ranking</td>
                                <td>Pontuação</td>
                                <td>Ranking</td>
                                <td>Pontuação</td>
                                <td>Ranking</td>
                                <td>Pontuação</td>
                            </tr>
                            <tr class="gold">
                                <td>Primeiro colocado</td>
                                <td><?php if(isset($ranking11[0])) {echo '1º';}else{echo '-';};?></td>
                                <td><?php if(isset($ranking11[0])) echo $ranking11[0]->pontos_primeiro;?></td>
                                <td><?php if(isset($ranking12[0])) {echo '1º';}else{echo '-';};?></td>
                                <td><?php if(isset($ranking12[0])) echo $ranking12[0]->pontos_primeiro;?></td>
                                <td><?php if(isset($ranking1[0])) {echo '1º';}else{echo '-';};?></td>
                                <td><?php if(isset($ranking1[0])) echo $ranking1[0]->pontos_primeiro;?></td>
                                <td><?php if(isset($ranking2[0])) {echo '1º';}else{echo '-';};?></td>
                                <td><?php if(isset($ranking2[0])) echo $ranking2[0]->pontos_primeiro;?></td>
                                <td><?php if(isset($acumulado)) {echo '1º';}else{echo '-';};?></td>
                                <td><?php if(isset($acumulado)) echo $acumulado->pontos_primeiro;?></td>
                            </tr>
                            <tr class="gold">
                                <td>Colocado Anterior</td>
                                <td><?php if(isset($ranking11[0])) {echo ($ranking11[0]->colocacao -1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($ranking11[0])) echo $ranking11[0]->pontos_anterior;?></td>
                                <td><?php if(isset($ranking12[0])) {echo ($ranking12[0]->colocacao -1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($ranking12[0])) echo $ranking12[0]->pontos_anterior;?></td>
                                <td><?php if(isset($ranking1[0])) {echo ($ranking1[0]->colocacao -1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($ranking1[0])) echo $ranking1[0]->pontos_anterior;?></td>
                                <td><?php if(isset($ranking2[0])) {echo ($ranking2[0]->colocacao -1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($ranking2[0])) echo $ranking2[0]->pontos_anterior;?></td>
                                <td><?php if(isset($acumulado)) {echo ($acumulado->colocacao -1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($acumulado)) echo $acumulado->pontos_anterior;?></td>
                            </tr>
                            <tr class="gold middle">
                                <td>Você</td>
                                <td><?php if(isset($ranking11[0])) {echo $ranking11[0]->colocacao."º";}else{echo '-';}?></td>
                                <td><?php if(isset($ranking11[0])) echo $ranking11[0]->pontuacao;?></td>
                                <td><?php if(isset($ranking12[0])) {echo $ranking12[0]->colocacao."º";}else{echo '-';}?></td>
                                <td><?php if(isset($ranking12[0])) echo $ranking12[0]->pontuacao;?></td>
                                <td><?php if(isset($ranking1[0])) {echo $ranking1[0]->colocacao."º";}else{echo '-';}?></td>
                                <td><?php if(isset($ranking1[0])) echo $ranking1[0]->pontuacao;?></td>
                                <td><?php if(isset($ranking2[0])) {echo $ranking2[0]->colocacao."º";}else{echo '-';}?></td>
                                <td><?php if(isset($ranking2[0])) echo $ranking2[0]->pontuacao;?></td>
                                <td><?php if(isset($acumulado)) {echo $acumulado->colocacao."º";}else{echo '-';}?></td>
                                <td><?php if(isset($acumulado)) echo $acumulado->pontuacao;?></td>
                            </tr>
                            <tr class="gold">
                                <td>Colocado Posterior</b></td>
                                <td><?php if(isset($ranking11[0])) {echo ($ranking11[0]->colocacao +1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($ranking11[0])) echo $ranking11[0]->pontos_depois;?></td>
                                <td><?php if(isset($ranking12[0])) {echo ($ranking12[0]->colocacao +1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($ranking12[0])) echo $ranking12[0]->pontos_depois;?></td>
                                <td><?php if(isset($ranking1[0])) {echo ($ranking1[0]->colocacao +1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($ranking1[0])) echo $ranking1[0]->pontos_depois;?></td>
                                <td><?php if(isset($ranking2[0])) {echo ($ranking2[0]->colocacao +1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($ranking2[0])) echo $ranking2[0]->pontos_depois;?></td>
                                <td><?php if(isset($acumulado)) {echo ($acumulado->colocacao +1).'º';}else{echo '-';}?></td>
                                <td><?php if(isset($acumulado)) echo $acumulado->pontos_depois;?></td>
                            </tr>
                        </table>
                    <?php endif;?>
                </div>
            </div>
        </div>
