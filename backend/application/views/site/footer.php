<div class="rodape" data-kui-anim="fadeInUp">
		<img src="<?php echo base_url('imgs/iconSantander.png'); ?>" alt="Santander">
</div>
<div class="modal">
		<div class="align">
			<?php if (isset($faq)): ?>
				<div class="faq bodyModal">
						<a class="close" >X</a>
						<div class="box">
								<h2>FAQ</h2>
								<div class="insert">
									<?php if ($duvida == 0): ?>
										<p><a href="mailto:<?php echo $email; ?>">Enviar dúvida...</a></p>
									<?php endif; ?>
									<ul>
										<?php foreach ($faq as $f): ?>
											<li>
												<h3 class="question">
													<?php echo $f->pergunta; ?>
												</h3>
												<div class="answer">
													<?php echo $f->resposta; ?>
												</div>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
						</div>
					</div>
			<?php endif; ?>
			<?php if (isset($regulamento)): ?>
				<div class="regulamento bodyModal">
						<a class="close" >X</a>
						<div class="box">
								<h2>Regulamento</h2>
								<div class="insert">
									<?php echo $regulamento; ?>
								</div>
						</div>
				</div>
			<?php endif; ?>

				<div class="modal-gallery thumb bodyModal">
						<a class="close" >X</a>
						<div class="box">
								<img src="<?php echo base_url('imgs/thumb-1.png'); ?>">
						</div>
				</div>
		</div>
</div>
<div class="lightboxVideo" id="youtube" display="none">
		<iframe class="video" width="950" height="534" src="" frameborder="0" allowfullscreen></iframe>
</div>
</div>
</body>
<script type="text/javascript">
  var url = "<?php echo base_url('home/cadastraImg') ?>";
</script>

<script src="<?php echo base_url('js/script.min.js'); ?>" charset="utf-8"></script>
</html>
