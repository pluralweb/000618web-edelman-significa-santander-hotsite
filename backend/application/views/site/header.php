<!doctype html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <meta lang="pt-br" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Santander - Desafio 2016</title>
    <meta name="description" content="Descrição" />
    <!-- Código para Google Authorship e Publisher -->
    <link rel="author" href="https://plus.google.com/(Google+_Profile)/posts" />
    <link rel="publisher" href="https://plus.google.com/(Google+_Page_Profile)" />
    <!-- Código do Schema.org também para o Google+ -->
    <meta itemprop="name" content="Santander - Desafio 2016" />
    <meta itemprop="description" content="Descrição" />
    <!-- para o Twitter Card -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="Conta do Twitter do site (incluindo arroba)" />
    <meta name="twitter:title" content="Titulo twitter" />
    <meta name="twitter:description" content="Descrição" />
    <meta name="twitter:creator" content="Conta do Twitter do autor do texto (incluindo arroba)" />
    <!-- imagens largas para o Twitter Summary Card precisam ter pelo menos 280x150px -->
    <meta name="twitter:image" content="http://www.example.com/image.jpg" />
    <!-- para o sistema Open Graph -->
    <meta property="og:title" content="Titulo" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="" />
    <meta property="og:description" content="Descrição" />
    <meta property="og:site_name" content="Nome do site" />
    <meta property="article:published_time" content="2013-09-17T05:59:00+01:00" />
    <meta property="article:modified_time" content="2013-09-16T19:08:47+01:00" />
    <meta property="article:section" content="Seção do artigo" />
    <meta property="article:tag" content="Tags do artigo" />
    <meta property="fb:admins" content="Facebook numeric ID" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('imgs/icon/apple-icon-57x57.png'); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('imgs/icon/apple-icon-60x60.png'); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('imgs/icon/apple-icon-72x72.png'); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('imgs/icon/apple-icon-76x76.png'); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('imgs/icon/apple-icon-114x114.png'); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('imgs/icon/apple-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('imgs/icon/apple-icon-144x144.png'); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('imgs/icon/apple-icon-152x152.png'); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('imgs/icon/apple-icon-180x180.png'); ?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url('imgs/icon/android-icon-192x192.png'); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('imgs/icon/favicon-32x32.png'); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('imgs/icon/favicon-96x96.png'); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('imgs/icon/favicon-16x16.png'); ?>">
    <link rel="manifest" href="<?php echo base_url('imgs/icon/manifest.json'); ?>">
    <meta name="msapplication-TileColor" content="#eeb867">
    <meta name="msapplication-TileImage" content="<?php echo base_url('imgs/icon/ms-icon-144x144.png'); ?>">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#eeb867" />
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#eeb867" />
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#eeb867" />
    <link rel="stylesheet" media="all" href="<?php echo base_url("css/style.min.css"); ?>" />
</head>
