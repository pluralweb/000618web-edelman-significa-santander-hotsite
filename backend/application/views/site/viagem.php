<!-- content -->
<div id="content">
	<div class="block">
        
    	<div id="boxInfos">
            <h1>Informações da Viagem</h1>
            <?php /*?><h2>Agenda:</h2>
            <table class="tableDates">
                <tr>
                    <th class="tb1"></th>
                    <th>Dia 1</th>
                    <th>Dia 2</th>
                    <th>Dia 3</th>
                    <th>Dia 4</th>
                    <th>Dia 5</th>
                    <th>Dia 6</th>
                </tr>
                <tr>
                    <td class="tb1">Manhã</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                 
                </tr>
                <tr>
                    <td class="tb1">Tarde</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    
                </tr>
                <tr>
                    <td class="tb1">Noite</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>

                </tr>
            </table>
            <h2>Dicas:</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget lorem commodo, tristique purus in, sagittis sem. Morbi tristique elit at massa tempor auctor. In consectetur laoreet fringilla. Aenean rhoncus metus vel risus dictum varius. Sed ut suscipit lorem, quis vulputate diam. Ut cursus faucibus nisl, non porta enim bibendum at. Morbi fermentum consequat rutrum. Mauris sed purus ipsum. Donec vitae felis a quam maximus congue id a lorem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam nec ex nec sapien pellentesque volutpat et eget nisl.</p>
            <h2>Confira aqui seu e-ticket:</h2><?php */?>

            <span class="name"><?php echo $nomeUser; ?></span>

            <?php /*?><table class="tableInfos">
                <tr>
                    <th>Data </th>
                    <th>Cia </th>
                    <th>Voo </th>
                    <th>Origem</th>
                    <th>Destino</th>
                    <th>Saída</th>
                    <th>Chegada</th>
                    <th>LOC</th>
                    <th>E-ticket</th>
                </tr>
                <tr>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                </tr>
                <tr>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                    <td>teste</td>
                </tr>
            </table><?php */?>
		            
			<div class="sendForm">
                <p class="sended" <?php echo ($formSend)?'style="display:block"':'style="display:none"'; ?>>Dados enviados com sucesso</p>
            </div>
            
            <form id="formViagem" action="<?php echo base_url('home/addViagem'); ?>" method="post" enctype="multipart/form-data">
                <h2 class="heighth2">Preencha seus dados:</h2>
               <!--  <h4>Dados Pessoais</h4> -->
                <ul class="listform">
                    <li>
                        <label>
                            <span class="label">Nome Completo:</span>
                            <div class="input"><input type="text" class="w293 empty" name="nome" id="nome" value="<?php echo (isset($temCadastro->nome)) ? $temCadastro->nome : "" ;?>" /></div>
                        </label>
                    </li>
                    <li>
                        <span class="label">Possui passaporte?</span>
                        <label>
                            <input type="radio" name="passaporte" value="S"  class="mark"<?php if(isset($temCadastro->passaporte) && $temCadastro->passaporte=='S') {?> checked="checked"<?php } ?>/><span class="textradio">Sim</span>
                        </label>
                        <label>
                            <input  type="radio" name="passaporte" value="N" class="radio2 mark"<?php if(isset($temCadastro->passaporte) && $temCadastro->passaporte=='N') {?> checked="checked"<?php } ?> /><span>Não</span>
                        </label>
                    </li>
                    <li<?php if(isset($temCadastro->passaporte) && $temCadastro->passaporte=='N') {?>  style="display:none"<?php } ?> class="divNomePassaporte">
                        <label>
                            <span class="label">Nome Conforme Passaporte:</span>
                            <div class="input"><input type="text" class="w293" name="nome_passaporte" id="nome_passaporte" value="<?php echo (isset($temCadastro->nome_passaporte)) ? $temCadastro->nome_passaporte : "" ;?>" /></div>
                        </label>
                    </li>
                    <li<?php if(isset($temCadastro->passaporte) && $temCadastro->passaporte=='N') {?>  style="display:none"<?php } ?> class="divNrPassaporte">
                        <label>
                            <span class="label">Número do Passaporte:</span>
                            <div class="input" ><input type="text" name="nrPassaporte" id="nrPassaporte" class="w260" maxlength="8" value="<?php echo (isset($temCadastro->numero_passaporte)) ? $temCadastro->numero_passaporte : "" ;?>" /></div>
                        </label>
                    </li>
                    <li<?php if(isset($temCadastro->passaporte) && $temCadastro->passaporte=='N') {?>  style="display:none"<?php } ?> class="divVlPassaporte">
                        <label>
                            <span class="label">Validade do Passaporte:</span>
                            <div  class="input"><input type="text" class="date vlPassaporte" name="validadePas" id="validadePas" value="<?php echo (isset($temCadastro->validade_passaporte)) ? implode('/',array_reverse(explode('-',$temCadastro->validade_passaporte))) : "" ;?>" /></div>
                            <span class="blue">Para a Espanha, o passaporte deverá ter uma validade mínima de 3 meses e 10 dias à partir do dia do embarque</span>
                        </label>
                    </li>
					<li<?php if(isset($temCadastro->passaporte) && $temCadastro->passaporte=='N') {?>  style="display:none"<?php } ?> class="divFtPassaporte">
                        <label>
                            <span class="label">Foto do passaporte:</span>
                            <div  class="input"><input type="file" name="fotoPass" accept="image/*" class="ftPassaporte"/></div>
                        </label>
                    </li>
					<li>
                        <label>
                            <span class="label">CPF:</span>
                            <div class="input"><input type="text" name="cpf" id="cpf" class="w200 cpf empty" value="<?php echo (isset($temCadastro->cpf)) ? $temCadastro->cpf : "" ;?>" /></div>
                        </label>
                    </li>                   
				    <li>
                        <label>
                            <span class="label">Data de nascimento:</span>
                            <div class="input"><input type="text" name="dataNasc" id="dataNasc" class="date empty" value="<?php echo (isset($temCadastro->data_nascimento)) ? implode('/',array_reverse(explode('-',$temCadastro->data_nascimento))) : "" ;?>" /></div>
                        </label>
                    </li>
                    <li>
                        <span class="label">Sexo:</span>
                        <label>
                            <input type="radio" name="genero" value="M" class="mark"<?php if(isset($temCadastro->sexo) && $temCadastro->sexo=='M') {?> checked="checked"<?php } ?> /><span class="textradio">Masculino</span>
                        </label>
                        <label>
                            <input class="radio2 mark" name="genero" value="F" type="radio"<?php if(isset($temCadastro->sexo) && $temCadastro->sexo=='F') {?> checked="checked"<?php } ?> /><span>Feminino</span>
                        </label>
                    </li>
					<li>
                        <label>
                            <span class="label">E-mail comercial:</span>
                            <div class="input"><input type="text" name="emailComercial" id="emailComercial" class="w293 emails empty" value="<?php echo (isset($temCadastro->email_comercial)) ? $temCadastro->email_comercial : "" ;?>" /></div>                       
                        </label>
                    </li>
					<li>
                        <label>
                            <span class="label">E-mail pessoal:</span>
                            <div class="input"><input type="text" name="emailPessoal" id="emailPessoal" class="w293 emails empty" value="<?php echo (isset($temCadastro->email_pessoal)) ? $temCadastro->email_pessoal : "" ;?>" /></div>                       
                        </label>
                    </li>
					<li>
						<label>
							<span class="label">Telefone comercial:</span>
							<div class="input"><input type="text" name="telComercial" id="telComercial" class="phone_with_ddd empty" value="<?php echo (isset($temCadastro->telefone_comercial)) ? $temCadastro->telefone_comercial : "" ;?>" /></div>
						</label>
					</li>
					<li>
						<label>
							<span class="label">Telefone Residencial:</span>
							<div class="input"><input type="text" name="telResidencial" id="telResidencial" class="phone_with_ddd empty" value="<?php echo (isset($temCadastro->telefone_residencial)) ? $temCadastro->telefone_residencial : "" ;?>" /></div>
						</label>
					</li>
					<li>
						<label>
							<span class="label">Telefone celular:</span>
							<div class="input"><input type="text" name="telCelular" id="telCelular" class="phone_with_ddd empty" value="<?php echo (isset($temCadastro->telefone_celular)) ? $temCadastro->telefone_celular : "" ;?>" /></div>
						</label>
					</li>
                    <li>
                        <label>
                            <span class="label">Endereço Comercial:</span>
                            <div  class="input"><input type="text" name="endComercial" id="endComercial" class="w380 empty" value="<?php echo (isset($temCadastro->endereco_comercial)) ? $temCadastro->endereco_comercial : "" ;?>" /></div>
                        </label>
                    </li>
                    <li class="inline">
                        <label>
                            <span class="label">Número:</span>
                            <div class="input"><input type="text" name="numeroComercial" id="numeroComercial" class="w100 spacing empty" maxlength="5" value="<?php echo (isset($temCadastro->numero_comercial)) ? $temCadastro->numero_comercial : "" ;?>" /></div>
                        </label>
                    </li>
                    <li class="inline">
                        <label>
                            <span class="labelside">Complemento:</span>
                            <div class="input"><input type="text" name="compComercial" id="compComercial" class="w200" value="<?php echo (isset($temCadastro->complemento_comercial)) ? $temCadastro->complemento_comercial : "" ;?>" /></div>
                        </label>
                    </li>
                    <li class="clear"></li>
					 <li class="inline">
						<label>
							<span class="label">CEP:</span>
							<div class="input"><input type="text" name="cepComercial" id="cepComercial" class="cep empty" value="<?php echo (isset($temCadastro->cep_comercial)) ? $temCadastro->cep_comercial : "" ;?>" /></div>
						</label>
					</li>
                    <li class="inline">
						<label>
							<span class="labelside ">Bairro:</span>
							<div class="input"><input type="text" name="bairroComercial" id="bairroComercial" class="w200 empty" value="<?php echo (isset($temCadastro->bairro_comercial)) ? $temCadastro->bairro_comercial : "" ;?>" /></div>
						</label>
					</li>  
					 <li class="clear"></li>
                    <li class="inline">
                        <label>
                            <span class="label">Cidade:</span>
                            <div class="input"><input type="text" name="cidadeComercial" id="cidadeComercial" class="w180 empty" value="<?php echo (isset($temCadastro->cidade_comercial)) ? $temCadastro->cidade_comercial : "" ;?>" /></div>
                        </label>
                    </li>
                    <li class="inline">
                        <label>
                            <span class="labelside padding">Estado:</span>
                            <div class="input"><input type="text" name="estadoComercial" id="estadoComercial" class="w45 empty" maxlength="2" value="<?php echo (isset($temCadastro->estado_comercial)) ? $temCadastro->estado_comercial : "" ;?>"/></div>
                        </label>
                    </li>
                </ul>

				<ul>
					<li>
                        <label>
                            <span class="label">Endereço Residencial:</span>
                            <div  class="input"><input type="text" name="endResidencial" id="endResidencial" class="w380 empty" value="<?php echo (isset($temCadastro->endereco_residencial)) ? $temCadastro->endereco_residencial : "" ;?>" /></div>
                        </label>
                    </li>
                    <li class="inline">
                        <label>
                            <span class="label">Número:</span>
                            <div class="input"><input type="text" name="numeroResidencial" id="numeroResidencial" class="w100 spacing empty" value="<?php echo (isset($temCadastro->numero_residencial)) ? $temCadastro->numero_residencial : "" ;?>"/></div>
                        </label>
                    </li>
                    <li class="inline">
                        <label>
                            <span class="labelside">Complemento:</span>
                            <div class="input"><input type="text" name="compResidencial" id="compResidencial" class="w200" value="<?php echo (isset($temCadastro->complemento_residencial)) ? $temCadastro->complemento_residencial : "" ;?>" /></div>
                        </label>
                    </li>
                    <li class="clear"></li>
					 <li class="inline">
						<label>
							<span class="label">CEP:</span>
							<div class="input"><input type="text" name="cepResidencial" id="cepResidencial" class="cep empty" value="<?php echo (isset($temCadastro->cep_residencial)) ? $temCadastro->cep_residencial : "" ;?>"/></div>
						</label>
					</li>
                    <li class="inline">
						<label>
							<span class="labelside ">Bairro:</span>
							<div class="input"><input type="text" name="bairroResidencial" id="bairroResidencial" class="w200 empty" value="<?php echo (isset($temCadastro->bairro_residencial)) ? $temCadastro->bairro_residencial : "" ;?>" /></div>
						</label>
					</li>  
					 <li class="clear"></li>
                    <li class="inline">
                        <label>
                            <span class="label">Cidade:</span>
                            <div class="input"><input type="text" name="cidadeResidencial" id="cidadeResidencial" class="w180 empty" value="<?php echo (isset($temCadastro->cidade_residencial)) ? $temCadastro->cidade_residencial : "" ;?>"/></div>
                        </label>
                    </li>
                    <li class="inline">
                        <label>
                            <span class="labelside padding">Estado:</span>
                            <div class="input"><input type="text" name="estadoResidencial" id="estadoResidencial" class="w45 empty" maxlength="2" value="<?php echo (isset($temCadastro->estado_residencial)) ? $temCadastro->estado_residencial : "" ;?>"/></div>
                        </label>
                    </li>
					<li>
						<label>
								<span class="label">Aeroporto mais próximo para vôo nacional:</span>
								<div class="divSelect">
									<select name="aeroporto" id="aeroporto" class="selectInput">
									    <option value="">Selecione um Aeroporto</option>
                                        <option value="SBHT"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBHT') {?> selected="selected"<?php } ?>>ALTAMIRA-PA</option>
                                        <option value="SBAR"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBAR') {?> selected="selected"<?php } ?>>ARACAJU-SE</option>
                                        <option value="SBBE"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBBE') {?> selected="selected"<?php } ?>>BELÉM-PA</option>
                                        <option value="SBBH"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBBH') {?> selected="selected"<?php } ?>>BELO HORIZONTE - PAMPULHA-MG</option>
                                        <option value="SBCF"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCF') {?> selected="selected"<?php } ?>>BELO HORIZONTE - T.NEVES-MG</option>
                                        <option value="SBBV"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBBV') {?> selected="selected"<?php } ?>>BOA VISTA-RR</option>
                                        <option value="SBBR"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBBR') {?> selected="selected"<?php } ?>>BRASÍLIA-DF</option>
                                        <option value="SBKG"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBKG') {?> selected="selected"<?php } ?>>CAMPINA GRANDE-PB</option>
                                        <option value="SBKP"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBKP') {?> selected="selected"<?php } ?>>CAMPINAS-SP</option>
                                        <option value="SBCG"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCG') {?> selected="selected"<?php } ?>>CAMPO GRANDE-MS</option>
                                        <option value="SBCP"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCP') {?> selected="selected"<?php } ?>>CAMPOS DOS GOYTACAZES-RJ</option>
                                        <option value="SBCJ"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCJ') {?> selected="selected"<?php } ?>>CARAJÁS-PA</option>
                                        <option value="SBCR"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCR') {?> selected="selected"<?php } ?>>CORUMBÁ-MS</option>
                                        <option value="SBCM"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCM') {?> selected="selected"<?php } ?>>CRICIÚMA-SC</option>
                                        <option value="SBCZ"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCZ') {?> selected="selected"<?php } ?>>CRUZEIRO DO SUL-AC</option>
                                        <option value="SBCY"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCY') {?> selected="selected"<?php } ?>>CUIABÁ-MT</option>
                                        <option value="SBCT"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBCT') {?> selected="selected"<?php } ?>>CURITIBA-PR</option>
                                        <option value="SBFL"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBFL') {?> selected="selected"<?php } ?>>FLORIANÓPOLIS-SC</option>
                                        <option value="SBFZ"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBFZ') {?> selected="selected"<?php } ?>>FORTALEZA-CE</option>
                                        <option value="SBFI"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBFI') {?> selected="selected"<?php } ?>>FOZ DO IGUAÇU-PR</option>
                                        <option value="SBGO"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBGO') {?> selected="selected"<?php } ?>>GOIÂNIA-GO</option>
                                        <option value="SBIL"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBIL') {?> selected="selected"<?php } ?>>ILHÉUS-BA</option>
                                        <option value="SBIZ"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBIZ') {?> selected="selected"<?php } ?>>IMPERATRIZ-MA</option>
                                        <option value="SBJP"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBJP') {?> selected="selected"<?php } ?>>JOAO PESSOA-PB</option>
                                        <option value="SBJV"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBJV') {?> selected="selected"<?php } ?>>JOINVILLE-SC</option>
                                        <option value="SBJU"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBJU') {?> selected="selected"<?php } ?>>JUAZEIRO DO NORTE-CE</option>
                                        <option value="SBLO"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBLO') {?> selected="selected"<?php } ?>>LONDRINA-PR</option>
                                        <option value="SBME"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBME') {?> selected="selected"<?php } ?>>MACAÉ-RJ</option>
                                        <option value="SBMQ"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBMQ') {?> selected="selected"<?php } ?>>MACAPÁ-AP</option>
                                        <option value="SBMO"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBMO') {?> selected="selected"<?php } ?>>MACEIÓ-AL</option>
                                        <option value="SBEG"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBEG') {?> selected="selected"<?php } ?>>MANAUS-AM</option>
                                        <option value="SBMA"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBMA') {?> selected="selected"<?php } ?>>MARABÁ-PA</option>
                                        <option value="SBMK"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBMK') {?> selected="selected"<?php } ?>>MONTES CLAROS-MG</option>
                                        <option value="SBSG"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBSG') {?> selected="selected"<?php } ?>>NATAL-RN</option>
                                        <option value="SBNF"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBNF') {?> selected="selected"<?php } ?>>NAVEGANTES-SC</option>
                                        <option value="SBPJ"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBPJ') {?> selected="selected"<?php } ?>>PALMAS-TO</option>
                                        <option value="SBPK"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBPK') {?> selected="selected"<?php } ?>>PELOTAS-RS</option>
                                        <option value="SBUF"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBUF') {?> selected="selected"<?php } ?>>PAULO AFONSO-BA</option>
                                        <option value="SBPL"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBPL') {?> selected="selected"<?php } ?>>PETROLINA-PE</option>
                                        <option value="SBPA"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBPA') {?> selected="selected"<?php } ?>>PORTO ALEGRE-RS</option>
                                        <option value="SBPV"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBPV') {?> selected="selected"<?php } ?>>PORTO VELHO - RO</option>
                                        <option value="SBRF"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBRF') {?> selected="selected"<?php } ?>>RECIFE-PE</option>
                                        <option value="SBRB"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBRB') {?> selected="selected"<?php } ?>>RIO BRANCO-AC</option>
                                        <option value="SBGL"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBGL') {?> selected="selected"<?php } ?>>RIO DE JANEIRO - GALEAO-RJ</option>
                                        <option value="SBRJ"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBRJ') {?> selected="selected"<?php } ?>>RIO DE JANEIRO - S.DUMONT-RJ</option>
                                        <option value="SBSV"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBSV') {?> selected="selected"<?php } ?>>SALVADOR-BA</option>
                                        <option value="SBSN"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBSN') {?> selected="selected"<?php } ?>>SANTARÉM-PA</option>
                                        <option value="SBSJ"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBSJ') {?> selected="selected"<?php } ?>>SAO JOSÉ DOS CAMPOS-SP</option>
                                        <option value="SBSL"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBSL') {?> selected="selected"<?php } ?>>SAO LUIS-MA</option>
                                        <option value="SBSP"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBSP') {?> selected="selected"<?php } ?>>SAO PAULO - CONGONHAS-SP</option>
                                        <option value="SBGR"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBGR') {?> selected="selected"<?php } ?>>SAO PAULO - GUARULHOS-SP</option>
                                        <option value="SBTT"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBTT') {?> selected="selected"<?php } ?>>TABATINGA-AM</option>
                                        <option value="SBTF"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBTF') {?> selected="selected"<?php } ?>>TEFÉ-AM</option>
                                        <option value="SBTE"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBTE') {?> selected="selected"<?php } ?>>TERESINA-PI</option>
                                        <option value="SBUR"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBUR') {?> selected="selected"<?php } ?>>UBERABA-MG</option>
                                        <option value="SBUL"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBUL') {?> selected="selected"<?php } ?>>UBERLÂNDIA-MG</option>
                                        <option value="SBUG"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBUG') {?> selected="selected"<?php } ?>>URUGUAIANA-RS</option>
                                        <option value="SBVT"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBVT') {?> selected="selected"<?php } ?>>VITÓRIA-ES</option>
                                        <option value="SBPB"<?php if(isset($temCadastro->aeroporto) && $temCadastro->aeroporto=='SBPB') {?> selected="selected"<?php } ?>>PARNAÍBA-PI</option>
									</select>	
								</div>		
						</label>
					</li>
					<li class="inline">
                        <label>
                            <span class="label">Distância até o aeroporto:</span>
                            <div class="input"><input type="text" name="distanciaAero" id="distanciaAero"  class="w100 spacing" value="<?php echo (isset($temCadastro->distancia_aero)) ? $temCadastro->distancia_aero : "" ;?>"/></div>
                        </label>
                    </li>
					<li>
                        <span class="label">Aeroporto de retorno é o mesmo de origem?</span>
                        <label>
                            <input type="radio" name="aeroportoRtn" value="S" class="mark"<?php if(isset($temCadastro->mesmo_aero) && $temCadastro->mesmo_aero=='S') {?> checked="checked"<?php } ?> /><span class="textradio">Sim</span>
                        </label>
                        <label>
                            <input  type="radio" name="aeroportoRtn" value="N" class="radio2 mark"<?php if(isset($temCadastro->mesmo_aero) && $temCadastro->mesmo_aero=='N') {?> checked="checked"<?php } ?>  /><span>Não</span>
                        </label>
                    </li>
					<li<?php if(isset($temCadastro->mesmo_aero) && $temCadastro->mesmo_aero=='S') {?> style="display:none"<?php } ?> class="divAeroportoRtn">
						<label>
							<span class="label">Aeroporto de Retorno:</span>
								<div class="divSelect">
									<select name="opAeroportoRtn" id="opAeroportoRtn" class="selectInput">
                                        <option value="">Selecione um Aeroporto</option>
                                        <option value="SBHT"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBHT') {?> selected="selected"<?php } ?>>ALTAMIRA-PA</option>
                                        <option value="SBAR"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBAR') {?> selected="selected"<?php } ?>>ARACAJU-SE</option>
                                        <option value="SBBE"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBBE') {?> selected="selected"<?php } ?>>BELÉM-PA</option>
                                        <option value="SBBH"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBBH') {?> selected="selected"<?php } ?>>BELO HORIZONTE - PAMPULHA-MG</option>
                                        <option value="SBCF"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCF') {?> selected="selected"<?php } ?>>BELO HORIZONTE - T.NEVES-MG</option>
                                        <option value="SBBV"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBBV') {?> selected="selected"<?php } ?>>BOA VISTA-RR</option>
                                        <option value="SBBR"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBBR') {?> selected="selected"<?php } ?>>BRASÍLIA-DF</option>
                                        <option value="SBKG"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBKG') {?> selected="selected"<?php } ?>>CAMPINA GRANDE-PB</option>
                                        <option value="SBKP"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBKP') {?> selected="selected"<?php } ?>>CAMPINAS-SP</option>
                                        <option value="SBCG"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCG') {?> selected="selected"<?php } ?>>CAMPO GRANDE-MS</option>
                                        <option value="SBCP"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCP') {?> selected="selected"<?php } ?>>CAMPOS DOS GOYTACAZES-RJ</option>
                                        <option value="SBCJ"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCJ') {?> selected="selected"<?php } ?>>CARAJÁS-PA</option>
                                        <option value="SBCR"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCR') {?> selected="selected"<?php } ?>>CORUMBÁ-MS</option>
                                        <option value="SBCM"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCM') {?> selected="selected"<?php } ?>>CRICIÚMA-SC</option>
                                        <option value="SBCZ"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCZ') {?> selected="selected"<?php } ?>>CRUZEIRO DO SUL-AC</option>
                                        <option value="SBCY"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCY') {?> selected="selected"<?php } ?>>CUIABÁ-MT</option>
                                        <option value="SBCT"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBCT') {?> selected="selected"<?php } ?>>CURITIBA-PR</option>
                                        <option value="SBFL"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBFL') {?> selected="selected"<?php } ?>>FLORIANÓPOLIS-SC</option>
                                        <option value="SBFZ"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBFZ') {?> selected="selected"<?php } ?>>FORTALEZA-CE</option>
                                        <option value="SBFI"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBFI') {?> selected="selected"<?php } ?>>FOZ DO IGUAÇU-PR</option>
                                        <option value="SBGO"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBGO') {?> selected="selected"<?php } ?>>GOIÂNIA-GO</option>
                                        <option value="SBIL"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBIL') {?> selected="selected"<?php } ?>>ILHÉUS-BA</option>
                                        <option value="SBIZ"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBIZ') {?> selected="selected"<?php } ?>>IMPERATRIZ-MA</option>
                                        <option value="SBJP"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBJP') {?> selected="selected"<?php } ?>>JOAO PESSOA-PB</option>
                                        <option value="SBJV"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBJV') {?> selected="selected"<?php } ?>>JOINVILLE-SC</option>
                                        <option value="SBJU"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBJU') {?> selected="selected"<?php } ?>>JUAZEIRO DO NORTE-CE</option>
                                        <option value="SBLO"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBLO') {?> selected="selected"<?php } ?>>LONDRINA-PR</option>
                                        <option value="SBME"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBME') {?> selected="selected"<?php } ?>>MACAÉ-RJ</option>
                                        <option value="SBMQ"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBMQ') {?> selected="selected"<?php } ?>>MACAPÁ-AP</option>
                                        <option value="SBMO"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBMO') {?> selected="selected"<?php } ?>>MACEIÓ-AL</option>
                                        <option value="SBEG"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBEG') {?> selected="selected"<?php } ?>>MANAUS-AM</option>
                                        <option value="SBMA"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBMA') {?> selected="selected"<?php } ?>>MARABÁ-PA</option>
                                        <option value="SBMK"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBMK') {?> selected="selected"<?php } ?>>MONTES CLAROS-MG</option>
                                        <option value="SBSG"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBSG') {?> selected="selected"<?php } ?>>NATAL-RN</option>
                                        <option value="SBNF"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBNF') {?> selected="selected"<?php } ?>>NAVEGANTES-SC</option>
                                        <option value="SBPJ"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBPJ') {?> selected="selected"<?php } ?>>PALMAS-TO</option>
                                        <option value="SBPK"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBPK') {?> selected="selected"<?php } ?>>PELOTAS-RS</option>
                                        <option value="SBUF"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBUF') {?> selected="selected"<?php } ?>>PAULO AFONSO-BA</option>
                                        <option value="SBPL"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBPL') {?> selected="selected"<?php } ?>>PETROLINA-PE</option>
                                        <option value="SBPA"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBPA') {?> selected="selected"<?php } ?>>PORTO ALEGRE-RS</option>
                                        <option value="SBPV"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBPV') {?> selected="selected"<?php } ?>>PORTO VELHO - RO</option>
                                        <option value="SBRF"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBRF') {?> selected="selected"<?php } ?>>RECIFE-PE</option>
                                        <option value="SBRB"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBRB') {?> selected="selected"<?php } ?>>RIO BRANCO-AC</option>
                                        <option value="SBGL"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBGL') {?> selected="selected"<?php } ?>>RIO DE JANEIRO - GALEAO-RJ</option>
                                        <option value="SBRJ"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBRJ') {?> selected="selected"<?php } ?>>RIO DE JANEIRO - S.DUMONT-RJ</option>
                                        <option value="SBSV"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBSV') {?> selected="selected"<?php } ?>>SALVADOR-BA</option>
                                        <option value="SBSN"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBSN') {?> selected="selected"<?php } ?>>SANTARÉM-PA</option>
                                        <option value="SBSJ"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBSJ') {?> selected="selected"<?php } ?>>SAO JOSÉ DOS CAMPOS-SP</option>
                                        <option value="SBSL"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBSL') {?> selected="selected"<?php } ?>>SAO LUIS-MA</option>
                                        <option value="SBSP"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBSP') {?> selected="selected"<?php } ?>>SAO PAULO - CONGONHAS-SP</option>
                                        <option value="SBGR"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBGR') {?> selected="selected"<?php } ?>>SAO PAULO - GUARULHOS-SP</option>
                                        <option value="SBTT"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBTT') {?> selected="selected"<?php } ?>>TABATINGA-AM</option>
                                        <option value="SBTF"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBTF') {?> selected="selected"<?php } ?>>TEFÉ-AM</option>
                                        <option value="SBTE"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBTE') {?> selected="selected"<?php } ?>>TERESINA-PI</option>
                                        <option value="SBUR"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBUR') {?> selected="selected"<?php } ?>>UBERABA-MG</option>
                                        <option value="SBUL"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBUL') {?> selected="selected"<?php } ?>>UBERLÂNDIA-MG</option>
                                        <option value="SBUG"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBUG') {?> selected="selected"<?php } ?>>URUGUAIANA-RS</option>
                                        <option value="SBVT"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBVT') {?> selected="selected"<?php } ?>>VITÓRIA-ES</option>
                                        <option value="SBPB"<?php if(isset($temCadastro->aeroporto_retorno) && $temCadastro->aeroporto_retorno=='SBPB') {?> selected="selected"<?php } ?>>PARNAÍBA-PI</option>
									</select>	
								</div>
						</label>
					</li>
					<li>
						<label>
							<span class="label">Telefone Emergencial:</span>
							<div class="input"><input type="text" name="telEmergencial" id="telEmergencial" class="phone_with_ddd empty" value="<?php echo (isset($temCadastro->telefone_emergencial)) ? $temCadastro->telefone_emergencial : "" ;?>"/></div>
						</label>
					</li>
					<li>
						<label>
							<span class="label">Contato:</span>
							<div class="input"><input type="text" name="contatoEmerg" id="contatoEmerg" value="<?php echo (isset($temCadastro->contato_emergencial)) ? $temCadastro->contato_emergencial : "" ;?>"/></div>
						</label>
					</li>
					<li>
						<span class="label">Possui plano de saúde?</span>
						<label>
							<input type="radio" name="planoSd" value="S" class="mark"<?php if(isset($temCadastro->plano_saude) && $temCadastro->plano_saude=='S') {?> checked="checked"<?php } ?> /><span class="textradio">Sim</span>
						</label>
						<label>
							<input class="radio2 mark" name="planoSd" type="radio" value="N"<?php if(isset($temCadastro->plano_saude) && $temCadastro->plano_saude=='N') {?> checked="checked"<?php } ?> /><span>Não</span>
						</label>
					</li>
					
					<li<?php if(isset($temCadastro->plano_saude) && $temCadastro->plano_saude=='N') {?> style="display:none"<?php } ?> class="divPlanoSd">
						<label>
							<span class="label">Cobertura:</span>
							<div class="input"><input type="text" name="cobertura" id="cobertura" class="opt" value="<?php echo (isset($temCadastro->cobertura)) ? $temCadastro->cobertura : "" ;?>"/></div>
						</label>
					</li>
					<li>
						<span class="label">É gestante?</span>
						<label>
							<input type="radio" name="gestante" value="S"  class="mark"<?php if(isset($temCadastro->gestante) && $temCadastro->gestante=='S') {?> checked="checked"<?php } ?> /><span class="textradio">Sim</span>
						</label>
						<label>
							<input class="radio2 mark" name="gestante" type="radio" value="N"<?php if(isset($temCadastro->gestante) && $temCadastro->gestante=='N') {?> checked="checked"<?php } ?>/><span>Não</span>
						</label>
					</li>
					
					<li<?php if(isset($temCadastro->gestante) && $temCadastro->gestante=='N') {?> style="display:none"<?php } ?> class="divGestante">
						<label>
							<span class="label">Qual o período?</span>
							<div class="input"><input type="text" name="periodo" id="periodo" class="opt" value="<?php echo (isset($temCadastro->periodo)) ? $temCadastro->periodo : "" ;?>" /></div>
						</label>
					</li>
					<li>
						<span class="label">Possui alguma limitação física?</span>
						<label>
							<input type="radio" name="lmtcFisica" value="S" class="mark"<?php if(isset($temCadastro->tem_limitacao) && $temCadastro->tem_limitacao=='S') {?> checked="checked"<?php } ?> /><span class="textradio">Sim</span>
						</label>
						<label>
							<input class="radio2 mark" name="lmtcFisica" type="radio" value="N"<?php if(isset($temCadastro->tem_limitacao) && $temCadastro->tem_limitacao=='N') {?> checked="checked"<?php } ?> /><span>Não</span>
						</label>
					</li>
					<li<?php if(isset($temCadastro->tem_limitacao) && $temCadastro->tem_limitacao=='N') {?> style="display:none"<?php } ?> class="divLmtcFisica">
						<label>
							<span class="label">Qual?</span>
							<div class="input"><input type="text" class="opt" name="limitacao" id="limitacao" value="<?php echo (isset($temCadastro->limitacao_fisica)) ? $temCadastro->limitacao_fisica : "" ;?>" /></div>
						</label>
					</li>
                    <li>
						<span class="label">Tem alergia a algum medicamento/ alimento?</span>
						<label>
							<input type="radio" name="temAlergia" value="S" class="mark"<?php if(isset($temCadastro->tem_alergia) && $temCadastro->tem_alergia=='S') {?> checked="checked"<?php } ?> /><span class="textradio">Sim</span>
						</label>
						<label>
							<input class="radio2 mark" name="temAlergia" type="radio" value="N"<?php if(isset($temCadastro->tem_alergia) && $temCadastro->tem_alergia=='N') {?> checked="checked"<?php } ?> /><span>Não</span>
						</label>
					</li>
					<li<?php if(isset($temCadastro->tem_alergia) && $temCadastro->tem_alergia=='N') {?> style="display:none"<?php } ?> class="divTemAlergia">
						<label>
							<span class="label">Qual?</span>
							<div class="input"><input type="text" class="opt" name="alergia" id="alergia" value="<?php echo (isset($temCadastro->alergia)) ? $temCadastro->alergia : "" ;?>" /></div>
						</label>
					</li>
					 <li>
						<span class="label">Tamanho de camiseta:</span>
						<label>
							<input type="radio" name="tmCamiseta" value="P" class="mark"<?php if(isset($temCadastro->camiseta) && $temCadastro->camiseta=='P') {?> checked="checked"<?php } ?>/><span class="textradio"><span>P</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="tmCamiseta" value="M" type="radio"<?php if(isset($temCadastro->camiseta) && $temCadastro->camiseta=='M') {?> checked="checked"<?php } ?> /><span>M</span>
						</label>
						<label>
							<input type="radio" name="tmCamiseta" value="G" class="radio2 mark"<?php if(isset($temCadastro->camiseta) && $temCadastro->camiseta=='G') {?> checked="checked"<?php } ?>/><span class="textradio"><span>G</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="tmCamiseta" value="GG" type="radio"<?php if(isset($temCadastro->camiseta) && $temCadastro->camiseta=='GG') {?> checked="checked"<?php } ?> /><span>GG</span>
						</label>
						<label>
							<input class="radio2 mark" name="tmCamiseta" value="XG" type="radio"<?php if(isset($temCadastro->camiseta) && $temCadastro->camiseta=='XG') {?> checked="checked"<?php } ?> /><span>XG</span>
						</label>
					</li>
					<li>
						<span class="label">É fumante?</span>
						<label>
							<input type="radio" name="fumante" value="S" class="mark"<?php if(isset($temCadastro->fumante) && $temCadastro->fumante=='S') {?> checked="checked"<?php } ?> /><span class="textradio">Sim</span>
						</label>
						<label>
							<input class="radio2 mark" name="fumante" type="radio" value="N"<?php if(isset($temCadastro->fumante) && $temCadastro->fumante=='N') {?> checked="checked"<?php } ?> /><span>Não</span>
						</label>
					</li>
				</ul>
				
				 <h4>Sobre sua alimentação, numere conforme sua preferência, classificando como 1 o que mais gosta e 5 que não gosta.</h4>
				<ul>
                	<li>
						<span class="label alinhamento">Porco</span>
						<label>
							<input type="radio" name="almPorco" value="1" class="mark"<?php if(isset($temCadastro->porco) && $temCadastro->porco==1) {?> checked="checked"<?php } ?> /><span class="textradio"><span>1</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almPorco" value="2" type="radio"<?php if(isset($temCadastro->porco) && $temCadastro->porco==2) {?> checked="checked"<?php } ?> /><span>2</span>
						</label>
						<label>
							<input type="radio" name="almPorco" value="3" class="radio2 mark"<?php if(isset($temCadastro->porco) && $temCadastro->porco==3) {?> checked="checked"<?php } ?>/><span class="textradio"><span>3</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almPorco" value="4" type="radio"<?php if(isset($temCadastro->porco) && $temCadastro->porco==4) {?> checked="checked"<?php } ?> /><span>4</span>
						</label>
						<label>
							<input class="radio2 mark" name="almPorco" value="5" type="radio"<?php if(isset($temCadastro->porco) && $temCadastro->porco==5) {?> checked="checked"<?php } ?> /><span>5</span>
						</label>
					</li>
					<li>
						<span class="label">Bovino</span>
						<label>
							<input type="radio" name="almBovino" value="1" class="mark"<?php if(isset($temCadastro->bovino) && $temCadastro->bovino==1) {?> checked="checked"<?php } ?> /><span class="textradio"><span>1</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almBovino" value="2" type="radio"<?php if(isset($temCadastro->bovino) && $temCadastro->bovino==2) {?> checked="checked"<?php } ?> /><span>2</span>
						</label>
						<label>
							<input type="radio" name="almBovino" value="3" class="radio2 mark"<?php if(isset($temCadastro->bovino) && $temCadastro->bovino==3) {?> checked="checked"<?php } ?>/><span class="textradio"><span>3</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almBovino" value="4" type="radio"<?php if(isset($temCadastro->bovino) && $temCadastro->bovino==4) {?> checked="checked"<?php } ?> /><span>4</span>
						</label>
						<label>
							<input class="radio2 mark" name="almBovino" value="5" type="radio"<?php if(isset($temCadastro->bovino) && $temCadastro->bovino==5) {?> checked="checked"<?php } ?> /><span>5</span>
						</label>
					</li>
					 <li>
						<span class="label">Frango</span>
						<label>
							<input type="radio" name="almFrango" value="1" class="mark"<?php if(isset($temCadastro->frango) && $temCadastro->frango==1) {?> checked="checked"<?php } ?> /><span class="textradio"><span>1</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almFrango" value="2" type="radio"<?php if(isset($temCadastro->frango) && $temCadastro->frango==2) {?> checked="checked"<?php } ?> /><span>2</span>
						</label>
						<label>
							<input type="radio" name="almFrango" value="3" class="radio2 mark"<?php if(isset($temCadastro->frango) && $temCadastro->frango==3) {?> checked="checked"<?php } ?>/><span class="textradio"><span>3</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almFrango" value="4" type="radio"<?php if(isset($temCadastro->frango) && $temCadastro->frango==4) {?> checked="checked"<?php } ?> /><span>4</span>
						</label>
						<label>
							<input class="radio2 mark" name="almFrango" value="5" type="radio"<?php if(isset($temCadastro->frango) && $temCadastro->frango==5) {?> checked="checked"<?php } ?> /><span>5</span>
						</label>
					</li>
					 <li>
						<span class="label">Peixe</span>
						<label>
							<input type="radio" name="almPeixe" value="1" class="mark"<?php if(isset($temCadastro->peixe) && $temCadastro->peixe==1) {?> checked="checked"<?php } ?> /><span class="textradio"><span>1</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almPeixe" value="2" type="radio"<?php if(isset($temCadastro->peixe) && $temCadastro->peixe==2) {?> checked="checked"<?php } ?> /><span>2</span>
						</label>
						<label>
							<input type="radio" name="almPeixe" value="3" class="radio2 mark"<?php if(isset($temCadastro->peixe) && $temCadastro->peixe==3) {?> checked="checked"<?php } ?>/><span class="textradio"><span>3</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almPeixe" value="4" type="radio"<?php if(isset($temCadastro->peixe) && $temCadastro->peixe==4) {?> checked="checked"<?php } ?> /><span>4</span>
						</label>
						<label>
							<input class="radio2 mark" name="almPeixe" value="5" type="radio"<?php if(isset($temCadastro->peixe) && $temCadastro->peixe==5) {?> checked="checked"<?php } ?> /><span>5</span>
						</label>
					</li>
					 <li>
						<span class="label">Frutos do mar</span>
						<label>
							<input type="radio" name="almFrutos" value="1" class="mark"<?php if(isset($temCadastro->frutos_mar) && $temCadastro->frutos_mar==1) {?> checked="checked"<?php } ?>/><span class="textradio"><span>1</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almFrutos" value="2" type="radio"<?php if(isset($temCadastro->frutos_mar) && $temCadastro->frutos_mar==2) {?> checked="checked"<?php } ?> /><span>2</span>
						</label>
						<label>
							<input type="radio" name="almFrutos" value="3" class="radio2 mark"<?php if(isset($temCadastro->frutos_mar) && $temCadastro->frutos_mar==3) {?> checked="checked"<?php } ?>/><span class="textradio"><span>3</span></span>
						</label>
						<label>
							<input class="radio2 mark" name="almFrutos" value="4" type="radio" <?php if(isset($temCadastro->frutos_mar) && $temCadastro->frutos_mar==4) {?> checked="checked"<?php } ?>/><span>4</span>
						</label>
						<label>
							<input class="radio2 mark" name="almFrutos" value="5" type="radio"<?php if(isset($temCadastro->frutos_mar) && $temCadastro->frutos_mar==5) {?> checked="checked"<?php } ?> /><span>5</span>
						</label>
					</li>
                    <li>
						<label>
							<span class="label">Tem alguma restrição alimentar?</span>
							<div class="input"><input type="text" name="restricao_alimentar" id="restricao_alimentar" class="w180" value="<?php echo (isset($temCadastro->restricao_alimentar)) ? $temCadastro->restricao_alimentar : "" ;?>"/></div>
						</label>
					</li>
			</ul>

            <h4>TERMO DE AUTORIZAÇÃO DE USO DE IMAGEM E CONTEÚDO</h4>
            <span class="label textposition">Baseando-se nas informações dadas acima EU AUTORIZO o BANCO SANTANDER (BRASIL) S.A., inscrito no CNPJ/MF sob o n.º 90.400.888/0001-42 e suas demais empresas coligadas, subsidiárias, afiliadas ou pertencentes ao mesmo conglomerado econômico no Brasil e no exterior (doravante SANTANDER), a utilizar, a título gratuito, por prazo permanente e em todo o mundo, a minha imagem em fotos e vídeos, inclusive minha voz, bem como o conteúdo de frases de minha autoria, para utilização em qualquer suporte tangível ou intangível, inclusive, mas não se limitando, em releases e publicações junto à imprensa brasileira e estrangeira, além do site <a href="www.santander.com.br">www.santander.com.br</a>, destinadas à divulgação ao público em geral e/ou apenas para uso interno do SANTANDER, desde que não haja desvirtuamento da sua finalidade. AUTORIZO também a realização de reproduções, ampliações, alterações com efeitos especiais e fixações necessárias, visando o melhor aproveitamento da Imagem e/ou Voz e/ou Conteúdo, podendo a mesma ser apresentada no todo ou em parte, isoladamente ou em composição com textos e outras imagens.
			</span>
			<ul>
            	<li>
                    <label>
                        <input type="checkbox" name="termoAutorizacao" id="tmAutorizacao" value="0">
                        <span class="textradio">AUTORIZO O BANCO SANTANDER A UTILIZAR A MINHA IMAGEM PARA OS FINS MENCIONADOS ACIMA</span> 
                    </label>
                </li>
            </ul>
            <div class="btnsubmit">
                <input type="submit" id="btnSubmit" disabled/>
            </div>
        </form>

            <div class="sendForm">
                <p class="send">Enviando seus Dados</p>
            </div>
        </div>

    </div>
</div>
<!-- end:content -->
