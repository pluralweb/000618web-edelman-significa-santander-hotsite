<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Santander - Desafio 2015</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">td img {display: block;}</style>
</head>
<body bgcolor="#ffffff">
<center>
<table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="600">
  <tr>
   <!-- <td><img name="index_r1_c1" src="<?php echo base_url('imgs/email/index_r1_c1.jpg') ?>" width="600" height="179" id="index_r1_c1" alt="" /></td> -->
  </tr>
  <tr>
   <td width="600" height="47"></td>
  </tr>
  <tr>
   <td><table style="display: inline-table;" align="left" border="0" cellpadding="0" cellspacing="0" width="600">
	  <tr>
	   <td width="46"></td>
	   <td width="513">
       <font face="Verdana, Geneva, sans-serif" size="2" color="#333">
        Olá <strong><?php echo $nome; ?></strong>,<br /><br />
        Já alteramos a sua senha.
        <br /><br />
        A sua nova senha é <font color="#ed1c24"><strong><?php echo $senha; ?></strong></font>.
        <br /><br />
        Obrigado pela visita. Esperamos em breve construir, juntos, um Santander ainda maior.
        <br /><br />
        Att,<br />
        Equipe Santander.

       </td>
	   <td width="41"></td>
	  </tr>
	</table></td>
  </tr>
  <tr>
   <td width="600" height="47"></td>
  </tr>
  <tr>
   <!-- <td><img name="index_r6_c1" src="<?php echo base_url('imgs/email/index_r6_c1.jpg') ?>" width="600" height="85" id="index_r6_c1" alt="" /></td> -->
  </tr>
</table>
</center>
</body>
</html>
